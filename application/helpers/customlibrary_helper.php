<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* * **************************************
 * 	FUNCIONES DE CONTROL DE SESIONES	*
 * ************************************** */

if (!function_exists('is_login_with_cookies')) {

    function is_login_with_cookies($thish) {
        if ($thish->input->cookie($thish->config->item('encryption_key') . 'emailUsuario') != FALSE
                || $thish->input->cookie($thish->config->item('encryption_key') . 'contrasenaUsuario') != FALSE) {
            $thish->session->set_flashdata('usuario', $thish->input->cookie($thish->config->item('encryption_key') . 'emailUsuario'));
            $thish->session->set_flashdata('contrasenaUsuario', $thish->input->cookie($thish->config->item('encryption_key') . 'contrasenaUsuario'));
            redirect('sesion/logout/admin/true');
        }
    }
}

if (!function_exists('is_login_with_session')) {

    function is_login_with_session($thish) {
        if ($thish->session->userdata('adentro') == true) {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('tengo_permiso')) {

    function tengo_permiso($idRol, $idPermiso) {
        $CI = &get_instance();
        $CI -> load -> model('auth_model');
        
        if($CI -> auth_model -> tengo_permiso($idRol, $idPermiso))
            return true;
        return false;
    }

}

if (!function_exists('getMyPath')) {

    function getMyPath($idRol) {
                
        $path = '';
        
        switch ($idRol):
            case '1':
                $path = '';                
                break;
                
            case '2':
                $path = 'coordinador/';                
                break;
                
            case '3':
                $path = 'asesor/';                
                break;
                
            case '4':
                $path = 'hostess/';                
                break;
                
            case '5':
                $path = 'titulacion/';                
                break;
                
            case '6':
                $path = 'coordinador_obra/';                
                break;
                
            case '7':
                $path = 'broker/';                
                break;
                
            case '8':
                $path = '';                
                break;
            
            default:
                $path = '';                                
                break;
        endswitch;
        
        return $path;
    }
}

if (!function_exists('getMyMenu')) {

    function getMyMenu($idRol) {
        
        $return_menu = '';
        switch($idRol):
			case '1':
                $return_menu .= '<ul class="mainMenu">';
                $return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/usuarios">Usuarios</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/fraccionamientos">Fraccionamientos</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prototipos">Prototipos </a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/comisionesfrac">Comisiones</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prototiposfrac">Prototipos Fracc.</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/personalfrac">Asigna personal</a>';
                $return_menu .= '</li>';
								
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/casas">Casas</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/promociones">Promociones</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/casasdisponibles">Disponibilidad de casas</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/estatuscliente">Estatus</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prospecto">Prospectaci&oacute;n</a>';
                $return_menu .= '</li>';
				
                $return_menu .= '</ul>';                
                break;
				
            case '8':
                $return_menu .= '<ul class="mainMenu">';
                $return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/usuarios">Usuarios</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/fraccionamientos">Fraccionamientos</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prototipos">Prototipos </a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/comisionesfrac">Comisiones</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prototiposfrac">Prototipos Fracc.</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/personalfrac">Asigna personal</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/casas">Casas</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/promociones">Promociones</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/casasdisponibles">Disponibilidad de casas</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/estatuscliente">Estatus</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prospecto">Prospectaci&oacute;n</a>';
                $return_menu .= '</li>';
				
                $return_menu .= '</ul>';                
                break;
				
				case '2':
				$return_menu .= '<ul class="mainMenu">';
                $return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prospecto/misprospectos">Mis prospectos</a>';
                $return_menu .= '</li>';
				$return_menu .= '</ul>';
				
	           	$return_menu .= '<ul class="mainMenu">';
                $return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prospecto">Prospectaci&oacute;n</a>';
                $return_menu .= '</li>';
				$return_menu .= '</ul>';                
                break;
				
				case '3':
				$return_menu .= '<ul class="mainMenu">';
                
				// $return_menu .= '<li>';
                // $return_menu .= '<a href="'.base_url(). 'admin/dashboard">Inicio</a>';
                // $return_menu .= '</li>';
				
                $return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/prospecto/misprospectos">Mis prospectos</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
				$return_menu .= '	<a href="">Agenda</a>';
				$return_menu .= '	<ul>';
				$return_menu .= '		<li>';
                $return_menu .= '			<a href="'.base_url(). 'admin/agenda/index/1">Solo Hoy</a>';
				$return_menu .= '		</li>';
				$return_menu .= '		<li>';
                $return_menu .= '			<a href="'.base_url(). 'admin/agenda/index/2">Solo Pendientes</a>';
				$return_menu .= '		</li>';
				$return_menu .= '		<li>';
                $return_menu .= '			<a href="'.base_url(). 'admin/agenda/index/3">Todas las actividades</a>';
				$return_menu .= '		</li>';
                $return_menu .= '	</ul>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/casasdisponibles">Disponibilidad de casas</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '<li>';
                $return_menu .= '<a href="'.base_url(). 'admin/estatuscliente">Estatus</a>';
                $return_menu .= '</li>';
				
				$return_menu .= '</ul>';                
                break;
        endswitch;
        
        return $return_menu;
    }

}

/* * **************************************
 * 	FUNCIONES DE FORMATO				*
 * ************************************** */
 
if (!function_exists('getFormatDate')) {

    function getFormatDate($date, $flag) {
    	
		$meses = array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
		$fecha = substr($date, 0,10);
		$anio = substr($fecha, 2,2);
		$mes = substr($fecha, 5,2);
		$dia = substr($fecha, 8,2);
		$mes = $meses[$mes-1];
		
		if($flag) {
			$hora = substr($date, '11',8);	
			return $dia."/".$mes."/".$anio." ".$hora;
		}
		else {
			return $dia."/".$mes."/".$anio;
		}
	}
}
 

if (!function_exists('cutstr')) {

    // function acorta strings, 2 parametros: string, longitud menor deseada
    function cutstr($strtocut, $long) {
        $strtocut = trim($strtocut);
        if (strlen($strtocut) > $long) {
            $strtocut = substr($strtocut, 0, $long - 3);
            $strtocut = $strtocut . "...";
        }
        return $strtocut;
    }

}


if (!function_exists('monTOint')) {

    // funcion de Moneda a Integer
    function monTOint($cdn) {
        $cdn = trim($cdn);
        $cdn = str_replace("$", "", $cdn);
        $cdn = str_replace(",", "", $cdn);
        $cdn = intval($cdn);
        return $cdn;
    }

}

if (!function_exists('intTOmon')) {

    // funcion de integer a moneda
    function intTOmon($cdn) {
        $cdn = trim($cdn);
        $CadLen = strlen($cdn);
        $Newcdn = "";
        if ($CadLen == 0) {
            $cdn = 0;
        }
        if ($CadLen > 3) {
            $cdnDp = "G" . $cdn;
            $mmc = 0;
            for ($i = $CadLen; $i >= 1; $i--) {
                $Newcdn = $cdnDp{$i} . $Newcdn;
                $mmc++;
                if (( $mmc == 3 ) && ( $i > 1 )) {
                    $mmc = 0;
                    $Newcdn = "," . $Newcdn;
                }
            }
            $cdn = $Newcdn;
        }
        $cdn = "$" . $cdn . ".00";
        return $cdn;
    }

}

if (!function_exists('guioner')) {

    // funcion agrega todos espacios
    function guioner($url) {
//        $cdn = trim($cdn);
//        $cdn = str_replace(" ", "-", $cdn);
//        return $cdn;
        $url = strtolower($url);
        $buscar = array(' ', '&', '+');
        $url = str_replace($buscar, '-', $url);
        $buscar = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $remplzr = array('a', 'e', 'i', 'o', 'u', 'n');
        $url = str_replace($buscar, $remplzr, $url);
        $buscar = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
        $remplzr = array('', '-', '');
        $url = preg_replace($buscar, $remplzr, $url);
        return $url;
    }

}

if (!function_exists('desguioner')) {

    // funcion quita todos guiones
    function desguioner($cdn) {
        $cdn = trim($cdn);
        $cdn = str_replace("-", " ", $cdn);
        return $cdn;
    }

}

if (!function_exists('cambiar_url')) {

    function cambiar_url($url) {
        $url = strtolower($url);
        $buscar = array(' ', '&', '+');
        $url = str_replace($buscar, '-', $url);
        $buscar = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $remplzr = array('a', 'e', 'i', 'o', 'u', 'n');
        $url = str_replace($buscar, $remplzr, $url);
        $buscar = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
        $remplzr = array('', '-', '');
        $url = preg_replace($buscar, $remplzr, $url);
        return $url;
    }

}


if (!function_exists('cleanStringUrl')) {

    //Limpia una cadena y la prepara para URL
    function cleanStringUrl($cadena) {
        $cadena = strtolower($cadena);
        $cadena = trim($cadena);
        $cadena = strtr($cadena, "���̀����������ͅ���������菎�������쓒����󆝜��؄�", "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
        $cadena = strtr($cadena, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz");
        $cadena = preg_replace('#([^.a-z0-9]+)#i', '-', $cadena);
        $cadena = preg_replace('#-{2,}#', '-', $cadena);
        $cadena = preg_replace('#-$#', '', $cadena);
        $cadena = preg_replace('#^-#', '', $cadena);
        return $cadena;
    }

}

/* * **************************************
 * 	FUNCIONES PARA APIS/SERVICIOS		*
 * ************************************** */

if (!function_exists('gTranslate')) {

    // Funcion que traduce a un idioma en especial
    function gTranslate($text, $langOriginal, $langFinal) {
        //Si los idiomas son iguales no hago nada
        if ($langOriginal != $langFinal) {
            /* Definimos la URL de la API de Google Translate y metemos en la variable el texto a traducir */
            $url = 'http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=' . urlencode($text) . '&langpair=' . $langOriginal . '|' . $langFinal;
            // iniciamos y configuramos curl_init();
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            $code = curl_exec($curl_handle);
            curl_close($curl_handle);
            /* La api nos devuelve los resultados en forma de objeto stdClass */
            $json = json_decode($code)->responseData;
            $traduccion = utf8_decode($json->translatedText);
            return utf8_decode($traduccion);
        } else {
            return $text;
        }
    }

}

if (!function_exists('getTinyUrl')) {

    // Funcion que obtiene TinyURL
    function getTinyUrl($bigURL) {
        // Se crea un manejador CURL
        $ch = curl_init();
        // Se establece la URL y algunas opciones
        $urlVieja = "http://tinyurl.com/api-create.php?url=" . $bigURL;
        curl_setopt($ch, CURLOPT_URL, $urlVieja);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Se obtiene la URL indicada
        $result = curl_exec($ch);
        $resultArray = curl_getinfo($ch);
        //Si hay error manda un correo al administrador
        if ($resultArray['http_code'] == 200) {
            return $result;
        } else {
            return $bigURL;
        }
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
    }

}