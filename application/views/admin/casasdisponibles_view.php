<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }
            );
             
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/casasdisponibles/consulta_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data 	 : 'idFraccionamiento=' + $("#idFraccionamiento").val() + '&statusVenta=' + $("#statusVenta").val(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#usersTable tr").empty();
                                        $("#newUserBody").prepend(data.html);
                                                                             
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
	                            	noty({
				                            text : 'NO SE ENCONTRARON RESULTADOS CON LOS CRITERIOS DE BÚSQUEDA SELECCIONADOS',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }                             
                            }                            
                        })
                      } // cierra if status true 
                    }
                }
            );
                                    
        }
    );
</script>
<div id="contentUsers">
    
    <form class="niceform">
        <input type="button" value="Buscar" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Casas disponibles'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">
                            <option value="">- - - -</option>
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
								?>
									<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
								<?php
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="statusVenta">Estatus</label></dt>
                    <dd>
                    	<select size="1" name="statusVenta" id="statusVenta" class="validate[required]">
                            <option value="">- - - -</option>
                            <option value="1"> Disponible </option>
                            <option value="2"> Apartada </option>
                            <option value="3"> Con contrato </option>
                            <option value="4"> Escriturada </option>
                            <option value="5"> Entregada </option>
                        </select>                    
                    </dd>
                </dl>
                                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Buscar Casas"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Fraccionamiento</th> <th>N. Casa</th> <th>Prototipo</th> <th>Terreno</th> <th>Construcci&oacute;n</th> <th>Orientaci&oacute;n</th> <th>Acabados</th> <th>Precio de lista</th> <th> Venta </th> <th></th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
	
		</tbody>
	</table>
</div>                     