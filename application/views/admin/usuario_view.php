<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").stop().hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").stop().show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").stop().hide(speed);
                        open = false;                        
                    }
                }
            );
            
            //Jquery validations
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true) {
	                
                        $.ajax({
                            url      : '<?=base_url()?>admin/usuarios/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'USUARIO AGREGADO SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
                              			noty({
				                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        });      
                                }                             
                            }                            
                        })
                    } // cierra el if status true 
                    
                    
                  }
                }
            );
            
            $(".optsPane").live(
                'mouseover',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().show();
                }
            );
            
            $(".optsPane").live(
                'mouseleave',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().hide();
                }
            );
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea elminar a este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/usuarios/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idUsuario=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
            
            
            
        }
    );
    
    function touchStart(event,id) {
	  // Insert your code here
	  try{
	  	document.getElementById("optsPane"+id).style.display = 'block';
	  }
	  catch(e){ alert(e); }
	  
	}
	
</script>
<div id="contentUsers">
    <form class="niceform">
	    <input type="button" value="Nuevo usuario" class="shContent" />
    </form>
    <br />
        
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nuevo Usuario'); ?>
                <dl>
                    <dt><label for="nombre">Nombre</label></dt>
                    <dd><input type="text" name="nombre" id="nombre" value="" class="validate[required] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="email">Email</label></dt>
                    <dd><input type="text" name="email" id="email" value="" class="validate[required,custom[email]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="puesto">Puesto</label></dt>
                    <dd><input type="text" name="puesto" id="puesto" value="" class="validate[required] text-input"></dd>
                </dl>                
                
                <dl>
                    <dt><label for="fechaIngreso">Fecha de Ingreso</label></dt>
                    <dd>
                    	<input type="text" name="fechaIngreso" id="fechaIngreso" value="" readonly="readonly" class="validate[required] text-input datepicker" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="rol">Tipo</label></dt>
                    <dd>
                        <select name="rol" id="rol" class="validate[required]">
                            <option value="">- - - -</option>
                            <option value="1">Administrador</option>
                            <option value="2">Coordinador</option>
                            <option value="3">Asesor</option>
                            <option value="4">Hostess</option>
                            <option value="5">Titulacion</option>
                            <option value="6">Cordinador de obra</option>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Agregar usuario" class="submit"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    
	<table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Nombre</th> <th>Email</th> <th>Puesto</th> <th>Fecha Ingreso</th> <th>Tipo</th> <th class="optionsPane">Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($usuarios != null):
            $strong = true;
            $class = '';
            
        foreach ($usuarios as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idUsuario?>" class="<?=$class?> optsPane">
				<td><?=$key->nombre?></td>
				<td><?=$key->emailUsuario?></td>
				<td><?=$key->puesto?></td>
				<td><?=getFormatDate($key->fechaIngreso,false)?></td>
				<td>
					<?php
					    
						switch ($key->idRol) {
							case 1:
								echo "Administrador";
								break;
							case 2:
								echo "Coordinador";
								break;
							case 3:
								echo "Asesores";
								break;
							case 4:
								echo "Hostess";
								break;
							case 5:
								echo "Coordinador de obra";
								break;
							case 6:
								echo "Titulaci&oacute;n";
								break;
							case 7:
								echo "Broker";
								break;
							default:
								echo "Desconocido";
								break;
						}
                        
					?>
				</td>
				<td class="optionsPane" ontouchstart="touchStart(event,<?=$key->idUsuario?>);">
				    
				    <?php if($key->estatus == '1'): ?>
					    <span>
					        Activo
					    </span>
				    
					    <div id="optsPane<?=$key->idUsuario?>">
					        <a id="editRow<?=$key->idUsuario?>" href="<?=base_url()?>admin/usuarios/editar/<?=$key->idUsuario?>">
					            <img src="<?=base_url()?>/img/edit_row.png" />
					        </a>
					        
					        <a id="deleteRow<?=$key->idUsuario?>" href="<?=$key->idUsuario?>" class="deleteRow">				            
			                    <img src="<?=base_url()?>/img/delete_row.png" />
			                </a>				       
				    	</div>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				    
				</td>
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>              

        