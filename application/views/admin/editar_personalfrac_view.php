<script type="text/javascript">
    jQuery(document).ready(
        function($){
                        
            $("#editar").click(
                function(e){
                    e.preventDefault();
                    if($("#idFraccionamiento").val() == 0 || $("#idUsuario").val() == 0){
                        noty({
                            text : 'POR FAVOR SELECCIONE EL FRACCIONAMIENTO Y EL USUARIO QUE DESEA REGISTRAR',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                    	
                    	noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                    }
                }
            );
            
            
        }
    );
</script>
<div id="contentUsers">
  
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/personalfrac/editar_do">
        	<input type="hidden" name="idFracUsuario" id="idFracUsuario" value="<?=$info->idFracUsuario?>" />
            <?=form_fieldset('Editar usuario asignado'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento">
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
									if($info->idFraccionamiento == $key->idFraccionamiento):
									?>
											<option value="<?=$key->idFraccionamiento?>" selected="selected"> <?=$key->nombreFrac?> </option>
									<?php
										else:
									?>
										<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
									<?php
									endif;
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="idUsuario">Usuario</label></dt>
                    <dd>
                    	<select size="1" name="idUsuario" id="idUsuario">
                            <?php
                            if($usuarios != null):
								foreach ($usuarios as $key):
									if($info->idUsuario == $key->idUsuario):
									?>
										<option value="<?=$key->idUsuario?>" selected="selected"> <?=$key->nombre?> </option>
									<?php
										else:
									?>
										<option value="<?=$key->idUsuario?>"> <?=$key->nombre?> </option>
									<?php
									endif;
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Guardar" id="editar"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    
</div>                     