<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title><?= $SYS_metaTitle; ?></title>

    <meta content="text/html; charset=UTF-8" />	
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/adminLogin.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jqueryUi.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/niceforms-default.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/noty/buttons.css" type="text/css">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>imgs/favicon.ico" type="image/x-icon">
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jqueryUi.js"></script>	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/library.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/setuplabels.js"></script>
	
	
	<!-- noty -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/jquery.noty.js"></script>
  
    <!-- layouts -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottom.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomCenter.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomRight.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/center.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/centerLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/centerRight.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/inline.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/top.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topCenter.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topRight.js"></script>

    <!-- themes -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/themes/default.js"></script>
	
	<!-- Load Dynamic Libraries -->
	<?php if(isset($dinamicLibrary['autoComplete'])):?>
		 <link rel="stylesheet" href="<?php echo base_url(); ?>css/autocomplete.css" type="text/css">
		 <script type="text/javascript" src="<?php echo base_url(); ?>js/autocomplete.js"></script>	
	<?php endif ?>
	
	<script type="text/javascript">
		//Arreglo que contiene el idioma para los errores
        jQuery(document).ready(function($) {
        <?php if(isset($error) || $this->session->flashdata('error')): ?>        
            noty({
                text : '<?=$this->lang->line(((isset($error))?($error):($this->session->flashdata('error'))))?>',
                type : '<?=$this -> session -> flashdata('notyType')?>',
                dismissQueue: true,
                layout: 'top',
                theme: 'default',
                timeout: 2000                
            });
        <?php endif ?>    
        });
        //-->
    </script>
</head>
<body>     
    <div id="wrapper">
        <?=$this->load->view($module)?>        
    </div>
    
    <div id="footer">
      <p><span>iCADI | Sistema de Grupo CADI www.icadi.mx &copy; - Todos los Derechos Reservados</span></p>
      <p>Sugerencias y recomendaciones? escribenos a sistema@icadi.mx, Condiciones de Uso | Ayuda | Estado del Sistema | Politica de Privacidad |<?php echo date("r"); ?></p>
</div>
</body>
</html>