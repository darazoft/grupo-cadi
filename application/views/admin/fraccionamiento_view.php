<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }
            );
            
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/fraccionamientos/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'FRACCIONAMIENTO AGREGADO SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
	                                	noty({
				                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        });
                                }                             
                            }                            
                        })
                       } // cierra el if status true
                    }
                }
            );
            
            $(".optsPane").live(
                'mouseover',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().show();
                }
            );
            
            $(".optsPane").live(
                'mouseleave',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().hide();
                }
            );
                        
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea elminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/fraccionamientos/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idFraccionamiento=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
            
        }
    );
    		 
    function touchStart(event,id) {
	  // Insert your code here
	  try{
	  	document.getElementById("optsPane"+id).style.display = 'block';
	  }
	  catch(e){ alert(e); }
	  
	}
</script>
<div id="contentUsers">
    <form class="niceform">
	    <input type="button" value="Nuevo fraccionamiento" class="shContent" />                
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nuevo fraccionamiento'); ?>
                <dl>
                    <dt><label for="nombreFrac">Nombre</label></dt>
                    <dd><input type="text" name="nombreFrac" id="nombreFrac" value="" class="validate[required] text-input" > </dd>
                </dl>
                
                <dl>
                    <dt><label for="direccion">Direcci&oacute;n</label></dt>
                    <dd><input type="text" name="direccion" id="direccion" value="" class="validate[required] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="telefono1">Tel&eacute;fono 1:</label></dt>
                    <dd><input type="text" name="telefono1" id="telefono1" value="" class="validate[custom[integer], minSize[10], maxSize[13], groupRequired[telefono1, telefono2]] text-input"></dd>
                </dl>                
                
                <dl>
                    <dt><label for="telefono2">Tel&eacute;fono 2:</label></dt>
                    <dd><input type="text" name="telefono2" id="telefono2" value="" class="validate[custom[integer], minSize[10], maxSize[13], groupRequired[telefono1, telefono2]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="unidades">Unidades:</label></dt>
                    <dd><input type="text" name="unidades" id="unidades" value="" class="validate[required, custom[integer], minSize[1], maxSize[3], min[1]] text-input"></dd>
                </dl>
                                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Agregar fraccionamiento" class="submit"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Nombre</th> <th>Direcci&oacute;n</th> <th>Tel&eacute;fono 1</th> <th>Tel&eacute;fono 2</th> <th>Unidades</th> <th>Fecha de Registro</th> <th class="optionsPane">Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($fraccionamientos != null):
            $strong = true;
            $class = '';
            
        foreach ($fraccionamientos as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idFraccionamiento?>" class="<?=$class?> optsPane">
				<td><?=$key->nombreFrac?></td>
				<td><?=$key->direccion?></td>
				<td><?=$key->telefono1?></td>
				<td><?=$key->telefono2?></td>
				<td><?=$key->unidades?></td>
				<td><?=getFormatDate($key->fechaRegistro,true)?></td>
				
				<td class="optionsPane" ontouchstart="touchStart(event,<?=$key->idFraccionamiento?>);">
				    
				    <?php if($key->statusFrac == '1'): ?>
					    <span class="">
					        Activo
					    </span>
				    
					    <div id="optsPane<?=$key->idFraccionamiento?>">
					        <a id="editRow<?=$key->idFraccionamiento?>" href="<?=base_url()?>admin/fraccionamientos/editar/<?=$key->idFraccionamiento?>">
					            <img src="<?=base_url()?>img/edit_row.png" />
					        </a>
					        
					        <a id="deleteRow<?=$key->idFraccionamiento?>" href="<?=$key->idFraccionamiento?>" class="deleteRow">				            
			                    <img src="<?=base_url()?>img/delete_row.png" />
			                </a>				       
				    	</div>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				</td>
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
	
</div>              

        