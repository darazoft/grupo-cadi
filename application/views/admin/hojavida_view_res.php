<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(                
                function(){                    
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }                
            );
            
            //Jquery validations            
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine(
            'attach',
            {
            	promptPosition : "bottomLeft", scroll: false,
            	
          		onValidationComplete: function(form, status){
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/prospecto/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true') {
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'PROSPECTO AGREGADO SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
									
									window.location.href = '<?=base_url()?>admin/prospecto/actividades/' + data.lastID;                                                            
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
                                	
                                	noty({
				                            text : 'CONTACTA A TU COORDINADOR, EL NOMBRE DEL PROSPECTO YA SE ENCUENTRA REGISTRADO',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        });
                                }                             
                            }                            
                        })
                      } //cierra if status true 
                   }
                    
               }
               
            );
            
            
            $("#q2").change (function(){
            	
            	if( $(this).val() == 8 ) {
            		
            		$("#q2otro").show();
            	}
            	else {
            		$("#q2otro").hide();
            		$('#q2otro').validationEngine('hide');
            	}
            });
            
            $("#q1").change (function(){
            	
            	if( $(this).val() == 1 ) {
            		
            		$("#asesor").show();
            	}
            	else {
            		$("#asesor").hide();
            		$('#asesor').validationEngine('hide');
            	}
            });
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea eliminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/prospecto/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idHojaVida=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
        }
    );
</script>



<div id="contentUsers">
    <form class="niceform">
        <input type="button" value="Nuevo Prospecto" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nuevo Prospecto '); ?>
                <table style="position:relative; z-index: 1;">
                	<tr>
                		<td align="right" colspan="3">
                            <label for="idEstatusCliente">Clasificaci&oacute;n</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select name="idEstatusCliente" id="idEstatusCliente" class="validate[required]">
                                <option value="">- - - -</option>
                                <?php
                                if($clasificacion != null):
                                    foreach ($clasificacion as $key):
                                    ?>
                                        <option value="<?=$key->idEstatusCliente?>"> <?=$key->clasificacion?> </option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>                            
                            </select>
                            </div>
                        </td>
                	</tr>
                    <tr>
                        <td align="right">
                            <label for="idFraccionamiento">Desarrollo</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">
                                <option value="">- - - -</option>
                                <?php
                                if($fraccionamientos != null):
                                    foreach ($fraccionamientos as $key):
                                    ?>
                                        <option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>                            
                            </select>
                            </div>
                        </td>
                        
                        <td align="right">
                            <label for="idUsuario">Asesor</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="idUsuario" id="idUsuario" class="validate[required]">
                                <option value="">- - - -</option>
                                <?php
                                if($asesores != null):
								?>
									<optgroup label="Asesores">
								<?php
                                    foreach ($asesores as $key):
                                    ?>
                                        <option value="<?=$key->idUsuario?>"> <?=$key->nombre?> </option>
                                    <?php
                                    endforeach;
									?>
									</optgroup>
								<?php
                                endif;
                                
                                if($coordinadores != null):
								?>
									<optgroup label="Coordinadores">
								<?php
                                    foreach ($coordinadores as $key):
                                    ?>
                                        <option value="<?=$key->idUsuario?>"> <?=$key->nombre?> </option>
                                    <?php
                                    endforeach;
									?>
									</optgroup>
								<?php
                                endif;
                                ?>
                            </select>
                          	</div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="tipo">Tipo</label></td>
                        <td>
                        	<div class="divhojadd"> 
                            <select size="1" name="tipo" id="tipo" class="validate[required]">
                                <option value="">- - - -</option>
                                <option value="1">Visita</option>
                                <option value="2">LLamada</option>
                                <option value="3">Cambaceo</option>
                                <option value="4">Internet</option>
                                <option value="5">Regreso</option>
                                <option value="6">Referido</option>
                            </select>
                            </div>
                        </td>
                        
                        <td align="right"><label for="fecha">Fecha</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="fecha" id="fecha" value="" readonly="readonly" class="validate[required] text-input datepicker" /> </div> </td>                
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="hora">Hora 24Hrs.</label></dt>
                        <td> <div class="divhojadd"> <input type="text" name="hora" id="hora" value="" class="validate[required] text-input" /> </div></td>
                    
                        <td align="right"><label for="numPase"> &nbsp; </label></td>
                        <td> &nbsp; </td>
                    </tr>
                    
                    <tr>
                    	<td align="right"><label for="apaterno">Apellido paterno</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="apaterno" id="apaterno" value="" class="validate[required] text-input" /> </div></td>
                        <td align="right"><label for="amaterno">Apellido materno</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="amaterno" id="amaterno" value="" class="validate[required] text-input" /> </div></td>
                    </tr>
                    
                    <tr>
                    	<td align="right"><label for="nombreCliente">Nombre</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nombreCliente" id="nombreCliente" value="" class="validate[required] text-input" /> </div></td>
                        <td align="right"><label for="email">Email</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="email" id="email" value="" class="validate[custom[email]] text-input" /> </div></td>
                    </tr>
					
					<tr>
                    	<td align="right"><label for="ladaFijo">Lada Tel Fijo</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="ladaFijo" id="ladaFijo" value="" class="validate[custom[integer], minSize[2], maxSize[3], groupRequired[ladaFijo, ladaCelular]] text-input" /> </div> </td>
                            
                        <td align="right"><label for="ladaCelular">Lada Celular</label></td>
                        <td> 
                        	<div class="divhojadd"> 
                        		<select name="ladaCelular" class="validate[required, groupRequired[ladaFijo, ladaCelular]] ">
                        			<option value="">---</option>
    								<option value="044">044</option>
    								<option value="045">045</option>
                        		</select> 
                        	</div>
                        </td>
                    </tr>
                                        
                    <tr>
                    	<td align="right"><label for="telFijo">Tel&eacute;fono Fijo</label></td>
                        <td> <div class="divhojadd">	<input type="text" name="telFijo" id="telFijo" value="" class="validate[custom[integer], minSize[7], maxSize[8], groupRequired[telFijo, celular]] text-input" /> </div> </td>    
                        <td align="right"><label for="celular">Celular</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="celular" id="celular" value="" class="validate[custom[integer], minSize[10], maxSize[10], groupRequired[telFijo, celular]] text-input" /> </div></td>
                    </tr>
                    
                    <tr>                        
                        <td  align="right" colspan="3"><label>&iquest;Nos hab&iacute;a visitado, llamado o conoce algunos de nuestros desarrollos? </label></td>
                        <td class="hojadd" colspan="2">
                        	<div class="divhojadd">
                            <select size="1" name="q1" id="q1" class="validate[required]">
                                <option value="">----</option>
                                <option value="1">Si</option>
                                <option value="2">No</option>
                            </select>
                            </div>
                            <div class="divhojadd"> <input type="text" id="asesor" name="asesor" class="validate[required] input-text" style="display: none;" /> </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label>&iquest;C&oacute;mo se enter&oacute; de nosotros? </label></td>
                        <td colspan="2"> 
                        	<div class="divhojadd">
                        		<select size="1" name="q2" id="q2" class="validate[required]">
	                                <option value="">----</option>
	                                <option value="1">De paso</option>
	                                <option value="2">Internet</option>
	                                <option value="3">Recomendaci&oacute;n</option>
	                                <option value="4">Volanteo</option>
	                                <option value="5">Promoci&oacute;n</option>
	                                <option value="6">Folleto/Revista</option>
	                                <option value="7">Radio/TV</option>
	                                <option value="8">Otro</option>
                            	</select>
                            	<input type="text" name="q2otro" id="q2otro" value="" class="validate[required] input-text" style="display: none;" /> 
                        	</div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="nombreConyuge">Nombre de esposa(o) </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nombreConyuge" id="nombreConyuge" value="" /> </div></td>
                    
                        <td align="right"><label for="q3">&iquest;De d&oacute;nde nos visita? </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q3" id="q3" value="" /> </div> </td>
                    </tr>
                    <!--  FROM HOSTES-->                    
                    <tr>
                        <td align="right"><label for="q4">La busca para: </label></dt>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q4" id="q4" class="">
                                <option value="">----</option>
                                <option value="1">Vivir</option>
                                <option value="2">Invertir</option>
                                <option value="3">Fam o Amigo</option>
                            </select> 
                            </div>
                        </td>
                    
                        <td align="right"><label for="q5">&iquest;Cu&aacute;l es su presupuesto? </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q5" id="q5" value="" class=""/> </div></td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="q6">Recamaras necesarias </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q6" id="q6" value="" /> </div></td>
                        
                        <td><label for="q7">&iquest;C&oacute;mo piensa adquirirla? </label></td>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q7" id="q7" class="">
                                <option value=""> ---- </option>
                                <option value="1"> Contado </option>
                                <option value="2"> Bancario </option>
                                <option value="3"> Cofinavit </option>
                                <option value="4"> Infonavit </option>
                                <option value="5"> Aliados </option>
                                <option value="6"> Fovissste </option>
                                <option value="7"> CCadi </option>
                            </select>
                            </div>
                        </td>                        
                    </tr>                                        
                    
                    <tr>
                        <td align="right"><label for="nss">NSS </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nss" id="nss" value="" /> </div></td>
                        
                        <td align="right"><label for="curp"> CURP </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="curp" id="curp" value="" /> </div></td>                
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="ingresoComprobable">Ingresos comprobables $</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="ingresoComprobable" id="ingresoComprobable" value="" class="validate[custom[integer]] text-input" /> </div> </td>
                    
                        <td align="right"><label for="entregaRequerida">Entrega requerida </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="entregaRequerida" id="entregaRequerida" value="" readonly="readonly" class="text-input datepicker" /> </div></td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label for="q8">&iquest;Le gust&oacute; nuestra casa muestra? </label></td>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q8" id="q8">
                                <option value="0">----</option>
                                <option value="1">Si</option>
                                <option value="2">No</option>
                            </select>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label for="q9">&iquest;Cu&aacute;l ubicaci&oacute;n le gust&oacute;? </label></td>
                        <td>
                        	<div class="divhojadd">
	                        <select size="1" name="q9" id="q9" class="">
	                            <option value="">----</option>
	                            <option value="1">Norte</option>
	                            <option value="2">Sur</option>
	                            <option value="3">Oriente</option>
	                            <option value="4">Poniente</option>
	                            <option value="5">Primera Etapa</option>
	                            <option value="6">Segunda Etapa</option>
	                            <option value="7">Area Com&uacute;n</option>
	                        </select>
	                        </div>
                        </td>
                    </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q10">&iquest;Es lo que est&aacute; buscando? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q10" id="q10" class="">
                            <option value="">----</option>
                            <option value="1">Si</option>
                            <option value="2">No</option>
                        </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3"><label for="q10xque">&iquest;Por qu&eacute;? </label></td>
                    <td> <div class="divhojadd"> <input type="text" idaccept="q10xque" name="q10xque" class="" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q11">&iquest;Est&aacute; dentro de su plan de inversi&oacute;n? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q11" id="q11" class="">
                            <option value="">----</option>
                            <option value="1">Si</option>
                            <option value="2">No</option>
                        </select>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q12">&iquest;Cu&aacute;l es su inversi&oacute;n inicial? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q12" id="q12" value="" class="validate[custom[integer]] text-input" /> <div> </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q13">&iquest;Qu&eacute; le gust&oacute; de nuestro desarrollo? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q13" id="q13" value="" class="" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q14">&iquest;Qu&eacute; no le gust&oacute; de nuestro desarrollo? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q14" id="q14" value="" class="" /> </div> </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q15">&iquest;De qu&eacute; depende o qu&eacute; necesita para decidir? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q15" id="q15" value="" class="" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q16">&iquest;Qu&eacute; le pareci&oacute; el precio? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q16" id="q16" class="">
                            <option value="">----</option>
                            <option value="1">Alto</option>
                            <option value="2">Bien</option>
                            <option value="3">Bajo</option>
                        </select>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q17">&iquest;En qu&eacute; qued&oacute; el cliente? </label></td>
                    <td> <div class="divhojadd"> <textarea name="q17" id="q17" class="validate[required] text-input"></textarea> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="observaciones">Observaciones </label></td>
                    <td> <textarea name="observaciones" id="observaciones"></textarea></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label>&nbsp;</label></td>
                    <td><input type="submit" value="Agregar Prospecto" /></td>
                </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Frac.</th> <th>Asesor</th> <th>Cliente</th> <!--<th>Email</th>--> <th>Tel&eacute;fono</th> <th>Celular</th> <th>Estatus</th> <th>Acciones</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($prospectos != null):
            $strong = true;
            $class = '';
            
        foreach ($prospectos as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idHojaVida?>" class="<?=$class?>">
				<td><?=$key->nombreFrac?></td>
				<td><?=$key->nombre?></td>
				<td><?=$key->cliente?></td>
				<td><?=$key->ladaFijo." ".$key->telFijo?></td>
				<td><?=$key->ladaCelular." ".$key->telCelular?></td>
				<td><?=$key->clasificacion?></td>
				
				<td align="right">
				    <a href="<?=base_url()?>admin/prospecto/editar/<?=$key->idHojaVida?>">
				        <img src="<?=base_url()?>img/edit_row.png" alt="Editar" />
				    </a>
				    &nbsp;
				    <a href="<?=base_url()?>admin/prospecto/actividades/<?=$key->idHojaVida?>">
                        <img src="<?=base_url()?>img/task_row.png" alt="Actividades" />
                    </a>
				    &nbsp;
				    <a id="deleteRow<?=$key->idHojaVida?>" href="<?=$key->idHojaVida?>" class="deleteRow">
                        <img src="<?=base_url()?>img/delete_row.png" alt="Eliminar" />
                    </a>
				</td>
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>              
