<script type="text/javascript">
    jQuery(document).ready(
        function($){
                       
            $("#editar").click(
                function(e){
                    e.preventDefault();
                    if($("#nombreFrac").val() == '' || $("#direccion").val() == '' || ($("#telefono1").val() == '' && $("#telefono2").val() == '') || $("#unidades").val() == ''){
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DEL FRACCIONAMIENTO Y AL MENOS UN TELEFONO',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                        noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                    }
                }
            );
         
        }
    );
</script>
<div id="contentUsers">
 
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/fraccionamientos/editar_do">
        	<input type="hidden" name="idFraccionamiento" id="idFraccionamiento" value="<?=$info->idFraccionamiento?>">
        	
            <?=form_fieldset('Editar fraccionamiento'); ?>
                <dl>
                    <dt><label for="nombreFrac">Nombre</label></dt>
                    <dd><input type="text" name="nombreFrac" id="nombreFrac" value="<?=$info->nombreFrac?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="direccion">Direcci&oacute;n</label></dt>
                    <dd><input type="text" name="direccion" id="direccion" value="<?=$info->direccion?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="telefono1">Tel&eacute;fono 1:</label></dt>
                    <dd><input type="text" name="telefono1" id="telefono1" value="<?=$info->telefono1?>"></dd>
                </dl>                
                
                <dl>
                    <dt><label for="telefono2">Tel&eacute;fono 2:</label></dt>
                    <dd><input type="text" name="telefono2" id="telefono2" value="<?=$info->telefono2?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="unidades">Unidades:</label></dt>
                    <dd><input type="text" name="unidades" id="unidades" value="<?=$info->unidades?>"></dd>
                </dl>
                                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
                    </dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>
</div>              

        