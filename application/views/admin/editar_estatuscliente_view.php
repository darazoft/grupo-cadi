<script type="text/javascript">
    jQuery(document).ready(
        function($){
                        
            $("#editar").click(
                function(e){
                    e.preventDefault();
                    if($("#clasificacion").val() == '' || $("#descripcion").val() == ''){
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DE ESTATUS DEL CLIENTE',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                    	
                        noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                    }
                }
            );
            
        }
    );
</script>
<div id="contentUsers">
    
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/estatuscliente/editar_do">
        	<input type="hidden" name="idEstatusCliente" id="idEstatusCliente" value="<?=$info->idEstatusCliente?>">
            <?=form_fieldset('Editar estatus'); ?>
               <dl>
                    <dt><label for="clasificacion">Clasificaci&oacute;n</label></dt>
                    <dd><input type="text" name="clasificacion" id="clasificacion" value="<?=$info->clasificacion?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="descripcion">Descripci&oacute;n</label></dt>
                    <dd><textarea name="descripcion" id="descripcion"><?=$info->descripcion?></textarea></dd>
                </dl>
                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
                    </dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    	
</div>              

        