<script type="text/javascript">
    jQuery(document).ready(
        function($){
        	
        	$(".showMessages").click(
                function(){
                    var thisID = $(this).attr('headers');
                    $(".allContainer:not(#allContainer_+"+thisID+")").hide();
                    $("#allContainer_"+thisID).toggle();                    
                }
            );            
            
            jQuery(".addMessageForm").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
            
            $(".addMessageForm").submit(
                function(e) {
                	e.preventDefault();
                	
                	var formID = $(this).attr('id');
                    if(!$("#"+formID).validationEngine('validate')){
		         		return false;
		          	}
		          	
                    var commentID = $(this).attr('data-rel');
                    var textoID = $("#nuevoComment"+commentID).val();
					
		          	$("#submit_"+commentID).attr('disabled', 'disabled');
		          			          	
		          	$.ajax({
                        url : '<?=base_url()?>admin/prospecto/nuevo_mensaje_do',
                        type : 'POST',
                        dataType : 'json',
                        data : 'idHojaActividad=' + commentID + '&comentario='+textoID,
                        success: function(data) {
                        	
                            if(data.response == 'true'){
                    			$("#boxComments_" + commentID).prepend(data.html);
                    			                    			
                    			$("#"+formID).each (function(){
									  this.reset();
								});
								$("#submit_"+commentID).removeAttr('disabled');
								            
                            }
                            else if(data.response=='error_val') {
                                
	                        		noty({
			                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
			                            type : 'error',
			                            dismissQueue: true,
			                            layout: 'top',
			                            theme: 'default',
			                            timeout: 4000
			                        }); 	   
                            }          
                            else if(data.response=='false') {
                          			noty({
			                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
			                            type : 'error',
			                            dismissQueue: true,
			                            layout: 'top',
			                            theme: 'default',
			                            timeout: 4000
			                        });      
                            }
                        }
                   });
                      
                                                                                                                      
                }
            );

            
        }
    );
</script>



<div id="contentUsers">
	
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th colspan="3">Fecha</th> <th>Fraccionamiento</th> <th>Cliente</th> <th>Actividad</th><th>Tel&eacute;fono</th> <th>Celular</th> <th>Fecha de Alta de Prospecto</th> <th>D&iacute;as en Seguimiento</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($actividades != null):
            $strong = true;
            $class = '';
            
        foreach ($actividades as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idHojaVida?>" class="<?=$class?>">
				
				<td>
				<?php
					if($key->estado == 'yellow') :
					?>
						<a href="<?=base_url()?>admin/prospecto/editar_estado_actividad/<?=$key->idHojaActividad?>/<?=$key->idHojaVida?>" title="Pendiente">
							<img src="<?=base_url()?>img/yellow.png" alt="Estado" /> </span>
						</a>
						&nbsp;
					<?php
					elseif($key->estado == 'red') :
					?>
						<a href="<?=base_url()?>admin/prospecto/editar_estado_actividad/<?=$key->idHojaActividad?>/<?=$key->idHojaVida?>" title="Vencida">
                            <img src="<?=base_url()?>img/red.png" alt="Estado" />                            
                        </a>
						&nbsp;
					<?php
					endif;
                    ?>
				</td>
				<td>
				<?
					if($arrCount[$key->idHojaActividad] != null):
						foreach($arrCount[$key->idHojaActividad] as $row):
							if($row->total > 0):
                ?>
			                	<div align="left" style="font-family: 'Trebuchet MS'; color: red; font-size:13px; width:32px; height:32px; background:url(<?=base_url()?>img/messages_counter.png) left top no-repeat; padding-left:4px; padding-top:3px;">
							        <?=$row->total?>
							    </div>
                <?
							endif;
						endforeach;
                	endif;
                ?>
				</td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=getFormatDate($key->fechaInicio,true)?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=$key->nombreFrac?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=$key->cliente?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;">
					<?php
					switch ($key->nombreActividad) :
						case 1:	echo "LLamada";	break;
						case 2:	echo "Cita"; break;
						case 3:	echo "Email"; break;
						case 4:	echo "Firma de contrato"; break;
						case 5:	echo "Pago de apartado"; break;
						case 6:	echo "Cancelaci&oacute;n"; break;
						case 7:	echo "Seguimiento postventa"; break;
						case 8:	echo "Atenci&oacute;n a queja";	break;
						case 9:	echo "seguimeinto cobranza"; break;
						case 10: echo "Entrega de casa"; break;
						case 11: echo "Firma de escritura";	break;
						case 12: echo "Otro"; break;
					endswitch;
					?>
				</td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=$key->telFijo?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=$key->telCelular?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer;"><?=getFormatDate($key->fechaRegistro,true)?></td>
				<td class="showMessages" headers="<?=$key->idHojaActividad?>" style="cursor: pointer; color: red; font-weight: bold;"><?=$key->dias?></td>
			</tr>
			
			<tr id="allContainer_<?=$key->idHojaActividad?>" class="<?=$class?> allContainer" style="display: none;">
	            <td colspan="6">
	                <table id="boxComments_<?=$key->idHojaActividad?>" style="width: 750px;">
	                    <? if($arrComments[$key->idHojaActividad] != null):
	                           foreach($arrComments[$key->idHojaActividad] as $row): ?>
	                               <tr>
	                               	   <td colspan="2"><?=$row->comentario?></td>
	                                   <td colspan="2"><?=$row->nombre?></td>
	                                   <td colspan="2"><?=$row->fechaRegistro?></td>
	                               </tr>                              
	                           <? endforeach;
	                       endif; ?>                                            
	                </table>
	                <table id="boxaddComments_<?=$key->idHojaActividad?>" style="width: 750px;">
	                    <tr id="boxForm_<?=$key->idHojaActividad?>">
	                        <td>&nbsp;</td>
	                        <td colspan="4">
	                            <form id="formMessage<?=$key->idHojaActividad?>" data-rel="<?=$key->idHojaActividad?>" class="niceform addMessageForm">
	                            Comentario: <textarea name="nuevoComment<?=$key->idHojaActividad?>" id="nuevoComment<?=$key->idHojaActividad?>" style="width: 735px !important; height: 50px !important" class="validate[required] input-text"></textarea>
	                                <br />
	                                <br />
	                                <input type="submit" value="Agregar comentario" id="submit_<?=$key->idHojaActividad?>" />
	                                <br />
	                                &nbsp;                            
	                            </form>                                    
	                        </td>
	                    </tr>                            
	                </table>                        
	            </td>      
	        </tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>              
