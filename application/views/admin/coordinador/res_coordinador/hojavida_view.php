<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(                
                function(){                    
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }                
            );
            
            $("#newUser").submit(
                function(e){
                    e.preventDefault();
                    if( $("#idEstatusCliente").val() == 0 || $("#idFraccionamiento").val() == 0 || $("#idUsuario").val() == 0 || $("#cliente").val() == '' 
                    	|| $("#email").val() == '' || ($("#telFijo").val() == '' && $("#celular").val() == '') || $("#tipo").val() == 0 || $("#q1").val() == 0 
                    	|| $("#q4").val() == 0 || $("#q5").val() == '' || $("#q7").val() == 0 || $("#q9").val() == 0 || $("#q10").val() == 0 || $("#q10xque").val() == ''
                    	|| $("#q11").val() == 0 || $("#q12").val() == '' || $("#q13").val() == '' || $("#q14").val() == '' || $("#q15").val() == '' || $("#q16").val() == 0) {
                    		
                        noty({
                            text : 'POR FAVOR INGRESE LOS DATOS REQUERIDOS DEL PROSPECTO',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                        $.ajax({
                            url      : '<?=base_url()?>admin/prospecto/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : $(this).serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'PROSPECTO AGREGADO SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
									
									window.location.href = '<?=base_url()?>admin/prospecto/actividades/' + data.lastID;
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
                                	
                                	noty({
				                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        });
                                }                             
                            }                            
                        })
                    }
                }
            );
            
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea eliminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/prospecto/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idHojaVida=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
        }
    );
</script>



<div id="contentUsers">
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Frac.</th> <th>Asesor</th> <th>Cliente</th> <!--<th>Email</th>--> <th>Tel&eacute;fono</th> <!--<th>Celular</th>--> <th>Fecha de registro</th> <th>Estatus</th> <th>Acciones</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($prospectos != null):
            $strong = true;
            $class = '';
            
        foreach ($prospectos as $frac):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; 
            
				foreach($frac as $key):
	            ?>		    		    
				<tr id="<?=$key->idHojaVida?>" class="<?=$class?>">
					<td><?=$key->nombreFrac?></td>
					<td><?=$key->nombre?></td>
					<td><?=$key->cliente?></td>
					<!-- <td><?=$key->email?></td> -->
					<td><?=$key->telFijo?></td>
					<!-- <td><?=$key->telCelular?></td> -->
					<td><?=substr($key->fechaRegistro,0,10)?></td>
					<td>
					    <?php if($key->statusHoja == '1'): ?>
					        Activo
					    <?php else: ?>
					        Inactivo
					    <?php endif; ?>
					</td>
					<td align="center">
						<a href="<?=base_url()?>admin/prospecto/editar/<?=$key->idHojaVida?>">
					        <img src="<?=base_url()?>img/edit_row.png" alt="Editar" />
					    </a>
					    &nbsp;
					    <a href="<?=base_url()?>admin/prospecto/actividades/<?=$key->idHojaVida?>">
	                        <img src="<?=base_url()?>img/task_row.png" alt="Actividades" />
	                    </a>
					</td>
				</tr>
			<?php
				endforeach; 
			endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>              
