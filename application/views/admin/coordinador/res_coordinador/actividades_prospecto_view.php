<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(                
                function(){                    
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }                
            );
            
            $("#agregar").click(
                function(e) {
                	
                	e.preventDefault();
                    if( $("#fechaInicio").val() == '' || $("#fechaFin").val() == '' || $("#nombreActividad").val() == '' || $("#descripcion").val() == '' ) {
                    		
                        noty({
                            text : 'POR FAVOR INGRESE LOS DATOS REQUERIDOS DE LA ACTIVIDAD',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } 
                    else{
                    	$("#newUser").submit();
                    	return true;
                    }
                }
            );
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea eliminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/prospecto/eliminar_actividad',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idHojaActividad=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
            
        }
    );
</script>



<div id="contentUsers">
    <form class="niceform">
        <input type="button" value="Nueva Actividad" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform" action="<?=base_url()?>admin/prospecto/nueva_actividad_do" enctype="multipart/form-data">
            <?=form_fieldset('Nueva Actividad'); ?>
                <table>
                    <tr>
                        <td align="right">
                            <label for="fechaInico">Inicio:</label>
                        </td>
                        
                        <td>
                            <input type="hidden" name="idHojaVida" value="<?=$this_prospecto->idHojaVida?>" />
                            <input type="text" name="fechaInicio" id="fechaInicio" onfocus="displayCalendar(document.forms[1].fechaInicio,'yyyy-mm-dd',this)" readonly="readonly" />                            
                        </td>
                        
                        <td align="right">
                            <label for="fechaFin">Fin:</label>
                        </td>
                        
                        <td>
                            <input type="text" name="fechaFin" id="fechaFin" onfocus="displayCalendar(document.forms[1].fechaFin,'yyyy-mm-dd',this)" readonly="readonly" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="horaInicio">Hora inicio:</label>
                        </td>
                        
                        <td>
                            <select name="horaInicio" id="horaInicio">
                                <?php for($i = 0; $i<=24; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td>
                        
                        <td align="right">
                            <label for="horaFin">Hora fin:</label>
                        </td>
                        
                        <td>
                            <select name="horaFin" id="horaFin">
                                <?php for($i = 0; $i<=24; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="minutoInicio">Minuto inicio:</label>
                        </td>
                        
                        <td>
                            <select name="minutoInicio" id="minutoInicio">
                                <?php for($i = 0; $i<=59; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td>
                        
                        <td align="right">
                            <label for="minutoFin">Minuto fin:</label>
                        </td>
                        
                        <td>
                            <select name="minutoFin" id="minutoFin">
                                <?php for($i = 0; $i<=59; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="nombreActividad">Actividad:</label>
                        </td>
                        
                        <td>
                            <select name="nombreActividad" id="nombreActividad">
                                <option value="0" > - - - - </option>
                                <option value="1" >Llamada</option>
                                <option value="2" >Cita</option>
                                <option value="3" >Email</option>
                                <option value="4" >Firma de contrato</option>
                                <option value="5" >Pago de apartado</option>
                                <option value="6" >Cancelaci&oacute;n</option>
                                <option value="7" >Seguimiento post-venta</option>
                                <option value="8" >Atenci&oacute;n a queja</option>
                                <option value="9" >Seguimiento a cobranza</option>
                                <option value="10">Entrega de casa</option>
                                <option value="11">Firma de escritura</option>
                                <option value="12">Otra</option>
                            </select>
                        </td>
                        
                        <td align="right">
                            <label for="descripcion">Descripcion:</label>
                        </td>
                        
                        <td>
                            <textarea name="descripcion" id="descripcion"></textarea>                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="archivo">Archivo:</label>
                        </td>
                        
                        <td colspan="3">
                            <input type="file" name="archivo" id="archivo" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label>&nbsp;</label></td>
                        <td><input type="submit" value="Agregar Actividad" id="agregar" /></td>
                    </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">     
        <thead>
            <tr>
                <th>Actividad</th> <th>Fecha Inicio</th> <th>Fecha Fin</th> <th>Documento</th> <th>Acciones</th>
            </tr>           
        </thead>
        <tbody id="newUserBody">
        <?php
        $strong = true;
        $class = 'strong';
        
        if($actividades != null):
            foreach ($actividades as $key):
                $stringActividad = '';
                switch($key->nombreActividad):
                    case '1':
                        $stringActividad = 'Llamada';                        
                        break;
                    case '2':
                        $stringActividad = 'Cita';                        
                        break;
                    case '3':
                        $stringActividad = 'Email';                        
                        break;
                    case '4':
                        $stringActividad = 'Firma de contrato';                        
                        break;
                    case '5':
                        $stringActividad = 'Pago de apartado';                        
                        break;
                    case '6':
                        $stringActividad = 'Cancelaci&oacute;n';                        
                        break;
                    case '7':
                        $stringActividad = 'Seguimiento post-venta';                        
                        break;
                    case '8':
                        $stringActividad = 'Atenci&oacute;n a queja';                        
                        break;
                    case '9':
                        $stringActividad = 'Seguimiento cobranza';                        
                        break;
                    case '10':
                        $stringActividad = 'Entrega de casa';                        
                        break;
                    case '11':
                        $stringActividad = 'Firma de escritura';                        
                        break;
                    case '12':
                        $stringActividad = 'Otro';                        
                        break;
                    default:
                        $stringActividad = 'Otro';
                        break;
                endswitch;
                if($strong):
                    $class = 'strong';
                    $strong = false;
                elseif(!$strong):
                    $class = 'light';
                    $strong = true;
                endif; ?>                       
                <tr id="<?=$key->idHojaActividad?>" class="<?=$class?>">
                    <td> <?=$stringActividad?> </td>
                    <td> <?=$key->fechaInicio?> </td>
                    <td> <?=$key->fechaFin?> </td>
                    <td align="center" style="text-align: center !important;">
                        <?php if($key->adjunto != null): ?>
                            <a href="<?=base_url()?>docs/actividades/<?=$key->adjunto?>" target="_new" title="Ver Adjunto">
                                <img src="<?=base_url()?>img/attachment.png" alr="Un dato adjunto" />                                
                            </a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?=$key->idHojaActividad?>">
                            <img src="<?=base_url()?>img/comments.png" alr="Comentarios" />                            
                        </a>
                        &nbsp;
                        <a href="<?=base_url()?>admin/prospecto/editar_actividad/<?=$key->idHojaActividad?>">
                            <img src="<?=base_url()?>img/edit_row.png" alr="Editar" />                            
                        </a>
                        &nbsp;
                        <a id="deleteRow<?=$key->idHojaActividad?>" href="<?=$key->idHojaActividad?>" class="deleteRow">
                            <img src="<?=base_url()?>img/delete_row.png" alr="Eliminar" />                            
                        </a>
                    </td>     
                </tr>
        <?php 
               endforeach;
        else: ?>
            <tr class="<?=$class?>">
                <td colspan="5"> A&Uacute;N NO SE HAN AGENDADO ACTIVIDADES PARA ESTE PROSPECTO </td>                
            </tr>                  
        <?endif;
        ?>
        </tbody>
    </table>
</div>