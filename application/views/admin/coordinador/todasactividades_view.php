<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }
            );
    		
    		$( ".datepicker" ).datepicker();         
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/agenda/consulta_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data 	 : 'idFraccionamiento=' + $("#idFraccionamiento").val() + '&fechaInicio=' + $("#fechaInicio").val()+'&fechaFin='+$("#fechaFin").val(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#usersTable tr").empty();
                                        $("#newUserBody").prepend(data.html);
                                                                             
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
	                            	noty({
				                            text : 'NO SE ENCONTRARON RESULTADOS CON LOS CRITERIOS DE BÚSQUEDA SELECCIONADOS',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }                             
                            }                            
                        })
                      } // cierra if status true 
                    }
                }
            );
            
            
            /**************/
            $(".showMessages").live(
            	'click',
                function(){
                    var thisID = $(this).attr('headers');
                    $(".allContainer:not(#allContainer_+"+thisID+")").hide();
                    $("#allContainer_"+thisID).toggle();                    
                }
            );            
            
            jQuery(".addMessageForm").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
            
            $(".addMessageForm").live(
            	'submit',
                function(e) {
                	e.preventDefault();
                	
                	var formID = $(this).attr('id');
                    if(!$("#"+formID).validationEngine('validate')){
		         		return false;
		          	}
		          	
                    var commentID = $(this).attr('data-rel');
                    var textoID = $("#nuevoComment"+commentID).val();
					
		          	$("#submit_"+commentID).attr('disabled', 'disabled');
		          			          	
		          	$.ajax({
                        url : '<?=base_url()?>admin/prospecto/nuevo_mensaje_do',
                        type : 'POST',
                        dataType : 'json',
                        data : 'idHojaActividad=' + commentID + '&comentario='+textoID,
                        success: function(data) {
                        	
                            if(data.response == 'true'){
                    			$("#boxComments_" + commentID).prepend(data.html);
                    			                    			
                    			$("#"+formID).each (function(){
									  this.reset();
								});
								$("#submit_"+commentID).removeAttr('disabled');
								            
                            }
                            else if(data.response=='error_val') {
                                
	                        		noty({
			                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
			                            type : 'error',
			                            dismissQueue: true,
			                            layout: 'top',
			                            theme: 'default',
			                            timeout: 4000
			                        }); 	   
                            }          
                            else if(data.response=='false') {
                          			noty({
			                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
			                            type : 'error',
			                            dismissQueue: true,
			                            layout: 'top',
			                            theme: 'default',
			                            timeout: 4000
			                        });      
                            }
                        }
                   });
                      
                                                                                                                      
                }
            );
                                    
        }
    );
</script>
<div id="contentUsers">
    
    <form class="niceform">
        <input type="button" value="Buscar" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Todas las actividades'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">
                            <option value="">- - - -</option>
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
								?>
									<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
								<?php
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaInicio">Fecha de Inicio</label></dt>
                    <dd>
                    	<input type="text" name="fechaInicio" id="fechaInicio" value="" readonly="readonly" class="validate[required] text-input datepicker" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaFin">Fecha de Fin</label></dt>
                    <dd>
                    	<input type="text" name="fechaFin" id="fechaFin" value="" readonly="readonly" class="validate[required, future[fechaInicio]] text-input datepicker" />
                    </dd>
                </dl>
                                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Buscar Actividades"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th colspan="3">Fecha</th> <th>Fraccionamiento</th> <th>Cliente</th> <th>Actividad</th><th>Tel&eacute;fono</th> <th>Celular</th> <th>Fecha de Alta de Prospecto</th> <th>D&iacute;as en Seguimiento</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
	
		</tbody>
	</table>
</div>                     