<script type="text/javascript">
    jQuery(document).ready(
        function($){
        	
        	
        	
            //Jquery validations            
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
            
            $("#editar").click(
                function(e) {
                	
                	if(!$("#newUser").validationEngine('validate')){
		         		return false;
		          	}

                	e.preventDefault();
                	noty({
                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            
                       		$("#newUser").submit();                    
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false;
                          }
                        }
                      ]
                    });                                
             	}
            );
            
            $("#q2").change (function(){
            	
            	if( $(this).val() == 8 ) {
            		
            		$("#q2otro").show();
            	}
            	else {
            		$("#q2otro").val('');
            		$("#q2otro").hide();
            		$('#q2otro').validationEngine('hide');
            	}
            });
            
            $("#q1").change (function(){
            	
            	if( $(this).val() == 1 ) {
            		
            		$("#asesor").show();
            	}
            	else {
            		$("#asesor").hide();
            		$('#asesor').validationEngine('hide');
            	}
            });
            
        }
    );
</script>



<div id="contentUsers">
    
        
    <div id="newUserContainer">
        <form id="newUser" name="newUser" method="post" class="niceform" action="<?=base_url()?>admin/prospecto/editar_do">
            <?=form_fieldset('Editar Prospecto'); ?>
                <table>
                	<tr>
                		<td align="right" colspan="3">
                            <label for="idEstatusCliente">Clasificaci&oacute;n</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select name="idEstatusCliente" id="idEstatusCliente" class="validate[required]">                                
                                <option value="">- - - -</option>
                                <?php
                                if($clasificacion != null):
                                    foreach ($clasificacion as $key):
                                    ?>
                                        <option value="<?=$key->idEstatusCliente?>" <?php if($key->idEstatusCliente == $this_prospecto->idEstatusCliente) { echo 'selected="selected"'; } ?>> <?=$key->clasificacion ?> </option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>                                                            
                            </select>
                            </div>
                        </td>
                	</tr>
                	
                    <tr>
                        <td align="right">
                            <label for="idFraccionamiento">Desarrollo</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <input type="hidden" name="idHojaVida" value="<?=$this_prospecto->idHojaVida?>" />
                            <select name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">                                
                                <option value="">- - - -</option>
                                <?php
                                if($fraccionamientos != null):
                                    foreach ($fraccionamientos as $key):
                                    ?>
                                        <option value="<?=$key->idFraccionamiento?>" <?php if($key->idFraccionamiento == $this_prospecto->idFraccionamiento){ echo 'selected="selected"'; } ?>> <?=$key->nombreFrac?> </option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>                                                            
                            </select>
                            </div>
                        </td>
                        
                        <td align="right">
                            <label for="idUsuario">Asesor</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="idUsuario" id="idUsuario" class="validate[required]">
                                <option value="">- - - -</option>
                                <?php
                                if($asesores != null):
								?>
									<optgroup label="Asesores">
								<?php
                                    foreach ($asesores as $key):
                                    ?>
                                        <option value="<?=$key->idUsuario?>" <?php if($key->idUsuario == $this_prospecto->idUsuario){ echo 'selected="selected"'; } ?>> <?=$key->nombre?> </option>
                                    <?php
                                    endforeach;
									?>
									</optgroup>
								<?php
                                endif;
                                ?>                  
                            </select>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="tipo">Tipo</label></td>
                        <td> 
                        	<div class="divhojadd">
                            <select size="1" name="tipo" id="tipo" class="validate[required]">
                                <option value="">- - - -</option>
                                <option value="1" <?php if($this_prospecto->tipo == "1"){ echo 'selected="selected"'; } ?>>Visita</option>
                                <option value="2" <?php if($this_prospecto->tipo == "2"){ echo 'selected="selected"'; } ?>>LLamada</option>
                                <option value="3" <?php if($this_prospecto->tipo == "3"){ echo 'selected="selected"'; } ?>>Cambaceo</option>
                                <option value="4" <?php if($this_prospecto->tipo == "4"){ echo 'selected="selected"'; } ?>>Internet</option>
                                <option value="5" <?php if($this_prospecto->tipo == "5"){ echo 'selected="selected"'; } ?>>Regreso</option>
                                <option value="6" <?php if($this_prospecto->tipo == "6"){ echo 'selected="selected"'; } ?>>Referido</option>
                            </select>
                            </div>
                        </td>
                        
                        <td align="right"><label for="fecha">Fecha</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="fecha" id="fecha" value="<?=$this_prospecto->fecha?>" readonly="readonly" /> </div> </td>                
                    </tr>
                    
                    <!-- <tr>
                        <td align="right"><label for="hora">Hora</label></dt>
                        <td> <div class="divhojadd"> <input type="text" name="hora" id="hora" value="<?=$this_prospecto->hora?>" class="validate[required] text-input" /> </div> </td>
                    
                        <td align="right"><label for="numPase"> &nbsp; </label></td>
                        <td> &nbsp; </td>
                    </tr> -->
                    
                    <tr>
                    	<td align="right"><label for="apaterno">Apellido paterno</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="apaterno" id="apaterno" value="<?=$this_prospecto->apaterno?>" class="validate[required] text-input" /> </div></td>
                        <td align="right"><label for="amaterno">Apellido materno</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="amaterno" id="amaterno" value="<?=$this_prospecto->amaterno?>" class="validate[required] text-input" /> </div></td>
                    </tr>
                    
                    <tr>
                    	<td align="right"><label for="nombreCliente">Nombre</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nombreCliente" id="nombreCliente" value="<?=$this_prospecto->nombreCliente?>" class="validate[required] text-input" /> </div> </td>
                        
                        <td align="right"><label for="email">Email</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="email" id="email" value="<?=$this_prospecto->email?>" class="validate[custom[email]] text-input" /> </div></td>    
                    </tr>
                    
                    <tr>
                    	<td align="right"><label for="ladaFijo">Lada Tel Fijo</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="ladaFijo" id="ladaFijo" value="<?=$this_prospecto->ladaFijo?>" class="validate[custom[integer], minSize[2], maxSize[3], groupRequired[ladaFijo, ladaCelular]] text-input" /> </div> </td>
                            
                        <td align="right"><label for="ladaCelular">Lada Celular</label></td>
                        <td> 
                        	<div class="divhojadd"> 
                        		<select name="ladaCelular" class="validate[required, groupRequired[ladaFijo, ladaCelular]] ">
                        			<option value="">---</option>
    								<option value="044" <?php if($this_prospecto->ladaCelular == '044'){ echo 'selected="selected"'; }?>> 044 </option>
    								<option value="045" <?php if($this_prospecto->ladaCelular == '045'){ echo 'selected="selected"'; }?>> 045 </option>
                        		</select> 
                        	</div>
                        </td>
                    </tr>
                    
                    <tr>
                    	<td align="right"><label for="telFijo">Tel&eacute;fono Fijo</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="telFijo" id="telFijo" value="<?=$this_prospecto->telFijo?>" class="validate[custom[integer], minSize[7], maxSize[8], groupRequired[telFijo, celular]] text-input" /> </div> </td>
                        
                        <td align="right"><label for="celular">Celular</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="celular" id="celular" value="<?=$this_prospecto->telCelular?>" class="validate[custom[integer], minSize[10], maxSize[10], groupRequired[telFijo, celular]] text-input" /> </div></td>
                    </tr>
                    
                    <tr>                        
                        <td  align="right" colspan="3"><label>Nos hab&iacute;a visitado, llamado o conoce algunos de nuestros desarrollos ? </label></td>
                        <td class="hojadd" colspan="2">
                        	<div class="divhojadd">
                            <select size="1" name="q1" id="q1" class="validate[required]">
                                <option value="">----</option>
                                <option value="1" <?php if($this_prospecto->q1 == "1"){ echo 'selected="selected"'; } ?>>Si</option>
                                <option value="2" <?php if($this_prospecto->q1 == "2"){ echo 'selected="selected"'; } ?>>No</option>
                            </select>
                            
                            <input type="text" name="asesor" id="asesor" value="<?=$this_prospecto->asesor?>" class="validate[required] input-text" <?php if($this_prospecto->q1 != 1){ echo 'style="display: none;"'; }?> />
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label>C&oacute;mo se enter&oacute; de nosotros ? </label></td>
                        <td colspan="2"> 
                        	<div class="divhojadd">
                        		<select size="1" name="q2" id="q2" class="validate[required]">
	                                <option value="">----</option>
	                                <option value="1" <?php if($this_prospecto->q2 == 1) {echo 'selected="selected"'; }?> >De paso</option>
	                                <option value="2" <?php if($this_prospecto->q2 == 2) {echo 'selected="selected"'; }?> >Internet</option>
	                                <option value="3" <?php if($this_prospecto->q2 == 3) {echo 'selected="selected"'; }?> >Recomendaci&oacute;n</option>
	                                <option value="4" <?php if($this_prospecto->q2 == 4) {echo 'selected="selected"'; }?> >Volanteo</option>
	                                <option value="5" <?php if($this_prospecto->q2 == 5) {echo 'selected="selected"'; }?> >Promoci&oacute;n</option>
	                                <option value="6" <?php if($this_prospecto->q2 == 6) {echo 'selected="selected"'; }?> >Folleto/Revista</option>
	                                <option value="7" <?php if($this_prospecto->q2 == 7) {echo 'selected="selected"'; }?> >Radio/TV</option>
	                                <option value="8" <?php if($this_prospecto->q2 == 8) {echo 'selected="selected"'; }?> >Otro</option>
                            	</select>
                            	<input type="text" name="q2otro" id="q2otro" value="<?=$this_prospecto->q2otro?>" class="validate[required] input-text" <?php if($this_prospecto->q2 != 8){ echo 'style="display: none;"'; }?> />
                        	</div> 
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="nombreConyuge">Nombre de esposa(o) </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nombreConyuge" id="nombreConyuge" value="<?=$this_prospecto->nombreConyuge?>" /> </div> </td>
                    
                        <td align="right"><label for="q3">De d&oacute;nde nos visita ? </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q3" id="q3" value="<?=$this_prospecto->q3?>" /> </div> </td>
                    </tr>
                    <!--  FROM HOSTES-->                    
                    <tr>
                        <td align="right"><label for="q4">La busca para: </label></dt>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q4" id="q4" class="">
                                <option value="">----</option>
                                <option value="1" <?php if($this_prospecto->q4 == "1"){ echo 'selected="selected"'; } ?>>Vivir</option>
                                <option value="2" <?php if($this_prospecto->q4 == "2"){ echo 'selected="selected"'; } ?>>Invertir</option>
                                <option value="3" <?php if($this_prospecto->q4 == "3"){ echo 'selected="selected"'; } ?>>Fam o Amigo</option>
                            </select> 
                            </div>
                        </td>
                    
                        <td align="right"><label for="q5">Cu&aacute;l es su presupuesto ? </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q5" id="q5" value="<?=$this_prospecto->q5?>" class="" /> </div></td>
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="q6">Recamaras necesarias </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="q6" id="q6" value="<?=$this_prospecto->q6?>" /> </div> </td>
                        
                        <td><label for="q7">C&oacute;mo piensa adquirirla ? </label></td>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q7" id="q7" class="">
                                <option value="0"> ---- </option>
                                <option value="1" <?php if($this_prospecto->q7 == "1"){ echo 'selected="selected"'; } ?>> Contado </option>
                                <option value="2" <?php if($this_prospecto->q7 == "2"){ echo 'selected="selected"'; } ?>> Bancario </option>
                                <option value="3" <?php if($this_prospecto->q7 == "3"){ echo 'selected="selected"'; } ?>> Cofinavit </option>
                                <option value="4" <?php if($this_prospecto->q7 == "4"){ echo 'selected="selected"'; } ?>> Infonavit </option>
                                <option value="5" <?php if($this_prospecto->q7 == "5"){ echo 'selected="selected"'; } ?>> Aliados </option>
                                <option value="6" <?php if($this_prospecto->q7 == "6"){ echo 'selected="selected"'; } ?>> Fovissste </option>
                                <option value="7" <?php if($this_prospecto->q7 == "7"){ echo 'selected="selected"'; } ?>> CCadi </option>
                            </select>
                            </div>
                        </td>                        
                    </tr>                                        
                    
                    <tr>
                        <td align="right"><label for="nss">NSS </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="nss" id="nss" value="<?=$this_prospecto->nss?>" /> </div> </td>
                        
                        <td align="right"><label for="curp"> CURP </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="curp" id="curp" value="<?=$this_prospecto->curp?>" /> </div></td>                
                    </tr>
                    
                    <tr>
                        <td align="right"><label for="ingresoComprobable">Ingresos comprobables $</label></td>
                        <td> <div class="divhojadd"> <input type="text" name="ingresoComprobable" id="ingresoComprobable" value="<?=$this_prospecto->ingresoComprobable?>" class="validate[custom[integer]] text-input" /> </div></td>
                    
                        <td align="right"><label for="entregaRequerida">Entrega requerida </label></td>
                        <td> <div class="divhojadd"> <input type="text" name="entregaRequerida" id="entregaRequerida" value="<?=$this_prospecto->entregaRequerida?>" readonly="readonly" class="text-input datepicker" /> </div> </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label for="q8">Le gust&oacute; nuestra casa muestra ? </label></td>
                        <td>
                        	<div class="divhojadd">
                            <select size="1" name="q8" id="q8">
                                <option value="">----</option>
                                <option value="1" <?php if($this_prospecto->q8 == "1"){ echo 'selected="selected"'; } ?>>Si</option>
                                <option value="2" <?php if($this_prospecto->q8 == "2"){ echo 'selected="selected"'; } ?>>No</option>
                            </select>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right" colspan="3"><label for="q9">&iquest;Cu&aacute;l ubicaci&oacute;n le gust&oacute;? </label></td>
                        <td>
                        	<div class="divhojadd">
                        	<select size="1" name="q9" id="q9" class="">
	                            <option value="0">----</option>
	                            <option value="1" <?php if($this_prospecto->q9 == "1"){ echo 'selected="selected"'; } ?>>Norte</option>
	                            <option value="2" <?php if($this_prospecto->q9 == "2"){ echo 'selected="selected"'; } ?>>Sur</option>
	                            <option value="3" <?php if($this_prospecto->q9 == "3"){ echo 'selected="selected"'; } ?>>Oriente</option>
	                            <option value="4" <?php if($this_prospecto->q9 == "4"){ echo 'selected="selected"'; } ?>>Poniente</option>
	                            <option value="5" <?php if($this_prospecto->q9 == "5"){ echo 'selected="selected"'; } ?>>Primera Etapa</option>
	                            <option value="6" <?php if($this_prospecto->q9 == "6"){ echo 'selected="selected"'; } ?>>Segunda Etapa</option>
	                            <option value="7" <?php if($this_prospecto->q9 == "7"){ echo 'selected="selected"'; } ?>>Area Com&uacute;n</option>
	                        </select> 
	                        </div>
						</td>
                    </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q10">Es lo que est&aacute; buscando ? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q10" id="q10" class="">
                            <option value="">----</option>
                            <option value="1" <?php if($this_prospecto->q10 == "1"){ echo 'selected="selected"'; } ?>>Si</option>
                            <option value="2" <?php if($this_prospecto->q10 == "2"){ echo 'selected="selected"'; } ?>>No</option>
                        </select>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q10xque">&iquest;Por qu&eacute;? </label></td>
                    <td> <div class="divhojadd"> <input type="text" idaccept="q10xque" name="q10xque" value="<?=$this_prospecto->q10xque?>" /> </div> </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q11">Est&aacute; dentro de su plan de inversi&oacute;n ? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q11" id="q11">
                            <option value="0">----</option>
                            <option value="1" <?php if($this_prospecto->q11 == "1"){ echo 'selected="selected"'; } ?>>Si</option>
                            <option value="2" <?php if($this_prospecto->q11 == "2"){ echo 'selected="selected"'; } ?>>No</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q12">Cu&aacute;l es su inversi&oacute;n inicial ? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q12" id="q12" value="<?=$this_prospecto->q12?>" class="validate[custom[integer]] text-input" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q13">&iquest;Qu&eacute; le gust&oacute; de nuestro desarrollo? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q13" id="q13" value="<?=$this_prospecto->q13?>" class="" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q14">&iquest;Qu&eacute; no le gust&oacute; de nuestro desarrollo? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q14" id="q14" value="<?=$this_prospecto->q14?>" class="" /> </div> </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q15">De qu&eacute; depende o qu&eacute; necesita para decidir ? </label></td>
                    <td> <div class="divhojadd"> <input type="text" name="q15" id="q15" value="<?=$this_prospecto->q15?>" class="" /> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q16">Qu&eacute; le pareci&oacute; el precio ? </label></td>
                    <td>
                    	<div class="divhojadd">
                        <select size="1" name="q16" id="q16" class="">
                            <option value="0">----</option>
                            <option value="1" <?php if($this_prospecto->q16 == "1"){ echo 'selected="selected"'; } ?>>Alto</option>
                            <option value="2" <?php if($this_prospecto->q16 == "2"){ echo 'selected="selected"'; } ?>>Bien</option>
                            <option value="3" <?php if($this_prospecto->q16 == "3"){ echo 'selected="selected"'; } ?>>Bajo</option>
                        </select>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="q17">&iquest;En qu&eacute; qued&oacute; el cliente? </label></td>
                    <td> <div class="divhojadd"> <textarea name="q17" id="q17" class="validate[required] text-input"><?=$this_prospecto->q17?></textarea> </div></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label for="observaciones">Observaciones </label></td>
                    <td> <textarea name="observaciones" id="observaciones"><?=$this_prospecto->observaciones?></textarea></td>
                </tr>
                
                <tr>
                    <td align="right" colspan="3"><label>&nbsp;</label></td>
                    <td>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
                    </td>
                </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
    </div>
</div>