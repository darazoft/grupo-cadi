<script type="text/javascript">
    jQuery(document).ready(
        function($){
       
        }
    );
    	
</script>
<div id="contentUsers">
	
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Estatus</th> <th>Descripci&oacute;n</th> <th>Fecha de Registro</th> <th>Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($estatuscliente != null):
            $strong = true;
            $class = '';
            
        foreach ($estatuscliente as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idEstatusCliente?>" class="<?=$class?> optsPane">
				<td><?=$key->clasificacion?></td>
				<td><?=$key->descripcion?></td>
				<td><?=getFormatDate($key->fechaRegistro,true)?></td>
				
				<td>
				    <?php if($key->statusCliente == '1'): ?>
					    <span class="">
					        Activo
					    </span>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				</td>
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
	
</div>              

        