<script type="text/javascript">
    jQuery(document).ready(
        function($){


			//Jquery validations            
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
                                    
            $("#editar").click(
                function(e) {
                	
                	if(!$("#newUser").validationEngine('validate')){
		         		return false;
		          	}
		          	
                	return true;
                }
            );
        }
    );
</script>

<div id="contentUsers">
	
        <form id="newUser" name="newUser" method="post" class="niceform" action="<?=base_url()?>admin/prospecto/editar_estado_actividad_do/<?=$idHojaActividad?>/<?=$idHojaVida?>" enctype="multipart/form-data">
            <?=form_fieldset('Terminar Actividad'); ?>
                <table>
					
					<tr>
                		<td align="right">
                            <label for="">Prospecto:</label>
                        </td>
                        <td colspan="3"> 
                        	<label for=""><?=$prospecto->prospecto?> </label> 
                        </td>
                	</tr>
                	                	
                	<tr>
                		<td align="right" colspan="3">
                            <label for="nombreActividad">Motivo por el que cierra la actividad:</label>
                        </td>
                        
                		<td>
                        	<div class="divhojadd">
                            <input type="hidden" name="estatusCliente" value="<?=$estatusCliente?>" />
                            <textarea name="motivo" id="motivo" class="validate[required] text-input"></textarea>
                            </div>                            
                        </td>
                	</tr>
                	                    
                    <tr>
                        <td align="right" colspan="3"><label>&nbsp;</label></td>
                        <td>
                        	<input type="submit" value="Guardar" />
                        </td>
                    </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
</div>