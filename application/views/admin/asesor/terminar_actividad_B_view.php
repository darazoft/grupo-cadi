<script type="text/javascript">
    jQuery(document).ready(
        function($){


			//Jquery validations            
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
                                    
            $("#editar").click(
                function(e) {
                	
                	if(!$("#newUser").validationEngine('validate')){
		         		return false;
		          	}
		          	
                	return true;
                }
            );
        }
    );
</script>

<div id="contentUsers">
	
        <form id="newUser" name="newUser" method="post" class="niceform" action="<?=base_url()?>admin/prospecto/editar_estado_actividad_do/<?=$idHojaActividad?>/<?=$idHojaVida?>" enctype="multipart/form-data">
            <?=form_fieldset('Terminar Actividad'); ?>
                <table>
                	
                	<tr>
                		<td align="right">
                            <label for="">Prospecto:</label>
                        </td>
                        <td colspan="3"> 
                        	<label for=""><?=$prospecto->prospecto?> </label> 
                        </td>
                	</tr>
                	
                	<tr>
                		<td align="right" colspan="3">
                            <label for="nombreActividad">Motivo por el que cierra la actividad:</label>
                        </td>
                        
                		<td>
                        	<div class="divhojadd">
                            <input type="hidden" name="estatusCliente" value="<?=$estatusCliente?>" />
                            <textarea name="motivo" id="motivo" class="validate[required] text-input"></textarea>
                            </div>                            
                        </td>
                	</tr>
                	
                    <tr>
                    	<td align="right">
                            <label for="nombreActividad">Actividad:</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select name="nombreActividad" id="nombreActividad" class="validate[required]">
                                <option value="" > - - - - </option>
                                <option value="1">Llamada</option>
                                <option value="2">Cita</option>
                                <option value="3">Email</option>
                                <option value="4">Firma de contrato</option>
                                <option value="5">Pago de apartado</option>
                                <option value="6">Cancelaci&oacute;n</option>
                                <option value="7">Seguimiento post-venta</option>
                                <option value="8">Atenci&oacute;n a queja</option>
                                <option value="9">Seguimiento a cobranza</option>
                                <option value="10">Entrega de casa</option>
                                <option value="11">Firma de escritura</option>
                                <option value="12">Otra</option>
                            </select>
                           </div>
                        </td>
                        
                        <td align="right">
                            <label for="fechaInico">Inicio:</label>
                        </td>
                        
                        <td>
                            <input type="text" name="fechaInicio" id="fechaInicio" readonly="readonly" value="" class="validate[required] text-input datepicker" />                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="horaInicio">Hora inicio:</label>
                        </td>
                        
                        <td>
                            <select name="horaInicio" id="horaInicio">
                                <?php for($i = 0; $i<=24; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"> <?=$showHour?> </option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td>
                        
                       <!--  <td align="right">
                            <label for="minutoInicio">Minuto inicio:</label>
                        </td>
                        
                        <td>
                            <select name="minutoInicio" id="minutoInicio">
                                <?php for($i = 0; $i<=59; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>"><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td> -->
                    </tr>
                    
                    <tr>
                    	<td align="right">
                            <label for="archivo">Archivo:</label>
                        </td>
                        
                        <td>
                            <input type="file" name="archivo" id="archivo" />
                        </td>
                        
                        <td align="right">
                            <label for="descripcion">Descripcion:</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <textarea name="descripcion" id="descripcion" class="validate[required] text-input"></textarea>
                            </div>                            
                        </td>
                    </tr>
                                        
                    <tr>
                        <td align="right" colspan="3"><label>&nbsp;</label></td>
                        <td>
                        	<input type="submit" value="Guardar" />
                        </td>
                    </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
</div>