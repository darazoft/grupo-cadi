<script type="text/javascript">
    jQuery(document).ready(
        function($){


			//Jquery validations            
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( {
            	promptPosition : "bottomLeft", scroll: false,
            });
                                    
            $("#editar").click(
                function(e) {
                	
                	if(!$("#newUser").validationEngine('validate')){
		         		return false;
		          	}
		          	
                	return true;
                }
            );
        }
    );
</script>



<div id="contentUsers">
	
        <form id="newUser" name="newUser" method="post" class="niceform" action="<?=base_url()?>admin/prospecto/editar_actividad_do" enctype="multipart/form-data">
            <?=form_fieldset('Editar Actividad'); ?>
                <table>
                    <tr>
                    	<td align="right">
                            <label for="nombreActividad">Actividad:</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <select name="nombreActividad" id="nombreActividad" class="validate[required]">
                                <option value="" > - - - - </option>
                                <option value="1" <?php if($actividad->nombreActividad == 1){ echo 'selected="selected"';}?> >Llamada</option>
                                <option value="2" <?php if($actividad->nombreActividad == 2){ echo 'selected="selected"';}?> >Cita</option>
                                <option value="3" <?php if($actividad->nombreActividad == 3){ echo 'selected="selected"';}?> >Email</option>
                                <option value="4" <?php if($actividad->nombreActividad == 4){ echo 'selected="selected"';}?> >Firma de contrato</option>
                                <option value="5" <?php if($actividad->nombreActividad == 5){ echo 'selected="selected"';}?> >Pago de apartado</option>
                                <option value="6" <?php if($actividad->nombreActividad == 6){ echo 'selected="selected"';}?> >Cancelaci&oacute;n</option>
                                <option value="7" <?php if($actividad->nombreActividad == 7){ echo 'selected="selected"';}?> >Seguimiento post-venta</option>
                                <option value="8" <?php if($actividad->nombreActividad == 8){ echo 'selected="selected"';}?> >Atenci&oacute;n a queja</option>
                                <option value="9" <?php if($actividad->nombreActividad == 9){ echo 'selected="selected"';}?> >Seguimiento a cobranza</option>
                                <option value="10" <?php if($actividad->nombreActividad == 10){ echo 'selected="selected"';}?> >Entrega de casa</option>
                                <option value="11" <?php if($actividad->nombreActividad == 11){ echo 'selected="selected"';}?> >Firma de escritura</option>
                                <option value="12" <?php if($actividad->nombreActividad == 12){ echo 'selected="selected"';}?> >Otra</option>
                            </select>
                           </div>
                        </td>
                        
                        <td align="right">
                            <label for="fechaInico">Inicio:</label>
                        </td>
                        
                        <td>
                            <input type="hidden" name="idHojaVida" value="<?=$actividad->idHojaVida?>" />
                            <input type="hidden" name="idHojaActividad" value="<?=$actividad->idHojaActividad?>" />
                            <input type="hidden" name="filename" value="<?=$actividad->adjunto?>" />
                            <input type="text" name="fechaInicio" id="fechaInicio" readonly="readonly" value="<?=substr($actividad->fechaInicio, 0,10)?>" class="validate[required] text-input datepicker" />                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="right">
                            <label for="horaInicio">Hora inicio:</label>
                        </td>
                        
                        <td>
                            <select name="horaInicio" id="horaInicio">
                                <?php for($i = 0; $i<=24; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>" <?php if(substr($actividad->fechaInicio,11,2) == $showHour){ echo 'selected="selected"';}?>> <?=$showHour?> </option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td>
                        
                        <!-- <td align="right">
                            <label for="minutoInicio">Minuto inicio:</label>
                        </td>
                        
                        <td>
                            <select name="minutoInicio" id="minutoInicio">
                                <?php for($i = 0; $i<=59; $i++):
                                          $showHour = $i;
                                          if($i < 10):
                                              $showHour = '0'.$i;
                                          endif; ?>
                                          <option value="<?=$showHour?>" <?php if(substr($actividad->fechaInicio,14,2) == $showHour){ echo 'selected="selected"';}?>><?=$showHour?></option>                                                                 
                                <?php endfor; ?>                                
                            </select>                                                        
                        </td> -->
                    </tr>
                    
                    <tr>
                    	<td align="right">
                            <label for="archivo">Archivo:</label>
                        </td>
                        
                        <td>
                            <input type="file" name="archivo" id="archivo" />
                        </td>
                        
                        <td align="right">
                            <label for="descripcion">Descripcion:</label>
                        </td>
                        
                        <td>
                        	<div class="divhojadd">
                            <textarea name="descripcion" id="descripcion" class="validate[required] text-input"><?=$actividad->descripcion?></textarea>
                            </div>                            
                        </td>
                    </tr>
                                        
                    <tr>
                        <td align="right" colspan="3"><label>&nbsp;</label></td>
                        <td>
                        	<input type="submit" value="Guardar" id="editar" />
                        	<input type="reset" value="Restablecer" />
                        </td>
                    </tr>                    
                </table>            
            <?=form_fieldset_close(); ?>            
        </form>                
</div>