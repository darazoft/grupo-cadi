<script type="text/javascript">
    jQuery(document).ready(
        function($){
                        
            $("#editar").click(
                function(e){
                    e.preventDefault();
                    if($("#idFraccionamiento").val() == 0 || $("#num_casas").val() == '' || $("#comision").val() == ''){
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DE LA COMISION',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                    	
                    	noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                        
                    }
                }
            );
            
        }
    );
</script>
<div id="contentUsers">
    
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/comisionesfrac/editar_do">
        	<input type="hidden" name="idFraccionamientoComision" id="idFraccionamientoComision" value="<?=$info->idFraccionamientoComision?>">
            <?=form_fieldset('Editar comision'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento">
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
									if($info->idFraccionamiento == $key->idFraccionamiento):
									?>
											<option value="<?=$key->idFraccionamiento?>" selecte="selected"> <?=$key->nombreFrac?> </option>
									<?php
										else:
									?>
										<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
									<?php
									endif;
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="numcasas">N&uacute;mero de casas</label></dt>
                    <dd><input type="text" name="numcasas" id="numcasas" value="<?=$info->num_casas?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="comision">Comisi&oacute;n</label></dt>
                    <dd><input type="text" name="comision" id="comision" value="<?=$info->comision?>"> %</dd>
                </dl>                
                                               
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
					</dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
        
</div>              
       