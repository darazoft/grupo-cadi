<script type="text/javascript">
    jQuery(document).ready(
        function($){
            
            $("#editar").click(
                function(e){

                    e.preventDefault();
                    if($("#nombre").val() == '' || $("#email").val() == '' || $("#puesto").val() == '' || $("#fechaIngreso").val() == '' || $("#rol").val() == 0) {
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DEL USUARIO',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                        
                        return false;           
                    } 
                    else{
                    	
	                    noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                    }
                }
            );
                                  
        }   
    );
</script>
<div id="contentUsers">
       
    
    <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/usuarios/editar_do">
        <?=form_fieldset('Editar Usuario'); ?>
            <dl>
                <dt><label for="nombre">Nombre</label></dt>
                <dd>
                    <input type="text" name="nombre" id="nombre" value="<?=$user_info->nombre?>">
                    <input type="hidden" name="idUsuario" id="idUsuario" value="<?=$user_info->idUsuario?>" />
                </dd>
            </dl>
            
            <dl>
                <dt><label for="email">Email</label></dt>
                <dd><input type="text" name="email" id="email" value="<?=$user_info->emailUsuario?>"></dd>
            </dl>
            
            <dl>
                <dt><label for="puesto">Puesto</label></dt>
                <dd><input type="text" name="puesto" id="puesto" value="<?=$user_info->puesto?>"></dd>
            </dl>                
            
            <dl>
                <dt><label for="fechaIngreso">Fecha de Ingreso</label></dt>
                <dd>
                    <input type="text" name="fechaIngreso" id="fechaIngreso" value="<?=substr($user_info->fechaRegistro, 0,10)?>" onfocus="displayCalendar(document.forms[0].fechaIngreso,'yyyy-mm-dd',this)" readonly="readonly" />
                </dd>
            </dl>
            
            <dl>
                <dt><label for="rol">Tipo</label></dt>
                <dd>
                    <select size="1" name="rol" id="rol">
                        <option value="0">- - - -</option>
                        <option value="1" <? if($user_info->idRol == '1'){echo'selected="selected"';} ?>>Administrador</option>
                        <option value="2" <? if($user_info->idRol == '2'){echo'selected="selected"';} ?>>Coordinador</option>
                        <option value="3" <? if($user_info->idRol == '3'){echo'selected="selected"';} ?>>Asesor</option>
                        <option value="4" <? if($user_info->idRol == '4'){echo'selected="selected"';} ?>>Hostess</option>
                        <option value="5" <? if($user_info->idRol == '5'){echo'selected="selected"';} ?>>Titulacion</option>
                        <option value="6" <? if($user_info->idRol == '6'){echo'selected="selected"';} ?>>Cordinador de obra</option>                            
                    </select>                    
                </dd>
            </dl>
            
            <dl>
                <dt><label>&nbsp;</label></dt>
                <dd>
                    <input type="submit" value="Guardar" id="editar">
                    <input type="reset" value="Restablecer" id="restablecer">
                </dd>
            </dl>
        <?=form_fieldset_close(); ?>
    </form>                
</div>              

        