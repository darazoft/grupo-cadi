<script type="text/javascript">
    jQuery(document).ready(
        function($){
                       
           	$("#editar").click(
                function(e){
                    e.preventDefault();
                    //if($("#idFraccionamiento").val() == 0 || $("#idPrototipo").val() == 0 || $("#metros").val() == '' || $("#metrosConstruccion").val() == '' || $("#numCasa").val() == '' || $("#acabados").val() == '' || $("#orientacion").val() == 0 || $("#precioLista").val() == '' || $("#statusVenta").val() == 0 ){
                    if($("#idFraccionamiento").val() == 0 || $("#idPrototipo").val() == 0 || $("#metros").val() == '' || $("#metrosConstruccion").val() == '' || $("#numCasa").val() == '' ) {
                    	
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DE LA CASA',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
                        noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });
                    }
                }
            );
            
        }
    );
</script>

<div id="contentUsers">        
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/casas/editar_do">
        	<input type="hidden" name="idCasa" id="idCasa" value="<?=$info->idCasa?>" />
            <?=form_fieldset('Editar casa'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento">
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
									
									if($info->idFraccionamiento == $key->idFraccionamiento):
								?>
										<option value="<?=$key->idFraccionamiento?>" selected="selected"> <?=$key->nombreFrac?> </option>
								<?php
									else:
								?>
									<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
								<?php
									endif;
								endforeach;
							endif;
                            ?>                          
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="idPrototipo">Prototipo</label></dt>
                    <dd>
                    	<select size="1" name="idPrototipo" id="idPrototipo">
                    		<option value="0"> ------- </option>
                           <?php
                            if($prototipos != null):
								foreach ($prototipos as $key):
									
									if($info->idPrototipo == $key->idPrototipo):
								?>
										<option value="<?=$key->idPrototipo?>" selected="selected"> <?=$key->nombrePrototipo?> </option>
								<?php
									else:
								?>
										<option value="<?=$key->idPrototipo?>"> <?=$key->nombrePrototipo?> </option>
								<?php
									endif;
								endforeach;
							endif;
                            ?>                          
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="numCasa">N. Casa</label></dt>
                    <dd><input type="text" name="numCasa" id="numCasa" value="<?=$info->numCasa?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="interior">Int. Ejem.(B)</label></dt>
                    <dd><input type="text" name="interior" id="interior" value="<?=$info->interior?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="metros">Metros <sup>2</sup> Terreno</label></dt>
                    <dd><input type="text" name="metros" id="metros" value="<?=$info->metros?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="metrosConstruccion">Metros <sup>2</sup> Construcci&oacute;n</label></dt>
                    <dd><input type="text" name="metrosConstruccion" id="metrosConstruccion" value="<?=$info->metrosConstruccion?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="acabados">Acabados</label></dt>
                    <dd><textarea name="acabados" id="acabados"><?=$info->acabados?></textarea></dd>
                </dl>
                
                <dl>
                    <dt><label for="orientacion">Orientaci&oacute;n</label></dt>
                    <dd>
                    	<select size="1" name="orientacion" id="orientacion">
                            <option value="0">----</option>
                            <option value="1" <? if($info->orientacion == '1'){echo'selected="selected"';} ?>>Norte</option>
                            <option value="2" <? if($info->orientacion == '2'){echo'selected="selected"';} ?>>Sur</option>
                            <option value="3" <? if($info->orientacion == '3'){echo'selected="selected"';} ?>>Oriente</option>
                            <option value="4" <? if($info->orientacion == '4'){echo'selected="selected"';} ?>>Poniente</option>
                        </select>
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="precioLista">Precio de Lista $</label></dt>
                    <dd><input type="text" name="precioLista" id="precioLista" value="<?=$info->precioLista?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="statusVenta">Estatus</label></dt>
                    <dd>
                    	<select size="1" name="statusVenta" id="statusVenta">
                            <option value="0">----</option>
                            <option value="1" <? if($info->statusVenta == '1'){echo'selected="selected"';} ?>> Disponible </option>
                            <option value="2" <? if($info->statusVenta == '2'){echo'selected="selected"';} ?>> Apartada </option>
                            <option value="3" <? if($info->statusVenta == '3'){echo'selected="selected"';} ?>> Con contrato </option>
                            <option value="4" <? if($info->statusVenta == '4'){echo'selected="selected"';} ?>> Escriturada </option>
                            <option value="5" <? if($info->statusVenta == '5'){echo'selected="selected"';} ?>> Entregada </option>
                        </select>
                    </dd>
                </dl>
                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
                    </dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>
</div>                     