<script type="text/javascript">
    jQuery(document).ready(
        function($){
        	var open = false;
            speed = 500;
            $(".hideForm").stop().hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").stop().show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").stop().hide(speed);
                        open = false;                        
                    }
                }
            );
			
			$( "#accordion" ).accordion({
				collapsible: true,
				autoHeight: false
			});
			
			
			var selected = false;
			//Selecciona todos los checks
			$("#allSelects").live(
				'click',
				function() {
				
				try {
					
					var aviso = '';
					if( $("input:checked[name^='chk']").length == 0 ) { //Seleccionalos
						
	               		$("input:checkbox[name^='chk']").attr("checked", true);
	               		$(".label_check").addClass("c_on");  //Agregamos la clase
	               		aviso = 'TODAS LAS CASAS DE CADA FRACCIONAMIENTO FUERON SELECCIONADAS';
	               	}
	               	else if( $("input:checked[name^='chk']").length > 0 ) { // Deseleccionalos
	               		
	               		$("input:checkbox[name^='chk']").attr("checked", false);
	               		$(".label_check").removeClass("c_on");
	               		aviso = 'TODAS LAS CASAS FUERON DESELECCIONADAS';
	               	}
	               	
	               	
	               	noty({
	                    text : aviso,
	                    type : 'warning',
	                    dismissQueue: true,
	                    layout: 'top',
	                    theme: 'default',
	                    timeout: 2000
	                });
	                
				}
				catch(e){ alert(e); }
					
			});
			
			//Selecciona todos las casas de un fraccionamiento
			$(".fracSelects").live(
                'click',
                function(e){
                e.preventDefault();
                
	                try {
	                	
	                    var thisID = $(this).attr('id');
	                    var thisHref = $(this).attr('href');
	                    
	                    if( $("input:checked[id^='chk"+thisHref+"']").length == 0 ) {
	                    	
		                    $("input:checkbox[id^='"+thisID+"']").attr("checked", true);
		                    $("label[for^='"+thisID+"']").addClass("c_on");  //Agregamos la clase
	                    }
	                    else if( $("input:checked[id^='chk"+thisHref+"']").length > 0) {
	                    	
	                    	$("input:checkbox[id^='"+thisID+"']").attr("checked", false);
		                    $("label[for^='"+thisID+"']").removeClass("c_on");  //Agregamos la clase	
	                    }
	                    
	                }catch(e){ alert(e); }     
                }
            );
            
            $("input:checkbox[name^='chk']").live(
            	'click',
            	function(){
            		
            		try {
            			
	            		var idFinal = '';
	            		var id = $(this).attr('id');
	            		idFinal = id.split('chk');
	            		idFinal = idFinal[1];
	            		idFinal = idFinal.split('-');
	            		idFinal = idFinal[0];
	            			            		
	            		if($('#'+id).is(':checked') == true) {
	            			
	            			 $('#'+id).attr("checked", true);
	            		}
	            		else if($('#'+id).is(':checked') == false) {
	            			
	            			 $('#'+id).attr("checked", false);
	            		}
   						
	            	}
	            	catch(e){ alert(e); }
            	}
            );
            
            
            /**FUNCIONES DE INSERCIÓN **/
           //Jquery validations
            $( ".datepicker" ).datepicker();
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true && $("input:checked[name^='chk']").length > 0) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/promociones/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'PROMOCIÓN AGREGADA SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
	                             
                              			noty({
				                            text : 'EXISTEN UNO O VARIOS REGISTROS SIMILARES EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        });      
                                }                             
                            }                            
                        })
                       } // cierra if status true
                       else {
	                       	noty({
	                            text : 'SELECCIONE AL MENOS UNA CASA A LA QUE APLICARA LA PROMOCION',
	                            type : 'warning',
	                            dismissQueue: true,
	                            layout: 'top',
	                            theme: 'default',
	                            timeout: 2000
	                        });   
                       } 
                    }
                }
            );
            
            
            $(".optsPane").live(
                'mouseover',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().show();
                }
            );
            
            
            
            $(".optsPane").live(
                'mouseleave',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().hide();
                }
            );
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea elminar a este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/promociones/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idFraccionamientoPromocion=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
            
            
        }
    );
    
    function touchStart(event,id) {
	  // Insert your code here
	  try{
	  	document.getElementById("optsPane"+id).style.display = 'block';
	  }
	  catch(e){ alert(e); }
	  
	}
</script>
<div id="contentUsers">
    <form class="niceform">
		<input type="button" value="Nueva Promoci&oacute;n" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nueva promoci&oacute;n'); ?>
            
            <dl>
	            <dt><label>&nbsp;</label></dt>
	            <dd><input type="button" id="allSelects" name="allSelects" value="Seleccionar Todo" /></dd>
	        </dl>
            <br /><br />
            <div id="accordion" style="border: solid 0px #000; position: relative; left: 50px;">
            	<?php
            	$titulo = '';
				$id = '';
                if($fraccionamientos != null):
					
					foreach($fraccionamientos as $fath):
						
						foreach($fath as $title):
							$id = $title->idFraccionamiento;
							$titulo = $title->nombreFrac;							
						endforeach;
					?>                		
	                	<h3> 
	                		<a href="#"> Fracc. <?=$titulo?> </a>	
	                	</h3>
	                	
                		<div>
                			<a href="<?=$id?>" id="chk<?=$id?>" class="fracSelects"> Todas las casas <input type="hidden" id="fflag<?=$id?>" name="fraccionamientos[]" value="<?=$id?>" /> </a>
                			<?php
                			  foreach($fath as $key):
								  
								$orientacion ='';
								switch($key->orientacion):
									case 1:
										$orientacion = "Norte";		
									break;
									case 2:
										$orientacion = "Sur";		
									break;
									case 3:
										$orientacion = "Oriente";		
									break;
									case 4:
										$orientacion = "Poniente";		
									break;
								endswitch;
							?>
		                        <label class="label_check" for="chk<?=$key->idFraccionamiento?>-<?=$key->idCasa?>">
		                            <input type="checkbox" class="all" name="chk[]" id="chk<?=$key->idFraccionamiento?>-<?=$key->idCasa?>" value="<?=$key->idFraccionamiento?>-<?=$key->idCasa?>" />
		                           	<span style="margin-left: 30px; border: solid 0px #000; width: 100px;">
		                			<?php
										echo "N. ".$key -> numCasa." ".$key->interior."&nbsp;&nbsp;";
										echo " ".$key->nombrePrototipo."&nbsp;&nbsp;";
										echo "Orientaci&oacute;n: ".$orientacion."&nbsp;&nbsp;";
									?>
		                        	</span>                                             
		                        </label>
		                        <br />
	                       <?php
	                         endforeach;
		                   ?>                        	                        
	                    </div>
                <?php
					endforeach;
				endif;
                ?>    
             </div>
             <br />
                               
                <dl>
                    <dt><label for="titulo">Titulo</label></dt>
                    <dd><input type="text" name="titulo" id="titulo" value="" class="validate[required] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="promocion">Promoci&oacute;n</label></dt>
                    <dd><textarea name="promocion" id="promocion" class="validate[required] text-input"></textarea></dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaInicio">Fecha Inicio</label></dt>
                    <dd>
                    	<input type="text" name="fechaInicio" id="fechaInicio" value="" readonly="readonly" class="validate[required] text-input datepicker" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaFin">Fecha Fin</label></dt>
                    <dd>
                    	<input type="text" name="fechaFin" id="fechaFin" value="" readonly="readonly" class="validate[required, future[fechaInicio]] text-input datepicker" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Agregar Promoci&oacute;n"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Fraccionamiento</th> <th>N. Casa</th> <th>T&iacute;tulo</th> <th>Promoci&oacute;n</th> <th>Fecha Inicio</th> <th>Fecha Fin</th> <th class="optionsPane">Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($promociones != null):
            $strong = true;
            $class = '';
            
        foreach ($promociones as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>
			<tr id="<?=$key->idFraccionamientoPromocion?>" class="<?=$class?> optsPane">
				<td><?=$key->nombreFrac?></td>
				<td><?=$key->numCasa?> <?=$key->interior?></td>
				<td><?=$key->titulo?></td>
				<td><?=$key->promocion?></td>
				<td><?=getFormatDate($key->fechaInicio,false)?></td>
				<td><?=getFormatDate($key->fechaFin,false)?></td>
								
				<td class="optionsPane" ontouchstart="touchStart(event,<?=$key->idFraccionamientoPromocion?>);">
				    
				    <?php if($key->statusPromo == '1'): ?>
					    <span class="">
					        Activo
					    </span>
				    
					    <div id="optsPane<?=$key->idFraccionamientoPromocion?>">
					        <a id="editRow<?=$key->idFraccionamientoPromocion?>" href="<?=base_url()?>admin/promociones/editar/<?=$key->idFraccionamientoPromocion?>">
					            <img src="<?=base_url()?>img/edit_row.png" />
					        </a>
					        
					        <a id="deleteRow<?=$key->idFraccionamientoPromocion?>" href="<?=$key->idFraccionamientoPromocion?>" class="deleteRow">				            
			                    <img src="<?=base_url()?>img/delete_row.png" />
			                </a>				       
				    	</div>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				</td>
				
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>                     