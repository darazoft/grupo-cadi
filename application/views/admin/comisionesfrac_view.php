<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }
            );
            
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status) {
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/comisionesfrac/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'COMISION AGREGADA SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                   
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                } 
                                else if(data.response=='error_val') {
                                
	                            		noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
                            		noty({
			                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
			                            type : 'error',
			                            dismissQueue: true,
			                            layout: 'top',
			                            theme: 'default',
			                            timeout: 4000
			                        });
                                }                             
                            }                            
                        })
                       } // cierra if status true
                    }
                }
            );
            
            $(".optsPane").live(
                'mouseover',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().show();
                }
            );
            
            $(".optsPane").live(
                'mouseleave',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().hide();
                }
            );
            
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea elminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/comisionesfrac/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idFraccionamientoComision=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
        }
    );
    
    function touchStart(event,id) {
	  // Insert your code here
	  try{
	  	document.getElementById("optsPane"+id).style.display = 'block';
	  }
	  catch(e){ alert(e); }
	  
	}
</script>
<div id="contentUsers">
    <form class="niceform">
		<input type="button" value="Nueva comision" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nueva comision'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">
                            <option value="">- - - -</option>
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
								?>
									<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
								<?php
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="numcasas">N&uacute;mero de casas</label></dt>
                    <dd><input type="text" name="numcasas" id="numcasas" value="" class="validate[required, custom[integer], minSize[1], maxSize[2], min[1]] text-input" ></dd>
                </dl>
                
                <dl>
                    <dt><label for="comision">Comisi&oacute;n</label></dt>
                    <dd><input type="text" name="comision" id="comision" value="" class="validate[required, custom[number], minSize[1], maxSize[4], min[1.0]] text-input"> %</dd>
                </dl>                
                                               
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Agregar comision"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Fraccionamiento</th> <th>Num de casas</th> <th>Comisi&oacute;n</th> <th>Fecha de Registro</th> <th class="optionsPane">Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($comisiones != null):
            $strong = true;
            $class = '';
            
        foreach ($comisiones as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>		    		    
			<tr id="<?=$key->idFraccionamientoComision?>" class="<?=$class?> optsPane">
				<td><?=$key->nombreFrac?></td>
				<td><?=$key->num_casas?></td>
				<td><?=$key->comision?>%</td>
				<td><?=getFormatDate($key->fechaRegistro,true)?></td>
				
				<td class="optionsPane" ontouchstart="touchStart(event,<?=$key->idFraccionamientoComision?>);">
				    <?php if($key->statusComision == '1'): ?>
					    <span class="">
					        Activo
					    </span>
				    
					    <div id="optsPane<?=$key->idFraccionamientoComision?>">
					        <a id="editRow<?=$key->idFraccionamientoComision?>" href="<?=base_url()?>admin/comisionesfrac/editar/<?=$key->idFraccionamientoComision?>">
					            <img src="<?=base_url()?>img/edit_row.png" />
					        </a>
					        
					        <a id="deleteRow<?=$key->idFraccionamientoComision?>" href="<?=$key->idFraccionamientoComision?>" class="deleteRow">				            
			                    <img src="<?=base_url()?>img/delete_row.png" />
			                </a>				       
				    	</div>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				</td>
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
	
</div>              
       