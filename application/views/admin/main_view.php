﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title><?= $SYS_metaTitle; ?></title>

    <meta content="text/html; charset=UTF-8" /> 
    <link rel="shortcut icon" href="<?php echo base_url(); ?>imgs/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/adminLayout.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jqueryUi.css" type="text/css">        
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/niceforms-default.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/noty/buttons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fancybox-1.3.4.css" type="text/css">
    
    <!-- JQUERY VALIDATIONS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css" type="text/css"/>
    <!-- <link rel="stylesheet" href="http://jqueryui.com/themes/base/jquery.ui.all.css" type="text/css" /> -->
    <!-- JQUERY VALIDATIONS-->        
    
    <!-- Versión para imprimir -->
	<style type="text/css" media="print">
		#sidebar {
			display:none;
	    }
	</style>
	<!-- Versión para imprimir -->
    
    <?php
        //css dinámicos
        if(isset($css)){
            for($i=0;$i<count($css);$i++){
                echo "<link rel=\"stylesheet\" href=\"".base_url()."css/".$css[$i].".css\" type=\"text/css\">\n";
            }
        }
    ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jqueryUi.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/facebox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/library.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/setuplabels.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>js/fancyselect.js"></script> -->
    
    <!-- fancybox -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox/jquery.easing-1.3.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox/jquery.fancybox-1.3.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>    
    
    <!-- noty -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/jquery.noty.js"></script>
  
    <!-- layouts -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottom.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomCenter.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/bottomRight.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/center.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/centerLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/centerRight.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/inline.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/top.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topCenter.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topLeft.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/noty/layouts/topRight.js"></script>

    <!-- themes -->
    <script type="text/javascript" src="<?=base_url()?>js/noty/themes/default.js"></script>
    
    <!-- JQUERY VALIDATIONS -->
    <script src="<?=base_url()?>js/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"> </script>
    <script src="<?=base_url()?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"> </script>
    <!-- JQUERY VALIDATIONS -->
    
    <!-- CLOCK24 -->
    <script src="<?=base_url()?>js/clock24.js" type="text/javascript" charset="utf-8"> </script>
    <!-- CLOCK24 -->
    
   <?php
        //js dinámicos
        if(isset($js)){
            for($i=0;$i<count($js);$i++){
                echo "<script type=\"text/javascript\" src=\"" . base_url() . "js/" . $js[$i] . ".js\"></script>\n";                
            }
        }
    ?>
    
    <script type="text/javascript">
    jQuery(document).ready(function($) {
    <?php if(isset($error) || $this->session->flashdata('error')): ?>
        noty({
                text : '<?=$this->lang->line(((isset($error))?($error):($this->session->flashdata('error'))))?>',
                type : '<?=$this -> session -> flashdata('notyType')?>',
                dismissQueue: true,
                layout: 'top',
                theme: 'default',
                timeout: 2000                
            });
    <?php endif ?>            
        //MY MENU
        //Ocultamos los submenus
        $('ul.mainMenu li ul').hide();
        
        $('ul.mainMenu li').hover(
            //Funcion Hover
            function(){
                //Escondemos otrosmenus
                $('ul.mainMenu li').not($("ul", this)).stop();
                
                
                //Mostramos el menu correspondiente                 
                $('ul', this).slideDown('slow');
            },
            //OnOut
            function(){
                //Escondemos todos los menus                    
                $('ul', this).slideUp("slow");                  
            }
        );
        /*FANCY SELECTS*/
       // do_fancy_select();
       
       
       /*Cargar fancybox*/
	    $("#paso").fancybox({					
			'titlePosition'	    : 'inside',
			'width'				: '35%',
			'height'			: '35%',
			'autoScale'			: false,
			'transitionIn'		: 'elastic',
			'transitionOut'		: 'elastic',
			//'type'				: 'iframe',
			 'modal'            : true,
	        'hideOnOverlayClick': true,
			'enableEscapeButton': true,
			'showCloseButton'   : true
	    });
	    
	     $("#paso").trigger('click'); 
	  
    });    
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="header">
            <div class="logo">  </div>    
        </div>
        <div id="leContent">
            <div class="userInfo">
                Hola! <?=$this->session->userdata('nombreUsuario')?>
                &nbsp;&nbsp;&nbsp;
                <a href="<?=base_url()?>sesion/logout/admin">Salir</a>
                &nbsp;&nbsp;&nbsp;
                <a href="<?=base_url()?>admin/dashboard/cuenta">Mi Cuenta</a>
                
                <?php
                if( $this->session->userdata('idRol') == 3 ) {
                ?>
                	<iframe src="<?=base_url()?>admin/dashboard/getActividadesPendientes" frameborder="0" allowtransparency="allowtransparency" scrolling="no" 
                	style="position:relative; z-index:1001; width: 32px; height: 32px; background-color:transparent;"> </iframe>
                <?php
                }
                ?>
                
                <span class="clock24s now" id="clock24_index115" style="color:#6393C3;"> &nbsp;</span>
                <span class="now"> 
                	<a href='javascript:void(0)' onclick='javascript:{window.print();}' title='Imprimir'> <img src='<?=base_url()?>img/print.png' border='0' /> </a> 
                </span>
                
            	<script type="text/javascript">
					var clock24_index115 = new clock24('index115',999,'%dd / %M / %yyyy %W %hh:%nn:%ss','es');
					clock24_index115.daylight(); clock24_index115.refresh();
				</script>
            </div>
            
            <div id="sidebar" class="floatLeft">
                <?=$secciones?>                                
            </div>
            
            <div id="appContainer" class="floatLeft">                
                <?=$this->load->view($module)?>                                                
            </div>
            
            <?php
            if(isset($fancy)):
            	if($fancy != null ):
            ?>
            	<a id="paso" href="#fancy" title=""> </a> 
	            <div id="fancy">
	            	<?=$this->load->view($fancy)?>
	            </div> 
            <?php
				endif;
			endif;
            ?>
        </div>  
    </div>
    
    <div id="footer">
        <span>
            Grupo CADI &copy; - Todos Los Derechos Reservados                
        </span>
    </div>
    
    <script type='text/javascript' language='javascript'>
		r=escape(document.referrer); u=escape(document.URL); 
		s=(typeof(screen)=='undefined')?'':screen.width+'x'+screen.height+'x'+(screen.colorDepth?screen.colorDepth:screen.pixelDepth);
		c='http://counter.2'+'4log.ru/counter?id=2&t=91&st=1&r='+r+'&s='+s+'&u='+u+'&rnd='+Math.random();
		document.write('<img border="0" width="1" height="1"'+' src="'+c+'" />');
	</script>

</body>
</html>