<script type="text/javascript">
    jQuery(document).ready(
        function($){
        	
            /**FUNCIONES DE INSERCIÓN **/
            $("#editar").click(
                function(e){
                    e.preventDefault();
                                        
                    if( $("#titulo").val() == '' || $("#promocion").val() == '' || $("#fechaInicio").val() == '' || $("#fechaFin").val() == ''){
                        noty({
                            text : 'POR FAVOR INGRESE TODOS LOS DATOS DE LA PROMOCIÓN',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } 
                    else{
                    	
                    	noty({
		                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
		                      type: 'alert',
		                      dismissQueue: true,
		                      layout: 'center',
		                      theme: 'default',
		                      buttons: [
		                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
		                            $noty.close();
		                       		
									$("#editUser").submit();                                 
		                          }
		                        },
		                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
		                            $noty.close();
		                            return false;
		                          }
		                        }
		                      ]
		                    });    
                    }
                }
            );
            
            
        }
    );
</script>
<div id="contentUsers">
    
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/promociones/editar_do">
        	<input type="hidden" name="idFraccionamientoPromocion" id="idFraccionamientoPromocion" value="<?=$info->idFraccionamientoPromocion?>">
        	<input type="hidden" name="idFraccionamiento" id="idFraccionamiento" value="<?=$info->idFraccionamiento?>">
        	<input type="hidden" name="idCasa" id="idCasa" value="<?=$info->idCasa?>">
            <?=form_fieldset('Nueva promoci&oacute;n'); ?>
                
                <dl>
                    <dt><label for="fraccionamiento">Fraccionamiento</label></dt>
                    <dd> <span> <?=$info->nombreFrac?> </span> </dd>
                </dl>
                
                <dl>
                    <dt><label for="numCasa">N&uacute;mero de Casa</label></dt>
                    <dd> <span> <?=$info->numCasa?> <?=$info->interior?> </span> </dd>
                </dl>
                      
                <dl>
                    <dt><label for="titulo">Titulo</label></dt>
                    <dd><input type="text" name="titulo" id="titulo" value="<?=$info->titulo?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="promocion">Promoci&oacute;n</label></dt>
                    <dd><textarea name="promocion" id="promocion"><?=$info->promocion?></textarea></dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaInicio">Fecha Inicio</label></dt>
                    <dd>
                    	<input type="text" name="fechaInicio" id="fechaInicio" value="<?=$info->fechaInicio?>" onfocus="displayCalendar(document.forms[0].fechaInicio,'yyyy-mm-dd',this)" readonly="readonly" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="fechaFin">Fecha Fin</label></dt>
                    <dd>
                    	<input type="text" name="fechaFin" id="fechaFin" value="<?=$info->fechaFin?>" onfocus="displayCalendar(document.forms[0].fechaFin,'yyyy-mm-dd',this)" readonly="readonly" />
                    </dd>
                </dl>
                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
                    	<input type="reset" value="Restablecer" id="restablecer">
                    </dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    
</div>                     