<script type="text/javascript">
    jQuery(document).ready(
        function($){
            
        }   
    );
</script>
<div id="contentUsers">
    <form id="editInfo" name="editInfo" method="post" class="niceform" action="<?=base_url()?>admin/dashboard/modificarcontrasena_do">
        <?=form_fieldset('Mi Cuenta'); ?>
            <dl>
                <dt><label for="nombre">Nombre</label></dt>
                <dd>
                    <input type="text" name="nombre" id="nombre" value="<?=$user_info->nombre?>" readonly="readonly"/>
                    
                </dd>
            </dl>
            
            <dl>
                <dt><label for="email">Email</label></dt>
                <dd><input type="text" name="email" id="email" value="<?=$user_info->emailUsuario?>" readonly="readonly"/></dd>
            </dl>
            
            <dl>
                <dt><label for="puesto">Puesto</label></dt>
                <dd><input type="text" name="puesto" id="puesto" value="<?=$user_info->puesto?>" readonly="readonly"/></dd>
            </dl>
            
            <dl>
                <dt>&nbsp;</dt>
                <dd>CAMBIAR CONTRASE&Ntilde;A</dd>
            </dl>
            
            <dl>
                <dt><label for="contrasenaUsuarioActual">Contrase&ntilde;a actual</label></dt>
                <dd><input type="password" name="contrasenaUsuarioActual" id="contrasenaUsuarioActual"/></dd>
            </dl>
            
            <dl>
                <dt><label for="contrasenaUsuario">Nueva contrase&ntilde;a</label></dt>
                <dd><input type="password" name="contrasenaUsuario" id="contrasenaUsuario"/></dd>
            </dl>
            
            <dl>
                <dt><label for="contrasenaUsuario2">Repetir nueva contrase&ntilde;a</label></dt>
                <dd><input type="password" name="contrasenaUsuario2" id="contrasenaUsuario2"/></dd>
            </dl>
            
            <dl>
                <dt><label>&nbsp;</label></dt>
                <dd>
                    <input type="submit" value="Cambiar">
                </dd>
            </dl>
        <?=form_fieldset_close(); ?>
    </form>                
</div>              

        