<script type="text/javascript">
    jQuery(document).ready(
        function($){
                      
            $("#editar").click(
                function(e){
                    e.preventDefault();
                    if($("#idFraccionamiento").val() == 0 || $("#idPrototipo").val() == 0){
                        noty({
                            text : 'POR FAVOR SELECCIONE EL FRACCIONAMIENTO Y EL PROTOTIPO QUE DESEA REGISTRAR',
                            type : 'warning',
                            dismissQueue: true,
                            layout: 'top',
                            theme: 'default',
                            timeout: 2000
                        });
                                                
                    } else{
            			noty({
	                      text: 'Realmente desea actualizar este registro? Se actualizará todo lo relacionado con el mismo.',
	                      type: 'alert',
	                      dismissQueue: true,
	                      layout: 'center',
	                      theme: 'default',
	                      buttons: [
	                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
	                            $noty.close();
	                       		
								$("#editUser").submit();                                 
	                          }
	                        },
	                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
	                            $noty.close();
	                            return false;
	                          }
	                        }
	                      ]
	                    });            
                    }
                }
            );
            
        }
    );
</script>
<div id="contentUsers">
        
        <form id="editUser" name="editUser" method="post" class="niceform" action="<?=base_url()?>admin/prototiposfrac/editar_do">
        	<input type="hidden" name="idFraccionamientoPrototipo" id="idFraccionamientoPrototipo" value="<?=$info->idFraccionamientoPrototipo?>">
            <?=form_fieldset('Editar prototipo asignado'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd> 
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento">
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
									if($info->idFraccionamiento == $key->idFraccionamiento):
									?>
											<option value="<?=$key->idFraccionamiento?>" selected="selected"> <?=$key->nombreFrac?> </option>
									<?php
										else:
									?>
										<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
									<?php
									endif;
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="idPrototipo">Prototipo</label></dt>
                    <dd>
                    	<select size="1" name="idPrototipo" id="idPrototipo">
                            <option value="0">- - - -</option>
                            <?php
                            if($prototipos != null):
								foreach ($prototipos as $key):
									if($info->idPrototipo == $key->idPrototipo):
									?>
											<option value="<?=$key->idPrototipo?>" selected="selected"> <?=$key->nombrePrototipo?> </option>
									<?php
										else:
									?>
											<option value="<?=$key->idPrototipo?>"> <?=$key->nombrePrototipo?> </option>
									<?php
									endif;
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd>
                    	<input type="submit" value="Guardar" id="editar">
					</dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
  	
</div>              

        