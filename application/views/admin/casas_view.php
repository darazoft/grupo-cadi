<script type="text/javascript">
    jQuery(document).ready(
        function($){
            var open = false;
            speed = 500;
            $(".hideForm").hide();
            
            $(".shContent").click(
                function(){
                    if(!open){                        
                        $(".hideForm").show(speed);
                        open = true;                        
                    } else{
                        $(".hideForm").hide(speed);
                        open = false;                        
                    }
                }
            );
            
            jQuery("#newUser").validationEngine( 'attach', {
          		
          		onValidationComplete: function(form, status){
	            if (status == true) {
                        $.ajax({
                            url      : '<?=base_url()?>admin/casas/nuevo_do/',
                            type     : 'POST',
                            dataType : 'json',
                            data     : form.serialize(),
                            success  : function(data){
                                if(data.response=='true'){
                                    $(".hideForm").hide(speed, function(){
                                        open = false;
                                        $("#newUserBody").prepend(data.html);
                                        noty({
                                            text : 'CASA AGREGADA SATISFACTORIAMENTE',
                                            type : 'success',
                                            dismissQueue: true,
                                            layout: 'top',
                                            theme: 'default',
                                            timeout: 2000
                                        });                                        
                                    });
                                    
                                    /*** Limpiamos formulario ***/
                                    $('#newUser').each (function(){
									  this.reset();
									});
                                                                        
                                }
                                else if(data.response=='error_val') {
                                
	                            	noty({
				                            text : 'OCURRIO UN ERROR AL ACTUALIZAR TU INFORMACION, INTENTA NUEVAMENTE',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }          
                                else if(data.response=='false') {
                                
	                            	noty({
				                            text : 'EXISTE UN REGISTRO SIMILAR EN LA BASE DE DATOS, VERIFIQUE LA INFORMACIÓN',
				                            type : 'error',
				                            dismissQueue: true,
				                            layout: 'top',
				                            theme: 'default',
				                            timeout: 4000
				                        }); 	   
                                }                             
                            }                            
                        })
                       } // cierra if status true 
                    }
                }
            );
            
            
            $(".optsPane").live(
                'mouseover',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().show();
                }
            );
            
            $(".optsPane").live(
                'mouseleave',
                function(){
                    var thisID = $(this).attr('id');
                    $("#optsPane" + thisID).stop().hide();
                }
            );
                       
            $(".deleteRow").live(
                'click',
                function(e){
                    e.preventDefault();
                    var thisID = $(this).attr('href');
                    noty({
                      text: 'Realmente desea eliminar este registro? Se eliminará todo lo relacionado con el mismo.',
                      type: 'alert',
                      dismissQueue: true,
                      layout: 'center',
                      theme: 'default',
                      buttons: [
                        {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                            $noty.close();
                            $.ajax({
                                url : '<?=base_url()?>admin/casas/eliminar',
                                type : 'POST',
                                dataType : 'json',
                                data : 'idCasa=' + thisID + '&tipo=deleted',
                                success: function(data){
                                    if(data.response == 'true'){
                            			$("#" + thisID).stop().hide('slow');            
                                    }
                                    else {
	                                     	noty({
					                            text : 'OCURRIÓ UN ERROR AL ELIMINAR EL REGISTRO. INTÉNTE NUEVAMENTE.',
					                            type : 'error',
					                            dismissQueue: true,
					                            layout: 'top',
					                            theme: 'default',
					                            timeout: 4000
					                        });   
                                     }
                                }
                            })
                            
                          }
                        },
                        {addClass: 'btn btn-danger', text: 'Cancel', onClick: function($noty) {
                            $noty.close();
                            return false
                          }
                        }
                      ]
                    });                    
                }
            );
            
        }
    );
    
    function touchStart(event,id) {
	  // Insert your code here
	  try{
	  	document.getElementById("optsPane"+id).style.display = 'block';
	  }
	  catch(e){ alert(e); }
	  
	}
</script>
<div id="contentUsers">
    <form class="niceform">
    	<input type="button" value="Nueva Casa" class="shContent" />
    </form>
    <br />
    <div id="newUserContainer" class="hideForm">
        <form id="newUser" name="newUser" method="post" class="niceform">
            <?=form_fieldset('Nueva casa'); ?>
                <dl>
                    <dt><label for="idFraccionamiento">Fraccionamiento</label></dt>
                    <dd>
                    	<select size="1" name="idFraccionamiento" id="idFraccionamiento" class="validate[required]">
                            <option value="">- - - -</option>
                            <?php
                            if($fraccionamientos != null):
								foreach ($fraccionamientos as $key):
								?>
									<option value="<?=$key->idFraccionamiento?>"> <?=$key->nombreFrac?> </option>
								<?php
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="idPrototipo">Prototipo</label></dt>
                    <dd>
                    	<select size="1" name="idPrototipo" id="idPrototipo" class="validate[required]">
                            <option value="">- - - -</option>
                            <?php
                            if($prototipos != null):
								foreach ($prototipos as $key):
								?>
									<option value="<?=$key->idPrototipo?>"> <?=$key->nombrePrototipo?> </option>
								<?php
								endforeach;
							endif;
                            ?>                            
                        </select>                    
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="numCasa">N. Casa</label></dt>
                    <dd><input type="text" name="numCasa" id="numCasa" value="" class="validate[required, custom[integer], minSize[1], maxSize[3], min[1]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="interior">Int. Ejem.(B)</label></dt>
                    <dd><input type="text" name="interior" id="interior" value="" class="validate[minSize[1], maxSize[3]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="metros">Metros <sup>2</sup> Terreno</label></dt>
                    <dd><input type="text" name="metros" id="metros" value="" class="validate[required, custom[number], minSize[3]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="metrosConstruccion">Metros <sup>2</sup> Construcci&oacute;n</label></dt>
                    <dd><input type="text" name="metrosConstruccion" id="metrosConstruccion" value="" class="validate[required, custom[number], minSize[3]] text-input"></dd>
                </dl>
                
                <dl>
                    <dt><label for="acabados">Acabados</label></dt>
                    <dd><textarea name="acabados" id="acabados" class="" ></textarea></dd>
                </dl>
                
                <dl>
                    <dt><label for="orientacion">Orientaci&oacute;n</label></dt>
                    <dd>
                    	<select size="1" name="orientacion" id="orientacion" class="">
                            <option value="">----</option>
                            <option value="1">Norte</option>
                            <option value="2">Sur</option>
                            <option value="3">Oriente</option>
                            <option value="4">Poniente</option>
                        </select>
                    </dd>
                </dl>
                
                <dl>
                    <dt><label for="precioLista">Precio de Lista $</label></dt>
                    <dd><input type="text" name="precioLista" id="precioLista" value="" class=""></dd>
                </dl>
                
                <dl>
                    <dt><label for="statusVenta">Estatus</label></dt>
                    <dd>
                    	<select size="1" name="statusVenta" id="statusVenta" class="validate[required]">
                            <option value="">----</option>
                            <option value="1" selected="selected"> Disponible </option>
                            <option value="2"> Apartada </option>
                            <option value="3"> Con contrato </option>
                            <option value="4"> Escriturada </option>
                            <option value="5"> Entregada </option>
                        </select>
                    </dd>
                </dl>
                                                              
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Agregar Casa"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>                
    </div>
    
    <table id="usersTable" class="resultTable">		
		<thead>
		    <tr>
                <th>Fraccionamiento</th> <th>N. Casa</th> <th>Prototipo</th> <th>M <sup>2</sup></th> <th>Precio de lista</th><th>Fecha de Registro</th> <th> Venta </th> <th class="optionsPane">Estatus</th>
            </tr>		    
		</thead>
		<tbody id="newUserBody">
		<?php
        if($casas != null):
            $strong = true;
            $class = '';
            
        foreach ($casas as $key):
            if($strong):
                $class = 'strong';
                $strong = false;
            elseif(!$strong):
                $class = 'light';
                $strong = true;
            endif; ?>
			<tr id="<?=$key->idCasa?>" class="<?=$class?> optsPane">
				<td><?=$key->nombreFrac?></td>
				<td><?=$key->numCasa?> <?=$key->interior?></td>
				<td><?=$key->nombrePrototipo?></td>
				<td><?=$key->metros?></td>
				<td>$<?=number_format($key->precioLista,2)?></td>
				
				<td><?=getFormatDate($key->fechaRegistro,true)?></td>
				
				<td>
			    <?php 
			    	switch ($key->statusVenta):
						case '1':
							echo 'Disponible';		
						break;
						
						case '2':
							echo 'Apartada';		
						break;
						
						case '3':
							echo 'Con contrato';		
						break;
						
						case '4':
							echo 'Escriturada';		
						break;
						
						case '5':
							echo 'Entregada';		
						break;
					endswitch; 
			    ?>
				</td>
				
				<td class="optionsPane" ontouchstart="touchStart(event,<?=$key->idCasa?>);">
				    
				    <?php if($key->statusCasa == '1'): ?>
					    <span class="">
					        Activo
					    </span>
				    
					    <div id="optsPane<?=$key->idCasa?>">
					        <a id="editRow<?=$key->idCasa?>" href="<?=base_url()?>admin/casas/editar/<?=$key->idCasa?>">
					            <img src="<?=base_url()?>img/edit_row.png" />
					        </a>
					        
					        <a id="deleteRow<?=$key->idCasa?>" href="<?=$key->idCasa?>" class="deleteRow">				            
			                    <img src="<?=base_url()?>img/delete_row.png" />
			                </a>				       
				    	</div>
				    <?php else: ?>
				        Inactivo/<?=$key->statusTipo?>
				    <?php endif; ?>
				</td>
				
			</tr>
		<?php 
			   endforeach;		
		endif;
		?>
		</tbody>
	</table>
</div>                     