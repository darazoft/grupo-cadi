    <script type="text/javascript">
        jQuery(document).ready(
            function($){                
                $("#formLogin").submit(
                    function(){
                        if($("#emailUsuario").val() != '' || $("#contrasenaUsuario").val() != ''){
                            return true;
                            
                        } else{
                            noty({
                                text : 'INGRESE SU NOMBRE DE USUARIO Y CONTRASE&Ntilde;A',
                                type : 'warning',
                                dismissQueue: true,
                                layout: 'top',
                                theme: 'default',
                                timeout: 2000
                            });
                            return false;
                        }
                    }
                );
            }
        );
    </script>
    
    
    <div id="header">
        <div class="logo">  </div>
        
        <h1>&nbsp;</h1>
        
    </div>
    <div id="leContent">        
        <form id="formLogin" name="formLogin" method="post" action="sesion/login/admin/false" class="niceform">
            <?=form_fieldset('Autentificaci&oacute;n'); ?>
                <dl>
                    <dt><label for="emailUsuario">Usuario</label></dt>
                    <dd><input type="text" name="emailUsuario" id="emailUsuario" value="<?php echo set_value('emailUsuario'); ?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="contrasenaUsuario">Contrase&ntilde;a</label></dt>
                    <dd><input type="password" name="contrasenaUsuario" id="contrasenaUsuario" value="<?php echo set_value('emailUsuario'); ?>"></dd>
                </dl>
                
                <dl>
                    <dt><label for="recordarme">Recordarme</label></dt>
                    <?php //Excepcion: unicamente en checkbox puedo ocupar directamente $_POST ?>
                    <dd>
                        <label class="label_check" for="recordarme">
                            <input type="checkbox" name="recordarme" id="recordarme" value="true" <?php echo ((!empty($_POST['recordarme']))?('checked'):(''));?> />                                             
                        </label>
                        
                    </dd>
                </dl>
                
                <!-- <dl>
                    <dt><label for="recordarme2">Recordarme2</label></dt>
                    <?php //Excepcion: unicamente en checkbox puedo ocupar directamente $_POST ?>
                    <dd>
                        <label class="label_radio" for="recordarme2">
                            <input type="radio" name="recordarme2" id="recordarme2"/>                                             
                        </label>
                        
                        <label class="label_radio">
                            <input type="radio" name="recordarme2" id="recordarme3"/>                                             
                        </label>
                    </dd>
                </dl> -->
                
                <dl>
                    <dt><label>&nbsp;</label></dt>
                    <dd><input type="submit" value="Entrar"></dd>
                </dl>
            <?=form_fieldset_close(); ?>
        </form>        
    </div>    