<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
    }
	
	function index() {		
		$data['SYS_metaTitle'] 			= 'Grupo CADI | WEB-APP';
		$data['SYS_metaKeyWords'] 		= 'Grupo CADI | WEB-APP';
		$data['SYS_metaDescription'] 	= 'Grupo CADI | WEB-APP';
		$data['module'] 				= 'publico/principal_view';
		$this->load->view('publico/main_view', $data);
        redirect('/admin/');		
	}

	function enviar(){
		$this->load->library('email');
		
		// $config['protocol'] = "smtp";
        // $config['smtp_host'] = 'http://www.cadi.com';
        // $config['smtp_port'] = 25;
        // $config['smtp_user'] = 'info@santohm.com.mx';
        // $config['smtp_pass'] = 'Santo1920';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        $mensaje = '
            <p>Mensaje de: ' . $this->input->post('nombre') . '<br />
                Correo: ' . $this->input->post('email') . '<br />
                Tel&eacute;fon: '.$this->input->post('telefono').'<br />
                Estado / Ciudad: '.$this->input->post('estadoCiudad').'<br />
                    Mensaje: <br/>
                    ' . $this->input->post('mensaje') . '
                </p>
                <br/><br/>
                <p>Mensaje enviado desde www.cadi.com </p>
            ';

        $this->email->from($this->input->post('email'), $this->input->post('nombre'));
        $this->email->to('cadi@cadi.com');

        $this->email->subject('Contacto desde web');
        $this->email->message($mensaje);

        try {
            $this->email->send();
			echo '<div id="forma">
            Mensaje Enviado!
            </div>';
            // echo '<div id="forma">
            // <h1 style="width:100%; text-align:center; margin-top:100px;">Gracias por enviar su correo</h1>
            // </div>';
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }		
	}
}