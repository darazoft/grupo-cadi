<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sesion extends CI_Controller {

	public function __construct(){
		parent::__construct();
    }
	
	function index(){
		/*
			Si se accede directamente se hace una redireccion, a donde?
			default al controlador default.
		*/
		redirect('');
	}
	
	function login($loginTo, $cookie){
		if($cookie == "true"){
			//Recupero variables con FlashData del programa que detecto la cookie
			$usuario 			= $this->session->flashdata('usuario');
			$contrasenaUsuario 	= $this->session->flashdata('contrasenaUsuario');
			$recordarme 		= 'true';
			if($usuario===false || $contrasenaUsuario===false){
				// Error, en los datos enviados
				redirect($loginTo);
				return false;
			}
		}else{
			//Recupero variables de formulario y las valido
			
			// Reglas para validacion de campos de lado del servidor
			$this->form_validation->set_rules('emailUsuario'		,'Usuario'			,'trim|required|xss_clean');
			$this->form_validation->set_rules('contrasenaUsuario'	,'Contrase&ntilde;a','trim|required|xss_clean|md5');
			$this->form_validation->set_rules('recordarme'			,'Recordarme'		,'trim|xss_clean');
			$this->form_validation->set_message('required','El campo "%s" es requerido');
			$this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
			$this->form_validation->set_error_delimiters('<span class="error">','</span>');
		
			// Ejecuto la validacion de campos de lado del servidor
			if(!$this->form_validation->run()) {
				$this->session->set_flashdata('error','error_4');
                $this->session->set_flashdata('notyType','error');
				redirect($loginTo);
				return false;
			}else{
				$usuario 			= $this->input->post('emailUsuario');
				$contrasenaUsuario 	= $this->input->post('contrasenaUsuario');
				$recordarme 		= $this->input->post('recordarme');
			}
		}

		// Cargo el control de usuario
		$this->load->model('usuario_model');
			
		// Usuario y contrasena correcta?
		if($this->usuario_model->login_correct($usuario,$contrasenaUsuario,$cookie)){
			// Me loggeo
			if($this->usuario_model->login_user($usuario,$recordarme,$this->config->item('encryption_key'), $this->input)){
				redirect($loginTo);
				return true;
			}
		}
		
		// Error al loggearse, envio error.				
		$this->session->set_flashdata('error','error_4');
        $this->session->set_flashdata('notyType','error');                
		redirect($loginTo);
	}
	
	function logout($logoutTo) {
		//Borro cookies
		if($this->input->cookie($this->config->item('encryption_key').'emailUsuario') != FALSE || $this->input->cookie($this->config->item('encryption_key').'contrasenaUsuario') != FALSE ){
			$cookieUser = array(
    					'name'   => 'emailUsuario',
   						'value'  => '',
						'expire' => '',
						'path'   => '/',
						'prefix' => $this->config->item('encryption_key'),
						'secure' => FALSE
						);
			$cookiePass = array(
    					'name'   => 'contrasenaUsuario',
   						'value'  => '',
						'expire' => '',
						'path'   => '/',
						'prefix' => $this->config->item('encryption_key'),
						'secure' => FALSE
						);
			$this->input->set_cookie($cookieUser);
			$this->input->set_cookie($cookiePass);
    	}
    	//Destruyo Session
		$this->session->sess_destroy();
		redirect($logoutTo);
	}
	
}