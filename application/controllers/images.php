<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Images extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('image_lib');
    }

    public function thumbnailer($width, $height, $carpeta, $img) {
        $config['source_image'] = 'docs/' . $carpeta . '/' . $img;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['dynamic_output'] = true;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
	
	public function portada($width, $height, $carpeta, $img) {
        $config['source_image'] = 'imgs/' . $carpeta . '/' . $img;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['dynamic_output'] = true;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
}
?>