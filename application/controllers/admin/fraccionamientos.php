<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fraccionamientos extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 4)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
            $this -> load -> model ('tabla_fraccionamiento_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		
		switch ($this->idRol):
			case '1':
        		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
			break;
			
			case '8':
        		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
			break;
		endswitch;
		
        $data['module'] = 'admin/fraccionamiento_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
        $this->form_validation->set_rules('nombreFrac','Nombre Fraccionamiento','trim|required|xss_clean');
        $this->form_validation->set_rules('direccion','Direcci&oacute;n','trim|required|xss_clean');
        $this->form_validation->set_rules('telefono1','Telefono 1','trim|xss_clean');
        $this->form_validation->set_rules('telefono2','Telefono 2','trim|xss_clean');
        $this->form_validation->set_rules('unidades','Unidades','trim|required|xss_clean');
		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $nombreFrac = $this->input->post('nombreFrac');
            $direccion = $this->input->post('direccion');
            $telefono1 = $this->input->post('telefono1');
			$telefono2 = $this->input->post('telefono2');
			$unidades = $this->input->post('unidades');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusFrac  = 1;
		            
            $arrData = array(
                'nombreFrac' => $nombreFrac,
                'direccion' => $direccion,
                'telefono1' => $telefono1,
                'telefono2' => $telefono2,
                'unidades' => $unidades,
                'fechaRegistro' => $fechaRegistro,
                'statusFrac' => $statusFrac,
                'idUser' => $this->idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusFrac' => '1'                
            );
			
			$arrDataLike = array(
                'nombreFrac' => $nombreFrac
            );
            
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'fraccionamiento', true ,$arrDataWhere,$arrDataLike);           
            if($lastID > 0) {
                $data['response'] = 'true';
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                    <td>' . $nombreFrac . '</td>
                    <td>' . $direccion . '</td>
                    <td>' . $telefono1 . '</td>
                    <td>' . $telefono2 . '</td>
                    <td>' . $unidades . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/fraccionamientos/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';                                                                
            }
			else {
				$data['response'] = 'false';
			}
			
            echo json_encode($data);            
        }       
    }

	function editar($idFraccionamiento = null){
        if($idFraccionamiento == null){
            redirect('admin/fraccionamientos');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Fraccionamientos';
        //$data['pestana'] = 1;
        
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('fraccionamiento', 'idFraccionamiento', $idFraccionamiento, null);
        
        if($data['info'] == null){            
            redirect('admin/fraccionamientos');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_fraccionamiento_view';
                        
        $this->load->view('admin/main_view', $data);
    }
	
	function editar_do(){
		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
        $this->form_validation->set_rules('nombreFrac','Nombre Fraccionamiento','trim|required|xss_clean');
        $this->form_validation->set_rules('direccion','Direcci&oacute;n','trim|required|xss_clean');
        $this->form_validation->set_rules('telefono1','Telefoono 1','trim|xss_clean');
        $this->form_validation->set_rules('telefono2','Telefono 2','trim|xss_clean');
        $this->form_validation->set_rules('unidades','Unidades','trim|required|xss_clean');
		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/fraccionamientos');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
            $nombreFrac = $this->input->post('nombreFrac');
            $direccion = $this->input->post('direccion');
            $telefono1 = $this->input->post('telefono1');
			$telefono2 = $this->input->post('telefono2');
			$unidades = $this->input->post('unidades');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusFrac  = 1;
		            
            $arrData = array(
                'nombreFrac' => $nombreFrac,
                'direccion' => $direccion,
                'telefono1' => $telefono1,
                'telefono2' => $telefono2,
                'unidades' => $unidades,
                'fechaRegistro' => $fechaRegistro,
                'statusFrac' => $statusFrac,
                'idAnterior' => $idFraccionamiento,
                'idUser' => $this->idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFraccionamiento' => $idFraccionamiento
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusFrac' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
            
			if($this -> defaultdata_model -> updateInfo($arrData, 'fraccionamiento', $arrDataWhere, $idFraccionamiento, $arrDataUpdate) ) {
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/fraccionamientos');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/fraccionamientos');            
			}
			            
        }       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idFraccionamiento = $this->input->post('idFraccionamiento');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusFrac' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idFraccionamiento' => $idFraccionamiento
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'fraccionamiento', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}