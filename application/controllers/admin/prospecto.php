<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prospecto extends CI_Controller {
    var $myPath = "";
    var $idRol = "";
    var $idUsuario = "";
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    // if(!tengo_permiso($this -> session -> userdata('idRol'), 12)){
		        // //idRol, idPermiso
		        // $this->session->set_flashdata('error', 'error_1');
                // $this->session->set_flashdata('notyType', 'warning');                
                // redirect('admin/dashboard');            
		    // }			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tabla_usuario_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
			$this -> load -> model ('tablaestatuscli_model');
			$this -> load -> model ('prospecto_model');
            $this -> load -> model('defaultdata_model');
			$this->load->helper(array('form', 'url'));
            $this->load->library('upload');
            $this->load->library('image_lib');
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}        
        $this->idRol = $this -> session -> userdata('idRol');
        $this->myPath = getMyPath($this->idRol);
        $this->idUsuario = $this -> session -> userdata('idUsuario');
    }
    
    function index() {
    	
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		$idRol = $this -> session -> userdata('idRol');
		$idUsuario = $this -> session -> userdata('idUsuario');
		
		switch ($idRol):
			case '1':
				$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
				$data['coordinadores'] = $this -> tabla_usuario_model -> getUsuarioTipo('2');
				$data['asesores'] = $this -> tabla_usuario_model -> getUsuarioTipo('3');
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);						
			break;
			
			case '2':
				$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getMisFraccionamientos($this->idUsuario);
				$data['asesores'] = $this -> tabla_usuario_model -> getMisUsuarios($this->idUsuario);
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);						
			break;
						
			case '4':
				$data['prospectos'] = $this -> prospecto_model -> getProspectos($idUsuario,null,false);
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);						
			break;
			
			case '8':
				$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
				$data['coordinadores'] = $this -> tabla_usuario_model -> getUsuarioTipo('2');
				$data['asesores'] = $this -> tabla_usuario_model -> getUsuarioTipo('3');
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);				
			break;
		endswitch;
		
				        
        $data['module'] = 'admin/' . $this->myPath . 'hojavida_view';
        $this->load->view('admin/main_view', $data);        
    }

	function misprospectos() {
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
				
		switch ($this->idRol):
						
			case '2':
				$data['misprospectos'] = $this -> prospecto_model -> getProspectos($this->idUsuario,null,false);						
			break;
			
			case '3':
				//$data['misprospectos'] = $this -> prospecto_model -> getProspectos($this->idUsuario,null,false);
				$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getMisFraccionamientos($this->idUsuario);
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);				
				$data['idUsuario'] = $this ->idUsuario;				
			break;
			
		endswitch;
		
				        
        $data['module'] = 'admin/' . $this->myPath . 'misprospectos_view';
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
    	
		if(!tengo_permiso($this -> session -> userdata('idRol'), 12)){
	        //idRol, idPermiso
	        $this->session->set_flashdata('error', 'error_1');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/dashboard_view');            
		}
		
    	$idRol = $this -> session -> userdata('idRol');
			
		switch ($idRol):
			case '1':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '2':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '3':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '4':
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '8':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
		endswitch;
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            	
            $data['response'] = 'error_val';
            echo json_encode($data);            
        } else{
                   
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idEstatusCliente = $this->input->post('idEstatusCliente');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idUsuario = $this->input->post('idUsuario');
			$fecha = $this->input->post('fecha');
			$hora = date('H:i');
			$tipo = $this->input->post('tipo');
			$apaterno = $this->input->post('apaterno');
			$amaterno = $this->input->post('amaterno');
			$nombreCliente = $this->input->post('nombreCliente');
			$email = $this->input->post('email');
			$ladaFijo = $this->input->post('ladaFijo');
			$telFijo = $this->input->post('telFijo');
			$ladaCelular = $this->input->post('ladaCelular');
			$telCelular = $this->input->post('celular');
			$q1 = $this->input->post('q1');
			$asesor = $this->input->post('asesor');
			$q2 = $this->input->post('q2');
			$q2otro = '';
			if($q2 == 8) {
				$q2otro = $this->input->post('q2otro');
			}
			$nombreConyuge = $this->input->post('nombreConyuge');
			$q3 = $this->input->post('q3');
			$q17 = $this->input->post('q17');
			$observaciones = $this->input->post('observaciones');
			
			if($idRol==1 || $idRol == 8 || $idRol == 2 || $idRol == 3) {
				$q4 = $this->input->post('q4');
				$q5 = $this->input->post('q5');
				$q6 = $this->input->post('q6');
				$q7 = $this->input->post('q7');
				$nss = $this->input->post('nss');
				$curp = $this->input->post('curp');
				$ingresoComprobable = $this->input->post('ingresoComprobable');
				$entregaRequerida = $this->input->post('entregaRequerida');
				$q8 = $this->input->post('q8');
				$q9 = $this->input->post('q9');
				$q10 = $this->input->post('q10');
				$q10xque = $this->input->post('q10xque');
				$q11 = $this->input->post('q11');
				$q12 = $this->input->post('q12');
				$q13 = $this->input->post('q13');
				$q14 = $this->input->post('q14');
				$q15 = $this->input->post('q15');
				$q16 = $this->input->post('q16');
			}
			
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusHoja  = 1;
		    
			if($idRol==1 || $idRol == 8 || $idRol == 2 || $idRol == 3) {        
	            $arrData = array(
	                'idFraccionamiento' => $idFraccionamiento, 'idUsuario' => $idUsuario,
	                'idEstatusCliente' => $idEstatusCliente,
	                'fecha' => $fecha, 'hora' => $hora,
	                'tipo' => $tipo, 'apaterno' => $apaterno, 'amaterno' => $amaterno, 'nombreCliente' => $nombreCliente,
	                'email' => $email, 'ladaFijo' => $ladaFijo, 'telFijo' => $telFijo,
	                'ladaCelular' => $ladaCelular, 'telCelular' => $telCelular, 'q1' => $q1,
	                'asesor' => $asesor, 'q2' => $q2, 'q2otro' => $q2otro,
	                'nombreConyuge' => $nombreConyuge, 'q3' => $q3,
	                'q4' => $q4, 'q5' => $q5,
	                'q6' => $q6, 'q7' => $q7,
	                'nss' => $nss, 'curp' => $curp,
	                'ingresoComprobable' => $ingresoComprobable, 'entregaRequerida' => $entregaRequerida,
	                'q8' => $q8, 'q9' => $q9,
	                'q10' => $q10, 'q10xque' => $q10xque, 'q11' => $q11,
	                'q12' => $q12, 'q13' => $q13,
	                'q14' => $q14, 'q15' => $q15,
	                'q16' => $q16, 'q17' => $q17, 
	                'observaciones' => $observaciones, 'fechaRegistro' => $fechaRegistro,
	                'statusHoja' => $statusHoja,
	                'idUser' => $this->idUsuario
	            );
	        }
			else if($idRol == 4) {
				
				$arrData = array(
	                'idFraccionamiento' => $idFraccionamiento, 'idUsuario' => $idUsuario,
	                'idEstatusCliente' => $idEstatusCliente,
	                'fecha' => $fecha, 'hora' => $hora,
	                'tipo' => $tipo, 'apaterno' => $apaterno, 'amaterno' => $amaterno, 'nombreCliente' => $nombreCliente,
	                'email' => $email, 'ladaFijo' => $ladaFijo, 'telFijo' => $telFijo,
	                'ladaCelular' => $ladaCelular, 'telCelular' => $telCelular, 'q1' => $q1,
	                'asesor' => $asesor, 'q2' => $q2, 'q2otro' => $q2otro,
	                'nombreConyuge' => $nombreConyuge, 'q3' => $q3,
	                'fechaRegistro' => $fechaRegistro,
	                'statusHoja' => $statusHoja,
	                'idUser' => $this->idUsuario
	            );
			}
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusHoja' => '1'
            );
			
			$cliente = $apaterno.' '.$amaterno.' '.$nombreCliente;
			$arrDataLike = array(
                'CONCAT(apaterno,\' \',amaterno,\' \',nombreCliente)' => $cliente 
            );
            
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'hojavida', true, $arrDataWhere, $arrDataLike);         
            if($lastID > 0) {
            	
            	$nombreFrac = '';
				$nombre = '';
            	$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFraccionamiento);
				$nombre = $this -> defaultdata_model -> getNameRow('nombre','usuario','idUsuario',$idUsuario);
				
               	$data['response'] = 'true';
                $data['html'] = '<tr class="light">
                	<td>' . $nombreFrac->nombreFrac . '</td>
                    <td>' . $nombre->nombre . '</td>
                    <td>' . $cliente . '</td>
                    <td>' . $ladaFijo .' '.$telFijo . '</td>                    
                    <td>' . $fechaRegistro . '</td>
                    <td>Activo</td>
                    <td align="right">
					    <a href="'.base_url().'admin/prospecto/editar/'.$lastID.'">
					        <img src="'.base_url().'img/edit_row.png" alt="Editar" />
					    </a>
					    &nbsp;
					    <a href="'.base_url().'admin/prospecto/actividades/'.$lastID.'">
	                        <img src="'.base_url().'img/task_row.png" alt="Actividades" />
	                    </a>
					    &nbsp;
					    <a id="deleteRow'.$lastID.'" href="'.$lastID.'" class="deleteRow">
	                        <img src="'.base_url().'img/delete_row.png" alt="Eliminar" />
	                    </a>
					</td>                 
                </tr>';
            }
			else {
				$data['response'] = 'false';
			} 
			
			$data['lastID'] = $lastID;
            echo json_encode($data);            
        }       
    }

	function editar($idHojaVida = null) {
		
        if($idHojaVida == null){
            redirect('admin/prospecto');
        }
        
		/*
		 * Validamos que el id pertenezca a mi usuario o asesores
		 * en caso de ser coordinador 
		 */
		if($this->idRol == 2 || $this->idRol == 3)
			if($this -> prospecto_model -> validaMiId($this->idRol, $this->idUsuario, $idHojaVida ) == 0)
				redirect('admin/dashboard');
		/* Validamos que el id pertenezca a mi usuario o asesores
		 * en caso de ser coordinador 
		 */
		
        $data['secciones'] = getMyMenu($this->idRol);
        $data['SYS_metaTitle'] = 'CADI';
		$data['SYS_metaDescription'] = 'Administracion | Prospectos - Editar';
		
		switch ($this->idRol):
			case '1':
				$data['this_prospecto'] = $this -> defaultdata_model -> getInfo('hojavida', 'idHojaVida', $idHojaVida);
		        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		        $data['asesores'] = $this -> tabla_usuario_model -> getUsuarioTipo('3');
				$data['coordinadores'] = $this -> tabla_usuario_model -> getUsuarioTipo('2');
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);						
			break;
			
			case '8':
				$data['this_prospecto'] = $this -> defaultdata_model -> getInfo('hojavida', 'idHojaVida', $idHojaVida);
		        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		        $data['asesores'] = $this -> tabla_usuario_model -> getUsuarioTipo('3');
				$data['coordinadores'] = $this -> tabla_usuario_model -> getUsuarioTipo('2');
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);						
			break;
			
			case '2':
				$data['this_prospecto'] = $this -> defaultdata_model -> getInfo('hojavida', 'idHojaVida', $idHojaVida);
		        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getMisFraccionamientos($this->idUsuario);
				$data['asesores'] = $this -> tabla_usuario_model -> getMisUsuarios($this->idUsuario);
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);
				$data['idUsuario'] = $this ->idUsuario;						
			break;
			
			case '3':
				$data['this_prospecto'] = $this -> defaultdata_model -> getInfo('hojavida', 'idHojaVida', $idHojaVida);
		        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getMisFraccionamientos($this->idUsuario);
				$data['clasificacion'] = $this -> tablaestatuscli_model -> getEstatus(true);
				$data['idUsuario'] = $this ->idUsuario;						
			break;
			
		endswitch;
		        
        /*********  DYNAMIC DATA  *********/
        $data['module'] = 'admin/' . $this->myPath . 'editar_hojavida_view';
        $this->load->view('admin/main_view', $data);
    }

	function editar_do() {
    	
		if(!tengo_permiso($this -> session -> userdata('idRol'), 21)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		}
	
    	$idRol = $this -> session -> userdata('idRol');
		
		switch ($idRol):
			case '1':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '2':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
			case '3':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
						
			case '8':
				$this->form_validation->set_rules('idEstatusCliente','Clasificacion','trim|required|xss_clean');
        		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
				$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
				$this->form_validation->set_rules('apaterno','Apellido Paterno','trim|required|xss_clean');
				$this->form_validation->set_rules('amaterno','Apellido Materno','trim|required|xss_clean');
				$this->form_validation->set_rules('nombreCliente','Nombre','trim|required|xss_clean');
				$this->form_validation->set_rules('email','Email','trim|xss_clean');
				$this->form_validation->set_rules('telFijo','Telefono Fijo','trim|xss_clean');
				$this->form_validation->set_rules('celular','Telefono Celular','trim|xss_clean');
			break;
			
		endswitch;
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/prospecto');            
            
        } else{
           
            //Preparamos arreglo para ir a insertar
            $idHojaVida = $this->input->post('idHojaVida');
            $idEstatusCliente = $this->input->post('idEstatusCliente');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idUsuario = $this->input->post('idUsuario');
			$fecha = $this->input->post('fecha');
			$hora = date('H:i');
			$tipo = $this->input->post('tipo');
			$apaterno = $this->input->post('apaterno');
			$amaterno = $this->input->post('amaterno');
			$nombreCliente = $this->input->post('nombreCliente');
			$email = $this->input->post('email');
			$ladaFijo = $this->input->post('ladaFijo');
			$telFijo = $this->input->post('telFijo');
			$ladaCelular = $this->input->post('ladaCelular');
			$telCelular = $this->input->post('celular');
			$q1 = $this->input->post('q1');
			$asesor = $this->input->post('asesor');
			$q2 = $this->input->post('q2');
			$q2otro = '';
			if($q2 == 8) {
				$q2otro = $this->input->post('q2otro');
			}
			$nombreConyuge = $this->input->post('nombreConyuge');
			$q3 = $this->input->post('q3');
			$q17 = $this->input->post('q17');
			$observaciones = $this->input->post('observaciones');
			
			$q4 = $this->input->post('q4');
			$q5 = $this->input->post('q5');
			$q6 = $this->input->post('q6');
			$q7 = $this->input->post('q7');
			$nss = $this->input->post('nss');
			$curp = $this->input->post('curp');
			$ingresoComprobable = $this->input->post('ingresoComprobable');
			$entregaRequerida = $this->input->post('entregaRequerida');
			$q8 = $this->input->post('q8');
			$q9 = $this->input->post('q9');
			$q10 = $this->input->post('q10');
			$q10xque = $this->input->post('q10xque');
			$q11 = $this->input->post('q11');
			$q12 = $this->input->post('q12');
			$q13 = $this->input->post('q13');
			$q14 = $this->input->post('q14');
			$q15 = $this->input->post('q15');
			$q16 = $this->input->post('q16');
			
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusHoja  = 1;
		          
	        $arrData = array(
	            'idFraccionamiento' => $idFraccionamiento, 'idUsuario' => $idUsuario,
	            'idEstatusCliente' => $idEstatusCliente,
	            'fecha' => $fecha, 'hora' => $hora,
	            'tipo' => $tipo, 'apaterno' => $apaterno, 'amaterno' => $amaterno, 'nombreCliente' => $nombreCliente,
                'email' => $email, 'ladaFijo' => $ladaFijo, 'telFijo' => $telFijo,
                'ladaCelular' => $ladaCelular, 'telCelular' => $telCelular, 'q1' => $q1,
	            'asesor' => $asesor, 'q2' => $q2, 'q2otro' => $q2otro,
	            'nombreConyuge' => $nombreConyuge, 'q3' => $q3,
	            'q4' => $q4, 'q5' => $q5,
	            'q6' => $q6, 'q7' => $q7,
	            'nss' => $nss, 'curp' => $curp,
	            'ingresoComprobable' => $ingresoComprobable, 'entregaRequerida' => $entregaRequerida,
	            'q8' => $q8, 'q9' => $q9,
	            'q10' => $q10, 'q10xque' => $q10xque, 'q11' => $q11,
	            'q12' => $q12, 'q13' => $q13,
	            'q14' => $q14, 'q15' => $q15,
	            'q16' => $q16, 'q17' => $q17, 
	            'observaciones' => $observaciones, 'fechaRegistro' => $fechaRegistro,
	            'statusHoja' => $statusHoja,
	            'idAnterior' => $idHojaVida,
	            'idUser' => $this->idUsuario
	        );
			
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idHojaVida' => $idHojaVida
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusHoja' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'hojavida', $arrDataWhere, $idHojaVida, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');
				
				switch ($this->idRol):
					case 1:
						redirect('admin/prospecto');		
					break;
					case 2:
						redirect('admin/prospecto');		
					break;
					case 3:
						redirect('admin/prospecto/misprospectos');		
					break;					
					case 8:
						redirect('admin/prospecto');		
					break;
				endswitch;                  
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                
				switch ($this->idRol):
					case 1:
						redirect('admin/prospecto');		
					break;
					case 2:
						redirect('admin/prospecto/misprospectos');		
					break;
					case 3:
						redirect('admin/prospecto/misprospectos');		
					break;					
					case 8:
						redirect('admin/prospecto');		
					break;
				endswitch;            
			}
	        
			
        }       
    }

	function eliminar(){
		
		if(!tengo_permiso($this -> session -> userdata('idRol'), 22)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		}
		
		//Arreglo para parsear por json            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idHojaVida = $this -> input-> post ('idHojaVida');
        $tipo = $this->input->post('tipo'); 
        $idUser = $this -> idUsuario;
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusHoja' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idHojaVida' => $idHojaVida
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'hojavida', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
        echo json_encode($data);
	}
    
	
    function actividades($idHojaVida = null){
    	
		if(!tengo_permiso($this -> session -> userdata('idRol'), 25)){
	        //idRol, idPermiso
	        $this->session->set_flashdata('error', 'error_1');
	        $this->session->set_flashdata('notyType', 'warning');                
	        redirect('admin/dashboard');            
		}
		
        if($idHojaVida == null) {
            redirect('admin/prospecto');
        }
				
        $data['secciones'] = getMyMenu($this->idRol);
        $data['this_prospecto'] = $this -> defaultdata_model -> getInfo('hojavida', 'idHojaVida', $idHojaVida);         
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Prospectos - Editar';
		
		$arrDataWhere = array(
			'idHojaVida' => $idHojaVida, 
			'statusActividad != ' => '2'
		);
		
		$data['actividades'] = $this -> prospecto_model -> getActividades($idHojaVida, $arrDataWhere);
		$actividades_comment = $this -> prospecto_model -> getActividades($idHojaVida, $arrDataWhere);
        $arrComments = array();
		
       	if($actividades_comment != null) {
            foreach($actividades_comment as $row){
                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
            }
        }
		
        $data['arrComments'] = $arrComments;
		
        $data['module'] = 'admin/' . $this->myPath . 'actividades_prospecto_view';
		
        $this->load->view('admin/main_view', $data); 
    }
    
    function do_upload($input, $carpeta, $size,  $width, $height, $filename = null) {
        if (!empty($_FILES[$input]['name'])) {
            if($filename == null)
                $filename = substr(md5(uniqid(rand())), 0, 10);
                        
            $config['upload_path'] = $carpeta;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|xls||xlsx|doc|docx|pdf|txt|ppt|pptx|pps|ppsx|zip|rar';
            $config['max_size'] = $size;
            $config['max_width'] = $width;
            $config['max_height'] = $height;
            $config['file_name'] = $filename;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input)) {
                return null;
            } else {
                $data = $this->upload->data();              
                return  $data['file_name'];
            }
        }
    } 
    
    function nueva_actividad_do(){
    	
		/* Validación para ingresar actividades
		 * solo a los prospectos que pertenezcan 
		 * al coordinador/asesor logueado
		 */
		$idHojaVida = $this->input->post('idHojaVida');
				
		if($this->idRol == 2 || $this->idRol == 3)
			if($this -> prospecto_model -> validaMiId($this->idRol, $this->idUsuario, $idHojaVida ) == 0)
				redirect('admin/prospecto');
		/* Validamos que el id pertenezca a mi usuario o asesores
		 * en caso de ser coordinador 
		*/

        $this->form_validation->set_rules('idHojaVida',' ','trim|required|xss_clean');
        $this->form_validation->set_rules('fechaInicio','Fecha inicio','trim|required|xss_clean');
        $this->form_validation->set_rules('horaInicio','Hora inicio','trim|required|xss_clean');
        $this->form_validation->set_rules('nombreActividad','Actividad','trim|required|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripcion','trim|required|xss_clean');
        $this->form_validation->set_rules('archivo','Archivo','trim|xss_clean');
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {            
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');
            //die(validation_errors());
            redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));            
        } else{
            $photo = $this->do_upload('archivo', 'docs/actividades', 3071,  2048, 2048);
            $arrInsert = array(
                'idHojaVida'      => $this -> input -> post('idHojaVida'),
                'nombreActividad' => $this -> input -> post('nombreActividad'),
                'descripcion'     => $this -> input -> post('descripcion'),
                'fechaInicio'     => $this -> input -> post('fechaInicio'). ' ' . $this ->input -> post('horaInicio') . ':00',
                'adjunto'         => $photo,
                'fechaRegistro'   => date('Y-m-d H:i:s'),
                'statusActividad' => '1',
                'idUser'          => $this->session->userdata('idUsuario')                 
            );
            
            if($this->defaultdata_model->addInfo($arrInsert, 'hojaactividad', false, null, null)){//ULTIMOS DOS PARAMETROS PARA COMPROBAR EN LA BASE QUE NO EXISTA UN REGISTRO SIMILAR
                $this->session->set_flashdata('error', 'error_6');
                $this->session->set_flashdata('notyType', 'success');
                redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));
            } else{
                $this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'warning');            
                redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));                
            }
        }
        
    }

    function editar_actividad($idHojaActividad = null){
    	
		if(!tengo_permiso($this -> session -> userdata('idRol'), 25)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		}
		
        if($idHojaActividad == null){
            redirect('admin/prospecto');
        }
        $data = array();
        $data['secciones'] = getMyMenu($this->idRol);                 
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Prospectos | Actividades - Editar';
        $data['actividad'] = $this -> defaultdata_model -> getInfo('hojaactividad', 'idHojaActividad', $idHojaActividad);
		
		$data['module'] = 'admin/' . $this->myPath . 'editar_actividad_prospecto_view';		
        $this->load->view('admin/main_view', $data);
    }
	
	function editar_actividad_do() {
		
		$this->form_validation->set_rules('idHojaActividad',' id Actividad','trim|required|xss_clean');
        $this->form_validation->set_rules('idHojaVida',' id Hoja','trim|required|xss_clean');
		$this->form_validation->set_rules('filename',' filename ','trim|xss_clean');
        $this->form_validation->set_rules('fechaInicio','Fecha inicio','trim|required|xss_clean');
        $this->form_validation->set_rules('horaInicio','Hora inicio','trim|required|xss_clean');
        $this->form_validation->set_rules('nombreActividad','Actividad','trim|required|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripcion','trim|required|xss_clean');
        $this->form_validation->set_rules('archivo','Archivo','trim|xss_clean');
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {            
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');
            //die(validation_errors());
            redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));
			            
        } else {
        	
			if(!empty($_FILES['archivo']['tmp_name']) ) {
				
				if($this->input->post('filename') != '') {
					$filename = explode(".", $this->input->post('filename'));
					$filename = $filename[0];
					$photo = $this->do_upload('archivo', 'docs/actividades', 3071,  2048, 2048, $filename);	
				}
				else {
					$photo = $this->do_upload('archivo', 'docs/actividades', 3071,  2048, 2048);
				}
			}
			else {
				$photo = $this->input->post('filename');
			}

			$idHojaActividad = $this -> input -> post('idHojaActividad');
			
            $arrInsert = array(
                'idHojaVida'      => $this -> input -> post('idHojaVida'),
                'nombreActividad' => $this -> input -> post('nombreActividad'),
                'descripcion'     => $this -> input -> post('descripcion'),
                'fechaInicio'     => $this -> input -> post('fechaInicio'). ' ' . $this ->input -> post('horaInicio') . ':00',
                'adjunto'         => $photo,
                'fechaRegistro'   => date('Y-m-d H:i:s'),
                'statusActividad' => '1',
                'idAnterior'          => $idHojaActividad,
                'idUser'          => $this->idUsuario                 
           	);
            
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idHojaActividad' => $idHojaActividad
           	);
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusActividad' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrInsert, 'hojaactividad', $arrDataWhere, $idHojaActividad, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/prospecto/actividades/' . $this -> input -> post('idHojaVida'));            
			}
        }
        
    }

	function editar_estado_actividad($idHojaActividad = null, $idHojaVida = null) {
		
		if($idHojaActividad == null || $idHojaVida == null){
			redirect('admin/prospecto');
		}

        $data = array();
        $data['secciones'] = getMyMenu($this->idRol);                 
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Prospectos | Actividades - Terminar';
        $data['idHojaActividad'] = $idHojaActividad;
		$data['idHojaVida'] = $idHojaVida;
		$data['prospecto'] = $this -> defaultdata_model -> getNameRow('CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) as prospecto', 'hojavida', 'idHojaVida', $idHojaVida);
		
		var_dump($data['prospecto']);
		
		$estatus = $this -> prospecto_model -> getEstatusHoja($idHojaVida);
		$data['estatusCliente'] = $estatus->clasificacion; 
		
		if( $estatus->clasificacion == 'F' || $estatus->clasificacion == 'f' ) {
				
			$data['module'] = 'admin/' . $this->myPath . 'terminar_actividad_A_view';	
		}
		else {
			$data['module'] = 'admin/' . $this->myPath . 'terminar_actividad_B_view';
		}
				
        $this->load->view('admin/main_view', $data);
	}
	
	function editar_estado_actividad_do($idHojaActividad = null, $idHojaVida = null) {
			
		if($idHojaActividad == null || $idHojaVida == null){
			redirect('admin/prospecto');
		}
				
		if( $this->input->post('estatusCliente') == 'F' || $this->input->post('estatusCliente') == 'f' ) {
			
			$this->form_validation->set_rules('estatusCliente',' ','trim|required|xss_clean');
			$this->form_validation->set_rules('motivo',' ','trim|required|xss_clean');	
		}
		else {		
			$this->form_validation->set_rules('motivo',' ','trim|required|xss_clean');
	        $this->form_validation->set_rules('fechaInicio','Fecha inicio','trim|required|xss_clean');
	        $this->form_validation->set_rules('horaInicio','Hora inicio','trim|required|xss_clean');
	        $this->form_validation->set_rules('nombreActividad','Actividad','trim|required|xss_clean');
	        $this->form_validation->set_rules('descripcion','Descripcion','trim|required|xss_clean');
	        $this->form_validation->set_rules('archivo','Archivo','trim|xss_clean');
        }
		
		$this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {            
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');
            //die(validation_errors());
            redirect('admin/prospecto/actividades/' . $idHojaVida);            
        } else{
        	
			$motivo = $this->input->post('motivo');
			$arrData = array(
				'motivo' => $motivo,
				'statusActividad' => '3'
			);
			
			$arrDataWhere = array (
				'idHojaActividad' => $idHojaActividad
			);
        	
			if( $this->input->post('estatusCliente') == 'F' || $this->input->post('estatusCliente') == 'f' ) {		
				/**
				 * Utilizamos modelo delete, para hacer un update en la base simplemente
				 */
				$this -> defaultdata_model -> deleteInfo($arrData, 'hojaactividad', $arrDataWhere);
			}
			elseif( $this->input->post('estatusCliente') != 'F' || $this->input->post('estatusCliente') != 'f' ) {
				
	            $photo = $this->do_upload('archivo', 'docs/actividades', 3071,  2048, 2048);
				
	           	$arrInsert = array(
	                'idHojaVida'      => $idHojaVida,
	                'nombreActividad' => $this -> input -> post('nombreActividad'),
	                'descripcion'     => $this -> input -> post('descripcion'),
	                'fechaInicio'     => $this -> input -> post('fechaInicio'). ' ' . $this ->input -> post('horaInicio') . ':00',
	                'adjunto'         => $photo,
	                'fechaRegistro'   => date('Y-m-d H:i:s'),
	                'statusActividad' => '1',
	                'idUser'          => $this->session->userdata('idUsuario')                 
	            );
				
				$this->defaultdata_model->addInfo($arrInsert, 'hojaactividad', false, null, null);		
				/**
				 * Utilizamos modelo delete, para hacer un update en la base simplemente
				 */
				$this -> defaultdata_model -> deleteInfo($arrData, 'hojaactividad', $arrDataWhere);	
			}
			
			switch($this->idRol):
				case 1:
					redirect('admin/prospecto/actividades/'.$idHojaVida); 
				break;
				
				case 2:
					redirect('admin/prospecto/actividades/'.$idHojaVida); 
				break;
				
				case 3:
					redirect('admin/dashboard'); 
				break;
				
				case 8:
					redirect('admin/prospecto/actividades/'.$idHojaVida); 
				break;
				
			endswitch;
	   }
	}
	
	function eliminar_actividad(){
		if(!tengo_permiso($this -> session -> userdata('idRol'), 25)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		}
		
		//Arreglo para parsear por json            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idHojaActividad = $this -> input-> post ('idHojaActividad');
        $tipo = $this->input->post('tipo'); 
        $idUser = $this -> idUsuario;
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusActividad' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idHojaActividad' => $idHojaActividad
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'hojaactividad', $arrDataWhere) > 0) {
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
        echo json_encode($data);
	}

	function nuevo_mensaje_do(){
		
        $this->form_validation->set_rules('idHojaActividad',' ','trim|required|xss_clean');
        $this->form_validation->set_rules('comentario','Comentario','trim|required|xss_clean');
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {            
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');
            $data['response'] = 'error_val';            
        } else{
            
			$idHojaActividad = $this -> input -> post('idHojaActividad');
			$comentario = $this -> input -> post('comentario');
			$fecha = date('Y-m-d H:i:s');
			
           	$arrInsert = array(
                'idHojaActividad'      => $idHojaActividad,
                'idUsuario' => $this -> idUsuario,
                'comentario'     => $comentario,
                'fechaRegistro'   => $fecha,
                'statusComentario' => '1',
                'idUser'          => $this->idUsuario                 
            );
            
			$lastID = $this->defaultdata_model->addInfo($arrInsert, 'comentario', false, null, null);
			$usuario = $this -> defaultdata_model -> getNameRow('nombre','usuario','idUsuario',$this->idUsuario);
			
						
            if($lastID > 0){//ULTIMOS DOS PARAMETROS PARA COMPROBAR EN LA BASE QUE NO EXISTA UN REGISTRO SIMILAR
                
                $data['response'] = 'true';
				$data['html'] = '<tr>
		                    <td colspan="2"> '.$comentario.' </td>
		                    <td colspan="2"> '.$usuario->nombre.' </td>
		                    <td colspan="2"> '.$fecha.' </td>
	                    </tr>';    
            } 
            else{
                $data['response'] = 'false';                
            }
						
			echo json_encode($data);
        }
        
    }

	function consulta_do () {
		
		$this->form_validation->set_rules('idFraccionamientoM','Fraccionamiento','trim|xss_clean');
		
		if($this->idRol != 3)
			$this->form_validation->set_rules('idUsuarioM','Asesor','trim|xss_clean');
		
		$this->form_validation->set_rules('fechaM','Fecha','trim|xss_clean');
		$this->form_validation->set_rules('idEstatusClienteM','Estatus','trim|xss_clean');
		$this->form_validation->set_rules('prospecto','Prospecto','trim|xss_clean');
		
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
		           
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);            
        } 
        else{
        	
			$idFraccionamiento = '';
			$idUsuario = '';
			$fecha = '';
			$idEstatusCliente = '';
			$prospecto = '';
			//Arreglo para parsear por json
	        $data = array();            
	        $data['response'] = 'true';
	        $data['html']     = '';
			$where = '';
			
			if($this->idRol != 3) {
				$datos = array(
						'idFraccionamiento' => $this->input->post('idFraccionamientoM'),
						'idUsuarioCon' 		=> $this->input->post('idUsuarioM'),
						'fecha'				=> $this->input->post('fechaM'),
						'idEstatusCliente'	=> $this->input->post('idEstatusClienteM'),
						'prospecto'			=> $this->input->post('prospecto') 	
				);
			}
			else {
				$datos = array(
						'idFraccionamiento' => $this->input->post('idFraccionamientoM'),
						'fecha'				=> $this->input->post('fechaM'),
						'idEstatusCliente'	=> $this->input->post('idEstatusClienteM'),
						'prospecto'			=> $this->input->post('prospecto') 	
				);
			}
			
			$this->session->set_userdata($datos);
				
			
			if($this->input->post('idFraccionamientoM') != "null")			
				$idFraccionamiento = explode(",",$this->input->post('idFraccionamientoM'));
			
			if($this->idRol != 3) {
				if($this->input->post('idUsuarioM') != "null")
					$idUsuario = explode(",",$this->input->post('idUsuarioM'));
			}
			
			if($this->input->post('fechaM') != "null")
				$fecha = $this->input->post('fechaM');
			
			if($this->input->post('idEstatusClienteM') != "null")
				$idEstatusCliente = explode(",",$this->input->post('idEstatusClienteM'));
			
			if($this->input->post('prospecto') != "null")
				$prospecto = $this->input->post('prospecto');
			
			//Recorremos arreglo de fraccionamiento
			if(!empty($idFraccionamiento) && !in_array("0", $idFraccionamiento) ) {
				
				for($i=0; $i<count($idFraccionamiento); $i++) {
					
					if($i == 0) 
						$where .='(';
					else if($i > 0)
						$where .= ' OR ';
					
					$where .= 'hv.idFraccionamiento='.$idFraccionamiento[$i];
				}
				$where .= ') ';
			}
			
			if( !empty($idUsuario) && !in_array('0', $idUsuario) ) {
				
				if($where != '')  // Si el arreglo anterior tiene valor concatenamos la nueva sentencia con AND
					$where .= ' AND ';
				
				for($i=0; $i<count($idUsuario); $i++) {
								
					if($i == 0) 
						$where .='(';
					else if($i > 0)
						$where .= ' OR ';
					
					$where .= 'hv.idUsuario='.$idUsuario[$i];
				}
				$where .= ') ';
			}
			
			if( !empty($idEstatusCliente) && !in_array('0', $idEstatusCliente) ) {
				
				if($where != '')  // Si el arreglo anterior tiene valor concatenamos la nueva sentencia con AND
					$where .= ' AND ';
				
				for($i=0; $i<count($idEstatusCliente); $i++) {
								
					if($i == 0) 
						$where .='(';
					else if($i > 0)
						$where .= ' OR ';
					
					$where .= 'hv.idEstatusCliente='.$idEstatusCliente[$i];
				}
				$where .= ') ';
			}
			
			
			if( !empty($fecha) && $fecha > 0) {
					
				if($where != '')  // Si el arreglo anterior tiene valor concatenamos la nueva sentencia con AND
					$where .= ' AND ';
				
				switch($fecha):
					case 1: 
						$where .='fecha=DATE(NOW())';
					break;
					case 2:
						$where .='fecha BETWEEN ADDDATE(NOW(), INTERVAL -7 DAY) AND NOW()';
					break;
					case 3:
						$where .='fecha BETWEEN ADDDATE(NOW(), INTERVAL -30 DAY) AND NOW()';
					break;
					case 4:
						$where .='fecha BETWEEN ADDDATE(fecha, INTERVAL -90 DAY) AND NOW()';
					break;
					case 5:
						$where .='fecha BETWEEN ADDDATE(fecha, INTERVAL -365 DAY) AND NOW()';
					break;
				endswitch;
			}
			
			if( !empty($prospecto) ) {
				
				if($where != '')  // Si el arreglo anterior tiene valor concatenamos la nueva sentencia con AND
					$where .= ' AND ';
				
				$where .='CONCAT(apaterno,\' \',amaterno,\' \',nombreCliente) LIKE \'%'.$prospecto.'%\'';
			}
			
			//var_dump($where);
			
			if($this->idRol == 1 || $this->idRol == 8 ) {
				$prospectos = $this -> prospecto_model -> getProspectos(null,$this->idRol,$where,false);
				$totalProspecto = $this -> prospecto_model -> getProspectos(null,$this->idRol,$where,true);
			}
			else if($this->idRol == 2 ) {
				$prospectos = $this -> prospecto_model -> getProspectos($this->idUsuario,$this->idRol,$where, false);
				$totalProspecto = $this -> prospecto_model -> getProspectos($this->idUsuario,$this->idRol,$where,true);
			}
			else if($this->idRol == 3 ) {
				$prospectos = $this -> prospecto_model -> getProspectos($this->idUsuario,null,$where, false);
				$totalProspecto = $this -> prospecto_model -> getProspectos($this->idUsuario,null,$where, true);
			}
			
			foreach($totalProspecto as $key)
				$total = $key->total;
			
			$strong = true;
			
			if($prospectos != '') {
				$data['html'] .= '<tr> <th colspan="5" align="right"> &nbsp; </th> <th> N. Registros </th> <th align="left"> '.$total.' </th> </tr>
									<tr>
					                <th>Frac.</th> <th>Asesor</th> <th>Cliente</th> <th>Tel&eacute;fono</th> <th>Celular</th> <th>Estatus</th> <th>Acciones</th>
					            </tr>';
								
				foreach($prospectos as $key):
					
					if($strong):
		                $class = 'strong';
		                $strong = false;
		            elseif(!$strong):
		                $class = 'light';
		                $strong = true;
		            endif;		    		    
					$data['html'].= '<tr id="'.$key->idHojaVida.'" class="'.$class.'">
						<td>'.$key->nombreFrac.'</td>
						<td>'.$key->nombre.'</td>
						<td>'.$key->cliente.'</td>
						<td>'.$key->ladaFijo." ".$key->telFijo.'</td>
						<td>'.$key->ladaCelular." ".$key->telCelular.'</td>
						<td>'.$key->clasificacion.'</td>
						
						<td align="right">
						    <a href="'.base_url().'admin/prospecto/editar/'.$key->idHojaVida.'">
						        <img src="'.base_url().'img/edit_row.png" alt="Editar" />
						    </a>
						    &nbsp;
						    <a href="'.base_url().'admin/prospecto/actividades/'.$key->idHojaVida.'">
		                        <img src="'.base_url().'img/task_row.png" alt="Actividades" />
		                    </a>
						    &nbsp;';
							
							if($this->idRol == 1 || $this->idRol == 8):
					  			$data['html'] .= '<a id="deleteRow'.$key->idHojaVida.'" href="'.$key->idHojaVida.'" class="deleteRow">
		                        		<img src="'.base_url().'img/delete_row.png" alt="Eliminar" />
		              				</a>';
							endif;
							
						$data['html'] .= '</td>
					</tr>';
				
				endforeach;
				
				$data['html'] .= '<tr> <th colspan="5" align="right"> &nbsp; </th> <th> N. Registros </th> <th align="left"> '.$total.' </th> </tr>';
			}
			else {
				$data['response'] = 'false';
			} 
			
			echo json_encode($data);
		}
	}

	
}