<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estatuscliente extends CI_Controller {

	var $idRol = '';
	var $idUsuario = '';
	var $myPath = "";
		
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 11)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');		
            $this -> load -> model('menu_model');
			$this -> load -> model ('tablaestatuscli_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
		
		$this->myPath = getMyPath($this->idRol);
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		
		switch ($this -> idRol):
			case '1':
				$data['estatuscliente'] = $this -> tablaestatuscli_model -> getEstatus(true);
			break;
			
			case '2':
				$data['estatuscliente'] = $this -> tablaestatuscli_model -> getEstatus(true);
			break;
			
			case '3':
				$data['estatuscliente'] = $this -> tablaestatuscli_model -> getEstatus(true);
			break;
			
			case '8':
				$data['estatuscliente'] = $this -> tablaestatuscli_model -> getEstatus(true);
			break;
		endswitch;
        $data['module'] = 'admin/' . $this->myPath . 'estatuscliente_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
        $this->form_validation->set_rules('clasificacion','Clasificaci&oacute;n','trim|required|xss_clean');
		$this->form_validation->set_rules('descripcion','Descripci&oacute;n','trim|required|xss_clean');
		                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $clasificacion = $this->input->post('clasificacion');
			$descripcion = $this->input->post('descripcion');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusCliente  = 1;
		            
            $arrData = array(
                'clasificacion' => $clasificacion,
                'descripcion' => $descripcion,
                'fechaRegistro' => $fechaRegistro,
                'statusCliente' => $statusCliente,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusCliente' => '1',
                'clasificacion' => $clasificacion
            );
            
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'estatuscliente', true, $arrDataWhere,NULL);         
            if($lastID > 0) {
            	
               	$data['response'] = 'true';
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                	<td>' . $clasificacion . '</td>
                	<td>' . $descripcion . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/estatuscliente/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';                                                                
            }
			else {
				$data['response'] = 'false';
			} 
            echo json_encode($data);            
        }       
    }

	function editar($idEstatusCliente = null){
        if($idEstatusCliente == null){
            redirect('admin/estatuscliente');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Estatus';
        //$data['pestana'] = 1;
        
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('estatuscliente', 'idEstatusCliente', $idEstatusCliente, null);
        
        if($data['info'] == null){            
            redirect('admin/estatuscliente');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_estatuscliente_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
		$this->form_validation->set_rules('idEstatusCliente','ID Estatus','trim|required|xss_clean');
        $this->form_validation->set_rules('clasificacion','Clasificaci&oacute;n','trim|required|xss_clean');
		$this->form_validation->set_rules('descripcion','Descripci&oacute;n','trim|required|xss_clean');
		                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/casas');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idEstatusCliente = $this->input->post('idEstatusCliente');
            $clasificacion = $this->input->post('clasificacion');
			$descripcion = $this->input->post('descripcion');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusCliente  = 1;
		            
            $arrData = array(
                'clasificacion' => $clasificacion,
                'descripcion' => $descripcion,
                'fechaRegistro' => $fechaRegistro,
                'statusCliente' => $statusCliente,
                'idAnterior' => $idEstatusCliente,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idEstatusCliente' => $idEstatusCliente
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusCliente' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'estatuscliente', $arrDataWhere, $idEstatusCliente, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/estatuscliente');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/estatuscliente');            
			}
		            
        }       
    }
	
	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idEstatusCliente = $this->input->post('idEstatusCliente');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusCliente' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idEstatusCliente' => $idEstatusCliente
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'estatuscliente', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}