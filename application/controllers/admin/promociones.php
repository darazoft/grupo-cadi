<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promociones extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 20)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
			$this -> load -> model ('tablapromociones_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index() {   	
		
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Promociones';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> idRol);
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFracCasa(true);
		$data['promociones'] = $this -> tablapromociones_model -> getPromociones(true);
        $data['module'] = 'admin/promociones_view';
        
        /* DYNAMIC css */
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
	        
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
    	    	
        //$this->form_validation->set_rules('fraccionamientos','Fraccionamientos','trim|required|xss_clean');
		//$this->form_validation->set_rules('chk','Casas','trim|required|xss_clean');
		$this->form_validation->set_rules('titulo','Titulo','trim|required|xss_clean');
		$this->form_validation->set_rules('promocion','Promocion','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaInicio','Fecha Inicio','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaFin','Fecha Fin','trim|required|xss_clean');
		                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
			$titulo = $this->input->post('titulo');
			$promocion = $this->input->post('promocion');
			$fechaInicio = $this->input->post('fechaInicio');
			$fechaFin = $this->input->post('fechaFin');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusPromo  = 1;
			
			//Recibimos arreglos
			$fraccionamientos = $this->input->post('fraccionamientos');
			$casas = $this->input->post('chk');
			$error = 0;
		    
						
			foreach($this->input->post('fraccionamientos') as $key => $idFrac) {
				
				foreach($casas as $keyCasa => $idFC) {
					
					$idFracc = explode("-", $idFC);
					$idF = $idFracc[0];
					$idCasa = $idFracc[1];
					
					if($idFrac == $idF) {
						
						$arrData = array(
			                'idFraccionamiento' => $idF,
			                'idCasa' => $idCasa,
			                'titulo' => $titulo,
			                'promocion' => $promocion,
			                'fechaInicio' => $fechaInicio,
			                'fechaFin' => $fechaFin,
			                'fechaRegistro' => $fechaRegistro,
			                'statusPromo' => $statusPromo,
			                'idUser' => $this -> idUsuario
			            );
						
						/***
						 * Array de condiciones para validar si existe
						 * algún registro similar
						 */
						$arrDataWhere = array(
			                'statusPromo' => '1',
			                'idFraccionamiento' => $idFrac,
			                'idCasa' => $idCasa
			            );
						
						$lastID = $this -> defaultdata_model -> addInfo($arrData, 'fraccionamientopromocion', true, $arrDataWhere, NULL);
						if($lastID > 0) {
							
							$nombreFrac = '';
							$numCasa = '';
							$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFrac);
							$numCasa = $this -> defaultdata_model -> getNameRow('numCasa','casa','idCasa',$idCasa);
							
							$data['html'] .= '<tr id="' . $lastID  . '" class="light optsPane">
			                	<td>' . $nombreFrac->nombreFrac . '</td>
			                	<td>' . $numCasa->numCasa . '</td>
			                    <td>' . $titulo . '</td>
			                    <td>' . $promocion . '</td>
			                    <td>' . $fechaInicio . '</td>
			                    <td>' . $fechaFin . '</td>
			                    <td>' . $fechaRegistro . '</td>
			                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
			                        <span>
			                            Activo
			                        </span>
			                        
			                        <div id="optsPane'.$lastID.'">
			                            <a id="editRow'.$lastID.'" href="'.base_url().'admin/promociones/editar/'.$lastID.'">
			                                <img src="'.base_url().'/img/edit_row.png" />
			                            </a>
			                            
			                            <a id="deleteRow'.$lastID.'" href="'.$lastID.'" class="deleteRow">                            
			                                <img src="'.base_url().'/img/delete_row.png" />
			                            </a>
			                        </div>                     
			                    </td>                 
			                </tr>';
							
						} // SI INSERTA EL REGISTRO
						else {
							$error++;										
						} 
									
					} //Cierra if que compara para insertar
				} //Cierra foreach casas
			} //Cierra foreach fraccionamientos
			
            
			if($error == 0) {
				$data['response'] = 'true';
			}
			else {
				$data['response'] = 'false';	
			}
						
            echo json_encode($data);            
        }       
    }

	function editar($idFraccionamientoPromocion = null){
        if($idFraccionamientoPromocion == null){
            redirect('admin/promociones');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Promoci&oacute;n';
        		
		$arrDataJoin = array(
			array('fraccionamiento as f','f.idFraccionamiento = fp.idFraccionamiento'),
			array('casa as c','c.idCasa = fp.idCasa')
		);
		
		$arrDataWhere = array(
			'idFraccionamientoPromocion'=> $idFraccionamientoPromocion
		);
		
		$campos = "fp.*, f.nombreFrac, c.numCasa, c.interior";
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfoJoin('fraccionamientopromocion as fp', $campos, $arrDataWhere, $arrDataJoin,false);
		        
        if($data['info'] == null){            
            redirect('admin/promociones');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);        
        $data['module'] = 'admin/editar_promocion_view';
		
		/*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
	        
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                        
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
         
		$this->form_validation->set_rules('idFraccionamientoPromocion','Fraccionamiento Promocion','trim|required|xss_clean');
		$this->form_validation->set_rules('titulo','Titulo','trim|required|xss_clean');
		$this->form_validation->set_rules('promocion','Promocion','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaInicio','Fecha Inicio','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaFin','Fecha Fin','trim|required|xss_clean');
		                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
		           
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/promociones');            
            
        } else{
                        
            //Preparamos arreglo para ir a insertar
            $idFraccionamientoPromocion = $this->input->post('idFraccionamientoPromocion');
			$idFraccionamiento = $this->input->post('idFraccionamiento');
			$idCasa = $this->input->post('idCasa');
            $titulo = $this->input->post('titulo');
			$promocion = $this->input->post('promocion');
			$fechaInicio = $this->input->post('fechaInicio');
			$fechaFin = $this->input->post('fechaFin');
			$statusPromo = 1;
            $fechaRegistro  = date('Y-m-d H:i:s');
					            
            $arrData = array(
            	'idFraccionamiento' => $idFraccionamiento,
            	'idCasa' => $idCasa,
                'titulo' => $titulo,
                'promocion' => $promocion,
                'fechaInicio' => $fechaInicio,
                'fechaFin' => $fechaFin,
                'fechaRegistro' => $fechaRegistro,
                'statusPromo' => $statusPromo,
                'idAnterior' => $idFraccionamientoPromocion,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFraccionamientoPromocion' => $idFraccionamientoPromocion
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusPromo' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'fraccionamientopromocion', $arrDataWhere, $idFraccionamientoPromocion, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/promociones');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/promociones');            
			}
                        
       	}       
    }
	
	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idFraccionamientoPromocion = $this->input->post('idFraccionamientoPromocion');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusPromo' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idFraccionamientoPromocion' => $idFraccionamientoPromocion
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'fraccionamientopromocion', $arrDataWhere) > 0){
			
			//Actualizar casa, para que quede libre de promocion
			$key = $this -> defaultdata_model -> getInfo('fraccionamientopromocion','idFraccionamientoPromocion',$idFraccionamientoPromocion);
			
			$idFraccionamiento = $key->idFraccionamiento;
			$idCasa = $key->idCasa;
			
			$arrData = array(
				'promocion' => 2
			);
			
			$arrDataWhere = array(
				'idFraccionamiento' => $idFraccionamiento,
				'idCasa' => $idCasa
			);
			
			if($this -> defaultdata_model -> deleteInfo($arrData, 'casa', $arrDataWhere) > 0) {
			
            	$data['response'] = 'true';
			}
			else {
				$data['response'] = 'false';
			}                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}