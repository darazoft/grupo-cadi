<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prototipos extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 9)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
            $this -> load -> model ('tabla_prototipo_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		
		switch ($this->idRol):
			case '1':
        		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
			break;
			
			case '8':
        		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
			break;
		endswitch;
        $data['module'] = 'admin/prototipo_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
        $this->form_validation->set_rules('nombrePrototipo','Nombre Pototipo','trim|required|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripcion','trim|required|xss_clean');
        		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $nombrePrototipo = $this->input->post('nombrePrototipo');
            $descripcion = $this->input->post('descripcion');
            
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusPrototipo  = 1;
		            
            $arrData = array(
                'nombrePrototipo' => $nombrePrototipo,
                'descripcion' => $descripcion,
                'fechaRegistro' => $fechaRegistro,
                'statusPrototipo' => $statusPrototipo,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusPrototipo' => '1'                
            );
			
			$arrDataLike = array(
                'nombrePrototipo' => $nombrePrototipo
            );
               
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'prototipo', true,$arrDataWhere,$arrDataLike);         
			
            if($lastID > 0){
                $data['response'] = 'true';
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                    <td>' . $nombrePrototipo. '</td>
                    <td>' . $descripcion. '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/prototipos/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
				$data['response'] = 'true';                                                                
            }
			else {
				$data['response'] = 'false';
			}
            echo json_encode($data);            
        }       
    }

	function editar($idPrototipo = null){
        if($idPrototipo == null){
            redirect('admin/prototipos');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Prototipos';
        //$data['pestana'] = 1;
        
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('prototipo', 'idPrototipo', $idPrototipo, null);
        
        if($data['info'] == null){            
            redirect('admin/prototipos');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_prototipo_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
		$this->form_validation->set_rules('idPrototipo','Pototipo','trim|required|xss_clean');
        $this->form_validation->set_rules('nombrePrototipo','Nombre Pototipo','trim|required|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripcion','trim|required|xss_clean');
        		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/prototipos');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idPrototipo = $this->input->post('idPrototipo');
            $nombrePrototipo = $this->input->post('nombrePrototipo');
            $descripcion = $this->input->post('descripcion');
            
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusPrototipo  = 1;
		            
            $arrData = array(
                'nombrePrototipo' => $nombrePrototipo,
                'descripcion' => $descripcion,
                'fechaRegistro' => $fechaRegistro,
                'statusPrototipo' => $statusPrototipo,
                'idAnterior' => $idPrototipo,
                'idUser' => $this -> idUsuario
            );
			
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idPrototipo' => $idPrototipo
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusPrototipo' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'prototipo', $arrDataWhere, $idPrototipo, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/prototipos');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/prototipos');            
			}
			            
        }       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idPrototipo = $this->input->post('idPrototipo');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusPrototipo' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idPrototipo' => $idPrototipo
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'prototipo', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}