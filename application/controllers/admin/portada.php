<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");
	
class Portada extends CI_Controller{
		
	function __construct(){
		parent::__construct();
		if (is_login_with_session($this) || is_login_with_cookies($this)) {
            $tipoUsuario = $this->session->userdata('tipoUsuario');
            if (!($tipoUsuario == '0' || $tipoUsuario == '1')) {
                $this->session->set_flashdata('error', 'error_1');
                redirect('');
            }
        } else {
            redirect('sesion/logout/admin/');
        }
		$this->load->model('portada_model');//el modelo que administra a BD
		$this->load->helper(array('form', 'url'));
        $this->load->library('upload');
		$this->load->library('image_lib');        
	}
	
	function index() {
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        $data['pestana'] = 2;
		
		//$data es la variable que le envia todo la info a la vista
		$data['portada'] = $this->portada_model->getPortada();
		$data['module'] = 'admin/portada_view';		
        $this->load->view('admin/main_view', $data);
    }
	
	function do_upload($input, $carpeta, $width, $height) {
        if (!empty($_FILES[$input]['name'])) {
            $config['upload_path'] = $carpeta;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['max_width'] = $width;
            $config['max_height'] = $height;
			$config['overwrite'] = true;
			
            // $config['file_name'] = substr(md5(uniqid(rand())), 0, 10);
            $config['file_name'] = 'portada';            
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input)) {
                return null;
            } else {
                $data = $this->upload->data();				
                return  $data['file_name'];
            }
        }
    }
	
	function addPhoto() {
		$photo = $this->do_upload('userfile', 'imgs/portada', 2048, 2048);
		if ($photo != null) {
			$arrUpdate = array(
				'imgPortada' => $photo
            );
            $this->portada_model->switchFront($arrUpdate);
            $this->session->set_flashdata('error', 'error_6');
            redirect('admin/portada');
        } else {
            $this->session->set_flashdata('error', 'error_7');
            redirect('admin/portada');
        }
	}			
}
?>