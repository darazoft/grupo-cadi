<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agenda extends CI_Controller {
	
	var $myPath = "";
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)) {
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 26)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
            $this -> load -> model('defaultdata_model');
			$this -> load -> model('agenda_model');
			$this -> load -> model('prospecto_model');         
			$this -> load -> model ('tabla_fraccionamiento_model');
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
		
		$this->myPath = getMyPath($this->idRol);
    }
    
    function index($tipo = null) {
    	
		if($tipo == null) {
			redirect('admin/prospecto');
		}   	
		     
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> idRol);
		$hoy = date('Y-m-d');
						
		switch($tipo):
			case 1:
				if($this->idRol == 2) { 
					$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1','DATE(fechaInicio) =',$hoy, false,null);
					$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1','DATE(fechaInicio) =',$hoy, false,null);
				}
				else if($this->idRol == 3) {
					$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1','DATE(fechaInicio) =',$hoy, false,null);
					$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1','DATE(fechaInicio) =',$hoy, false,null);
				}
				
				$arrComments = null;
		        if($actividades_comment != null) {
		            foreach($actividades_comment as $row){
		                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
		            }
		        }
		        
		        $data['arrComments'] = $arrComments;
				$comentarios_count = $actividades_comment;
				
				$arrCount = array();
				if($comentarios_count != null) {
					foreach($comentarios_count as $row){
		                $arrCount[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosCount($row->idHojaActividad); 
		            }
				}
				
				$data['arrCount'] = $arrCount;
				
				$data['module'] = 'admin/' . $this->myPath . 'dashboard_view';
			break;
			
			case 2:
				$pendientes = 'fechaInicio <= DATE(NOW())'; 
				if($this->idRol == 2) {
					$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1',null,null, false,$pendientes);
					$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1',null,null, false, $pendientes);
				}
				else if($this->idRol == 3) {
					$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1',null,null, false, $pendientes);
					$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1',null,null, false, $pendientes);
				}
				
				$arrComments = null; 
		        if($actividades_comment != null) {
		            foreach($actividades_comment as $row){
		                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
		            }
		        }
		        
		        $data['arrComments'] = $arrComments;
				$comentarios_count = $actividades_comment;
				
				$arrCount = array();
				if($comentarios_count != null) {
					foreach($comentarios_count as $row){
		                $arrCount[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosCount($row->idHojaActividad); 
		            }
				}
				
				$data['arrCount'] = $arrCount;
				
				$data['module'] = 'admin/' . $this->myPath . 'dashboard_view';
			break;
			
			case 3:
				$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getMisFraccionamientos($this->idUsuario);
				$data['module'] = 'admin/' . $this->myPath . 'todasactividades_view'; 
			break;
			
		endswitch;
		        
        $this->load->view('admin/main_view', $data);        
    }

	
	function consulta_do () {
		
		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaInicio','Fecha Inicio','trim|required|xss_clean');
		$this->form_validation->set_rules('fechaFin','Fecha Fin ','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
		           
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);            
        } else{
                        
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$fechaInicio = $this->input->post('fechaInicio');
			$fechaFin = $this->input->post('fechaFin');
			
			//Arreglo para parsear por json
	        $data = array();            
	        $data['response'] = 'true';
	        $data['html']     = '';
						
			$between = 'DATE(fechaInicio) BETWEEN "'.$fechaInicio.'" and "'.$fechaFin.'"';
			
			switch($this->idRol) :
				case 2:
					$actividades = $data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad != ','2','hv.idFraccionamiento',$idFraccionamiento,$between,null);
				break;
				case 3:
					$actividades = $data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad != ','2','hv.idFraccionamiento',$idFraccionamiento,$between,null);
				break;
			endswitch;
			
			$actividades_comment = $actividades;
	        $arrComments = array();
	        
	        if($actividades_comment != null) {
	            foreach($actividades_comment as $row){
	                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
	            }
	        }
			
			$comentarios_count = $actividades_comment;
				
			$arrCount = array();
			if($comentarios_count != null) {
				foreach($comentarios_count as $row){
	                $arrCount[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosCount($row->idHojaActividad); 
	            }
			}
				
			if($actividades != '') {
				
				$data['html'] .= '<tr>
		                <th colspan="3">Fecha</th> <th>Fraccionamiento</th> <th>Cliente</th> <th>Actividad</th><th>Tel&eacute;fono</th> <th>Celular</th> <th>Fecha de Alta de Prospecto</th> <th>D&iacute;as en Seguimiento</th>
		            </tr>';	
				$strong = true;
            	$class = '';
				foreach ($actividades as $key):
					
		            if($strong):
		                $class = 'strong';
		                $strong = false;
		            elseif(!$strong):
		                $class = 'light';
		                $strong = true;
		            endif;
								
					$data['html'] .= '<tr class="'.$class.'">';
					
						$data['html'] .= '<td>';
						if($key->statusActividad == 3) :
							$data['html'] .= '<span> <img src="'.base_url().'img/green.png" alt="Estado" title="Realizada" /> </span>
	                        	&nbsp;';
						elseif($key->estado == 'yellow') :
	                		$data['html'] .= '<a href="'.base_url().'admin/prospecto/editar_estado_actividad/'.$key->idHojaActividad.'/'.$key->idHojaVida.'" title="Pendiente">
								<img src="'.base_url().'img/yellow.png" alt="Estado" /> </span>
								</a>
								&nbsp;';
						elseif($key->estado == 'red') :
							$data['html'] .= '<a href="'.base_url().'admin/prospecto/editar_estado_actividad/'.$key->idHojaActividad.'/'.$key->idHojaVida.'" title="Vencida">
                            	<img src="'.base_url().'img/red.png" alt="Estado" />                            
                        		</a>
								&nbsp;';
						endif;
						$data['html'] .= '</td>';
						
						$data['html'] .= '<td>';
										if($arrCount[$key->idHojaActividad] != null):
											foreach($arrCount[$key->idHojaActividad] as $row):
												if($row->total > 0):
					                
								                	$data['html'] .= '<div align="left" style="font-family: \'Trebuchet MS\'; color: red; font-size:13px; width:32px; height:32px; background:url('.base_url().'img/messages_counter.png) left top no-repeat; padding-left:4px; padding-top:3px;">
												        '.$row->total.'
												    </div>';
												endif;
											endforeach;
					                	endif;
					                
						$data['html'] .= '</td>';
												
	                	$data['html'] .= '<td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;">' . getFormatDate($key -> fechaInicio, true). '</td>
	                	<td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;"> '.$key->nombreFrac . '</td>
	                    <td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;"> ' . $key->cliente . '</td>';
						
	                    $data['html'] .= '<td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;">';
						
							 switch ($key->nombreActividad) :
								case 1:	$data['html'] .= "LLamada";	break;
								case 2:	$data['html'] .= "Cita"; break;
								case 3:	$data['html'] .= "Email"; break;
								case 4:	$data['html'] .= "Firma de contrato"; break;
								case 5:	$data['html'] .= "Pago de apartado"; break;
								case 6:	$data['html'] .= "Cancelaci&oacute;n"; break;
								case 7:	$data['html'] .= "Seguimiento postventa"; break;
								case 8:	$data['html'] .= "Atenci&oacute;n a queja";	break;
								case 9:	$data['html'] .= "seguimeinto cobranza"; break;
								case 10: $data['html'] .= "Entrega de casa"; break;
								case 11: $data['html'] .= "Firma de escritura";	break;
								case 12: $data['html'] .= "Otro"; break;
							endswitch; 
						$data['html'] .= '</td>
						
	                    <td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;">'.$key->ladaFijo.' '.$key->telFijo. '</td>
	                    <td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;">'.$key->ladaCelular.' '.$key->telCelular. '</td>
	                    <td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer;">'.getFormatDate($key->fechaRegistro,true). '</td>
						<td class="showMessages" headers="'.$key->idHojaActividad.'" style="cursor: pointer; color: red; font-weight:bold;">'.$key->dias. '</td>';
						
	                $data['html'] .= '</tr>';
					
					$data['html'] .= '<tr id="allContainer_'.$key->idHojaActividad.'" class="'.$class.' allContainer" style="display: none;">
					            <td colspan="6">
					                <table id="boxComments_'.$key->idHojaActividad.'" style="width: 750px;">';
					                    	
					                    	if($arrComments[$key->idHojaActividad] != null):
					                           foreach($arrComments[$key->idHojaActividad] as $row):
					                               $data['html'] .= '<tr>
					                               	   <td colspan="2">'.$row->comentario.'</td>
					                                   <td colspan="2">'.$row->nombre.'</td>
					                                   <td colspan="2">'.$row->fechaRegistro.'</td>
					                               </tr>';                              
					                           endforeach;
					                       	endif;
											                                            
					           $data['html'] .= '</table>
					                <table id="boxaddComments_'.$key->idHojaActividad.'" style="width: 750px;">
					                    <tr id="boxForm_'.$key->idHojaActividad.'">
					                        <td>&nbsp;</td>
					                        <td colspan="4">
					                            <form id="formMessage'.$key->idHojaActividad.'" data-rel="'.$key->idHojaActividad.'" class="niceform addMessageForm">
					                            Comentario: <textarea name="nuevoComment'.$key->idHojaActividad.'" id="nuevoComment'.$key->idHojaActividad.'" style="width: 735px !important; height: 50px !important" class="validate[required] input-text"></textarea>
					                                <br />
					                                <br />
					                                <input type="submit" value="Agregar comentario" id="submit_'.$key->idHojaActividad.'" />
					                                <br />
					                                &nbsp;                            
					                            </form>                                    
					                        </td>
					                    </tr>                            
					                </table>                        
					            </td>      
					        </tr>';
					
				endforeach;
				
				$data['response'] = 'true';
			}
			else {
				$data['response'] = 'false';
			}

			echo json_encode($data); 
		}
	}
	    
}