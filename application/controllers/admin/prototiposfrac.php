<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PrototiposFrac extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 10)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tablaprototiposfrac_model');
			$this -> load -> model ('tabla_prototipo_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
		
		switch ($this -> idRol):
			case '1':
				$data['prototiposfrac'] = $this -> tablaprototiposfrac_model -> getPrototiposFrac(true);
			break;
			
			case '8':
				$data['prototiposfrac'] = $this -> tablaprototiposfrac_model -> getPrototiposFrac(true);
			break;
			
		endswitch;
        $data['module'] = 'admin/prototiposfrac_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
	}
    
    function nuevo_do(){
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idPrototipo','Prototipo','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idPrototipo = $this->input->post('idPrototipo');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusFP  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'idPrototipo' => $idPrototipo,
                'fechaRegistro' => $fechaRegistro,
                'statusFP' => $statusFP,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusFP' => '1',
                'idFraccionamiento' => $idFraccionamiento,
                'idPrototipo' => $idPrototipo
            );
            
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'fraccionamientoprototipo', true, $arrDataWhere, NULL);
						         
            if($lastID > 0){
            	$nombreFrac = '';
				$nombrePrototipo = '';
				$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFraccionamiento);
				$nombrePrototipo = $this -> defaultdata_model -> getNameRow('nombrePrototipo','prototipo','idPrototipo',$idPrototipo);
			
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                	<td>' . $nombreFrac->nombreFrac . '</td>
                    <td>' . $nombrePrototipo->nombrePrototipo . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/prototiposfrac/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
				$data['response'] = 'true';                                                                
            }
			else {
				$data['response'] = 'false';
			}
            echo json_encode($data);            
        }       
    }

	function editar($idFraccionamientoPrototipo = null){
        if($idFraccionamientoPrototipo == null){
            redirect('admin/prototiposfrac');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar prototipos asignados';
        //$data['pestana'] = 1;
        
        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('fraccionamientoprototipo', 'idFraccionamientoPrototipo', $idFraccionamientoPrototipo, null);
        
        if($data['info'] == null){            
            redirect('admin/prototiposfrac');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_prototiposfrac_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
		$this->form_validation->set_rules('idFraccionamientoPrototipo','ID Fraccionamiento Prototipo','trim|required|xss_clean');
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idPrototipo','Prototipo','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/prototiposfrac');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamientoPrototipo = $this->input->post('idFraccionamientoPrototipo');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idPrototipo = $this->input->post('idPrototipo');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusFP  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'idPrototipo' => $idPrototipo,
                'fechaRegistro' => $fechaRegistro,
                'statusFP' => $statusFP,
                'idAnterior' => $idFraccionamientoPrototipo,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFraccionamientoPrototipo' => $idFraccionamientoPrototipo
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusFP' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'fraccionamientoprototipo', $arrDataWhere, $idFraccionamientoPrototipo, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/prototiposfrac');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/prototiposfrac');            
			}
			
		}       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idFraccionamientoPrototipo = $this->input->post('idFraccionamientoPrototipo');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusFP' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idFraccionamientoPrototipo' => $idFraccionamientoPrototipo
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'fraccionamientoprototipo', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);	
	}
	
}