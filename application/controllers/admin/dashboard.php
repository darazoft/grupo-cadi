<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    var $myPath = "";
    var $idRol = "";
	var $idUsuario = "";
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){						
            $this -> load -> model('menu_model');
            $this -> load -> model('defaultdata_model');                     
			$this -> load -> model('agenda_model');
			$this -> load -> model('prospecto_model');
		}else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
        $this->idRol = $this -> session -> userdata('idRol');
		$this->idUsuario = $this -> session -> userdata('idUsuario');
        $this->myPath = getMyPath($this->idRol);
    }
	
	// Si no esta Sign In entonces muestro pantalla principal
    function index() {
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        $data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                 
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		
		switch($this->idRol):
			case 2:
				$pendientes = 'fechaInicio <= DATE(NOW())';
				$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1',null,null, false, $pendientes);
				$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,$this->idRol,'statusActividad','1',null,null, false, $pendientes);
				$data['arrComments'] = null;
				$data['arrCount'] = null;
				
				$arrComments = array();
		        if($actividades_comment != null) {
		            foreach($actividades_comment as $row){
		                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
		            }
					
					$data['arrComments'] = $arrComments;
		        }
		        
				$comentarios_count = $actividades_comment;
				
				$arrCount = array();
				if($comentarios_count != null) {
					foreach($comentarios_count as $row){
		                $arrCount[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosCount($row->idHojaActividad); 
		            }
					
					$data['arrCount'] = $arrCount;
				}
				
			break;
			
			case 3:
				$pendientes = 'fechaInicio <= DATE(NOW())';
				$data['actividades'] = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1',null,null, false, $pendientes);
				$actividades_comment = $this -> agenda_model -> getPendientes($this->idUsuario,null,'statusActividad','1',null,null, false, $pendientes);
				$data['arrComments'] = null;
				$data['arrCount'] = null;
				
				$arrComments = array();
		        if($actividades_comment != null) {
		            foreach($actividades_comment as $row){
		                $arrComments[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosActividad($row->idHojaActividad); 
		            }
		        }
		        
		        $data['arrComments'] = $arrComments;
				
				$comentarios_count = $actividades_comment;
				
				$arrCount = array();
				if($comentarios_count != null) {
					foreach($comentarios_count as $row){
		                $arrCount[$row->idHojaActividad] = $this -> prospecto_model -> getComentariosCount($row->idHojaActividad); 
		            }
				}
				
				$data['arrCount'] = $arrCount;
			break;
				
		endswitch;
		
        $data['module'] = 'admin/' . $this->myPath . 'dashboard_view';
                
        $this->load->view('admin/main_view', $data);
    }
    
    function cuenta(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        $data['pestana'] = 1;
        
        
        
        /*********  DYNAMIC DATA  *********/                 
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
        $data['user_info'] = $this -> defaultdata_model -> getInfo('usuario', 'idUsuario', $this -> session -> userdata('idUsuario'));
        $data['module'] = 'admin/micuenta_view';
                
        $this->load->view('admin/main_view', $data);        
    }
	
	function modificarcontrasena_do() {
        $this->form_validation->set_rules('contrasenaUsuarioActual', 'Contraseña actual', 'trim|required|xss_clean|md5');
        $this->form_validation->set_rules('contrasenaUsuario', 'Nueva contraseña', 'trim|required|xss_clean|md5');
        $this->form_validation->set_message('required', 'El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');

        // Ejecuto la validacion de campos de lado del servidor
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'error');
            redirect('admin/dashboard/cuenta');
            return false;
        } else {
            $contrasenaActual = $this->input->post('contrasenaUsuarioActual');
            $contrasenaUsuario = $this->input->post('contrasenaUsuario');
            $this->load->model('usuario_model');
            if ($this->usuario_model->cambiarContrasena($contrasenaActual, $contrasenaUsuario, $this->session->userdata('idUsuario'))) {
                $this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');
            } else {
                $this->session->set_flashdata('error', 'error_4');
                $this->session->set_flashdata('notyType', 'warning');
            }
            redirect('admin/dashboard/cuenta');
        }
    }

	function getActividadesPendientes() {
		
		$data['SYS_metaTitle'] = 'CADI';
		
		switch($this->idRol):
			case 3:
				$vencidas = 'DATE(fechaInicio) < DATE(NOW())';
				$data['actividades'] = $this -> agenda_model -> getTotalPendiente($this->idUsuario,null,'statusActividad','1',$vencidas, false);
			break;
				
		endswitch;
				 
        $this->load->view('admin/' . $this->myPath . 'actividadPendiente_view',$data);
	}
}