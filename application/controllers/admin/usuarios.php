<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    
    var $idRol = "";
    var $idUsuario = "";
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 1)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }			
			$this -> idRol = $this -> session -> userdata('idRol');
            $this -> idUsuario = $this -> session -> userdata('idUsuario');
            $this -> load -> model('menu_model');
            $this -> load -> model ('tabla_usuario_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
    	
		// $idRol = $this -> session -> userdata('idRol');
		// $idUsuario = $this -> session -> userdata('idUsuario');
		
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
		
		switch ($this->idRol):
			case '1':
        		$data['usuarios'] = $this -> tabla_usuario_model -> getUsuarios(true,false,$this->idUsuario);
			break;
			
			case '8':
        		$data['usuarios'] = $this -> tabla_usuario_model -> getUsuarios(true,false,$this->idUsuario);
			break;
		endswitch;
		
		// $año = date('Y');
		// $dia = date('d');
		// $mes = date('m');
		// $data['fechaActual'] = $año."-".$mes."-".$dia;
		
        $data['module'] = 'admin/usuario_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
	        
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
		        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
    	
        $this->form_validation->set_rules('nombre','Nombre','trim|required|xss_clean');
        $this->form_validation->set_rules('email','Email','trim|required|xss_clean');
        $this->form_validation->set_rules('puesto','Puesto','trim|required|xss_clean');
        $this->form_validation->set_rules('fechaIngreso','Fecha de ingreso','trim|required|xss_clean');
        $this->form_validation->set_rules('rol','Rol','trim|required|xss_clean');
		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'false';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $nivel = 0; 
            $nombreUsuario = $this->input->post('nombre');
            $emailUusuario = $this->input->post('email');
            $puesto        = $this->input->post('puesto');
            $fechaIngreso  = $this->input->post('fechaIngreso');
            $idRol         = $this->input->post('rol');
            if($idRol >= 1 && $idRol <= 6):
                $nivel = 1;
            elseif($idRol == 7):
                $nivel = 2;
            else:
                $nivel = 3;
            endif;
            
            $arrData = array(
                'idRol' => $idRol,
                'tipoUsuario' => $nivel,
                'nombre' => $nombreUsuario,
                'puesto' => $puesto,
                'fechaIngreso' => $fechaIngreso,
                'estatus' => '1',
                'emailUsuario' => $emailUusuario,
                'contrasenaUsuario' => '25d55ad283aa400af464c76d713c07ad',
                'hashUsuario'  => '',
                'fechaRegistro' => date('Y-m-d H:i:s'),
                'idUser' => $this -> session -> userdata('idUsuario')
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'estatus' => '1',
                'emailUsuario' => $emailUusuario
            );
			
			$arrDataLike = array(
                'nombre' => $nombreUsuario
            );
            
            $nombreRol = '';            
            switch($idRol):
                case 1:
                    $nombreRol = "Administrador";
                    break;
                case 2:
                    $nombreRol = "Coordinador";
                    break;
                case 3:
                    $nombreRol = "Asesores";
                    break;
                case 4:
                    $nombreRol = "Hostess";
                    break;
                case 5:
                    $nombreRol = "Coordinador de obra";
                    break;
                case 6:
                    $nombreRol = "Titulaci&oacute;n";
                    break;
                case 7:
                    $nombreRol = "Broker";
                    break;
                default:
                    $nombreRol = "Desconocido";
                    break;
            endswitch;
            
            $lastID = $this -> defaultdata_model -> addInfo($arrData, 'usuario', true,$arrDataWhere,$arrDataLike);
			
			if($lastID > 0) { 
                $data['response'] = 'true';
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                    <td>' . $nombreUsuario . '</td>
                    <td>' . $emailUusuario . '</td>
                    <td>' . $puesto . '</td>
                    <td>' . $fechaIngreso . '</td>
                    <td>' . $nombreRol . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');" >
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/usuarios/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
                
            	$data['response'] = 'true';                                                                
            }
			else {
				$data['response'] = 'false';	
			} 
            echo json_encode($data);            
        }       
    }

    function editar($idUsuario = null){
        if($idUsuario == null){
            redirect('admin/usuarios');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Usuarios';
        //$data['pestana'] = 1;
        
        /*USER INFO*/        
        $data['user_info'] = $this -> defaultdata_model -> getInfo('usuario', 'idUsuario', $idUsuario, null);
        
        if($data['user_info'] == null){            
            redirect('admin/usuarios');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_usuario_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }
    
	
    function editar_do(){
         
		$this->form_validation->set_rules('idUsuario','Id Usuario','trim|required|xss_clean');
		$this->form_validation->set_rules('nombre','Nombre','trim|required|xss_clean');
        $this->form_validation->set_rules('email','Email','trim|required|xss_clean');
        $this->form_validation->set_rules('puesto','Puesto','trim|required|xss_clean');
        $this->form_validation->set_rules('fechaIngreso','Fecha de ingreso','trim|required|xss_clean');
        $this->form_validation->set_rules('rol','Rol','trim|required|xss_clean');
		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/usuarios');            
            
        } else{
                        
            //Preparamos arreglo para ir a insertar
            $nivel = 0;
			$idUsuario = $this->input->post('idUsuario'); 
            $nombreUsuario = $this->input->post('nombre');
            $emailUusuario = $this->input->post('email');
            $puesto        = $this->input->post('puesto');
            $fechaIngreso  = $this->input->post('fechaIngreso');
            $idRol         = $this->input->post('rol');
            if($idRol >= 1 && $idRol <= 6):
                $nivel = 1;
            elseif($idRol == 7):
                $nivel = 2;
            else:
                $nivel = 3;
            endif;
            
			/** 
			 * Array con datos del insert
			 */
            $arrData = array(
                'idRol' => $idRol,
                'tipoUsuario' => $nivel,
                'nombre' => $nombreUsuario,
                'puesto' => $puesto,
                'fechaIngreso' => $fechaIngreso,
                'estatus' => '1',
                'emailUsuario' => $emailUusuario,
                'contrasenaUsuario' => '25d55ad283aa400af464c76d713c07ad',
                'hashUsuario'  => '',
                'fechaRegistro' => date('Y-m-d H:i:s'),
                'idAnterior' => $idUsuario,
                'idUser' => $this -> session -> userdata('idUsuario')
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idUsuario' => $idUsuario
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'estatus' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'usuario', $arrDataWhere, $idUsuario, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/usuarios');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/usuarios');            
			}
                        
       	}       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idUsuario = $this->input->post('idUsuario');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'estatus' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idUsuario' => $idUsuario
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'usuario', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
	}
	
}