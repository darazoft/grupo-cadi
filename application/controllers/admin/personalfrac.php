<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PersonalFrac extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 17)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tablapersonalfrac_model');
			$this -> load -> model ('tabla_usuario_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        $idUsuario = $this -> idUsuario;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['usuarios'] = $this -> tabla_usuario_model -> getUsuarios(true,false,$idUsuario);
		
		switch ($this -> idRol):
			case '1':
				$data['personalfrac'] = $this -> tablapersonalfrac_model -> getPersonalFrac(true);
			break;
			
			case '8':
				$data['personalfrac'] = $this -> tablapersonalfrac_model -> getPersonalFrac(true);
			break;
			
		endswitch;
		
        $data['module'] = 'admin/personalfrac_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idUsuario = $this->input->post('idUsuario');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusUsuario  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'idUsuario' => $idUsuario,
                'fechaRegistro' => $fechaRegistro,
                'statusUsuario' => $statusUsuario,
                'idUser' => $this -> idUsuario
          	);
			
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'fraccionamientousuario', true,NULL,NULL);                     
            if($lastID > 0){
            	$nombreFrac = '';
				$nombre = '';
            	$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFraccionamiento);
				$nombre = $this -> defaultdata_model -> getNameRow('nombre','usuario','idUsuario',$idUsuario);
				
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                	<td>' . $nombreFrac->nombreFrac . '</td>
                    <td>' . $nombre->nombre . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/personalfrac/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
				$data['response'] = 'true';                                                                
            }
            echo json_encode($data);            
        }       
    }

	function editar($idFracUsuario = null){
        if($idFracUsuario == null){
            redirect('admin/personalfrac');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar personal asignado';
        //$data['pestana'] = 1;
        
        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['usuarios'] = $this -> tabla_usuario_model -> getUsuarios(true,false,$this->idUsuario);
		
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('fraccionamientousuario', 'idFracUsuario', $idFracUsuario, null);
        
        if($data['info'] == null){            
            redirect('admin/personalfrac');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_personalfrac_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
		$this->form_validation->set_rules('idFracUsuario','Fraccionamiento Usuario','trim|required|xss_clean');
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idUsuario','Usuario','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/personalfrac');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFracUsuario = $this->input->post('idFracUsuario');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idUsuario = $this->input->post('idUsuario');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusUsuario  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'idUsuario' => $idUsuario,
                'fechaRegistro' => $fechaRegistro,
                'statusUsuario' => $statusUsuario,
                'idAnterior' => $idFracUsuario,
                'idUser' => $this -> idUsuario
          	);
			
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFracUsuario' => $idFracUsuario
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusUsuario' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'fraccionamientousuario', $arrDataWhere, $idFracUsuario, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/personalfrac');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/personalfrac');            
			}
			            
        }       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idFracUsuario = $this->input->post('idFracUsuario');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusUsuario' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idFracUsuario' => $idFracUsuario
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'fraccionamientousuario', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
	}
	
}