<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
			//Esta loggeado
			redirect('admin/dashboard');
		}else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
		}
    }
	
	// Si no esta Sign In entonces muestro pantalla principal
	function index() {	
		/* Si necesito alguna libreria dinamica la cargo aqui */
		//$data['dinamicLibrary']['autoComplete'] = true;
		/* Informacion para configurar pagina */
		$data['SYS_metaTitle'] 			= 'Administracion | Sign In';
		$data['SYS_metaDescription'] 	= 'Administracion | Sign In al DashBoard';
		$data['pestana'] 				= 0;
		$data['module'] 				= 'admin/principal_view';
		$this->load->view('admin/login_view', $data);		
	}
	
}