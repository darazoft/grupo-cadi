<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casasdisponibles extends CI_Controller {
	
	var $myPath = "";
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)) {
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 5)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
			$this -> load -> model ('tablacasas_model');
			$this -> load -> model ('tablapromociones_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
		
		$this->myPath = getMyPath($this->idRol);
    }
    
    function index() {   	
		
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> idRol);
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
						
        $data['module'] = 'admin/' . $this->myPath . 'casasdisponibles_view';
                
        $this->load->view('admin/main_view', $data);        
    }
	
	function consulta_do () {
		
		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('statusVenta','Estatus','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
		           
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);            
        } else{
                        
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$statusVenta = $this->input->post('statusVenta');
			
			//Arreglo para parsear por json
	        $data = array();            
	        $data['response'] = 'true';
	        $data['html']     = '';
			
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFraccionamiento' => $idFraccionamiento,
                'statusVenta' => $statusVenta,
                'statusCasa' => 1
            );
			
			$arrOrderBy = "numCasa,interior asc";
			
			$casas = $this -> defaultdata_model -> consultaInfo('casa', $arrDataWhere,$arrOrderBy,false,null);
			$totalCasas = $this -> defaultdata_model -> consultaInfo('casa', $arrDataWhere,$arrOrderBy,true,null);
			$totalSum = $this -> defaultdata_model -> consultaInfo('casa', $arrDataWhere,$arrOrderBy,false,'precioLista');
			//echo var_dump($casas);
			
			foreach($totalCasas as $key)
				$total = $key->totalCount;
			
			foreach($totalSum as $key)
				$totalSuma = $key->totalSum;
						
			if($casas != '') {
				
				$nombreFrac = '';
				$nombrePrototipo = '';
				$strong = true;
            	$class = '';
				
				$data['html'] .= ' <tr> <th colspan="4" align="right"> &nbsp; </th> <th> N. Registros </th> <th align="left"> '.$total.' </th> <th> Suma Total</th> <th>$'.number_format($totalSuma).'</th> <th colspan="2"> &nbsp; </th> </tr>
						<tr>
			                <th>Fraccionamiento</th> <th>N. Casa</th> <th>Prototipo</th> <th>Terreno</th> <th>Construcci&oacute;n</th> <th>Orientaci&oacute;n</th> <th>Acabados</th> <th>Precio de lista</th> <th> Venta </th> <th></th>
			            </tr>';
				foreach ($casas as $key):
					
		            if($strong):
		                $class = 'strong';
		                $strong = false;
		            elseif(!$strong):
		                $class = 'light';
		                $strong = true;
		            endif;
					
					$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$key->idFraccionamiento);
					$nombrePrototipo = $this -> defaultdata_model -> getNameRow('nombrePrototipo','prototipo','idPrototipo',$key->idPrototipo);
					
					//echo var_dump($nombrePrototipo);
					
					switch($key->statusVenta):
						case '1':
							$venta = 'Disponible';
						break;
						case '2':
							$venta = 'Apartada';
						break;
						case '3':
							$venta = 'Con contrato';
						break;
						case '4':
							$venta = 'Escriturada';
						break;
						case '5':
							$venta = 'Entregada';
						break;
					endswitch;
					
					$data['html'] .= '<tr class="'.$class.'">
	                	<td>' . $nombreFrac->nombreFrac . '</td>
	                	<td>' . $key->numCasa.' '.$key->interior . '</td>
	                    <td>' . $nombrePrototipo->nombrePrototipo . '</td>
	                    <td>' . number_format($key->metros) . '</td>
	                    <td>' . number_format($key->metrosConstruccion) . '</td>
	                    <td>' . $key->orientacion . '</td>
	                    <td>' . $key->acabados . '</td>
	                    <td>$' .number_format($key->precioLista) . '</td>
	                    <td>' . $venta . '</td>';
						
						if($key->promocion == 1) {   
	                    	$data['html'] .= '<td> 
	                    						<a id="editRow'.$key->idCasa.'" href="'.base_url().'admin/casasdisponibles/promocion/'.$key->idCasa.'" class="fancybox">
			                                		<img src="'.base_url().'/img/gift.png" />
			                            		</a>
			                            	</td>';
						}
						else {
							$data['html'] .= '<td> &nbsp;</td>';
						}
												
	                $data['html'] .= '</tr>';
					
				endforeach;
				
				$data['html'] .= '<tr> <th colspan="4" align="right"> &nbsp; </th> <th> N. Registros </th> <th align="left"> '.$total.' </th> <th> Suma Total</th> <th>$'.number_format($totalSuma).'</th> <th colspan="2"> &nbsp; </th> </tr>';	
				$data['response'] = 'true';
			}
			else {
				$data['response'] = 'false';
			}

			echo json_encode($data); 
		}
	}

	function promocion($idCasa = null) {
        if($idCasa == null){
            redirect('admin/casasdisponibles');
        }
        
        $data = array();
		$data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Promoci&oacute;n';
		$data['secciones'] = getMyMenu($this->idRol);
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);         
        $data['promocion'] = $this -> tablapromociones_model -> getPromocion($idCasa);
                
        /*********  DYNAMIC DATA  *********/
        $data['module'] = 'admin/casasdisponibles_view';
        $data['fancy'] = 'admin/casapromocion_view';
        $this->load->view('admin/main_view', $data);
    }
    
}