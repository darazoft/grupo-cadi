<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ComisionesFrac extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
		
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 8)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
            $this -> load -> model ('tabla_comisionfrac_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> session -> userdata('idRol'));
		
		switch ($this -> idRol):
			case '1':
        		$data['comisiones'] = $this -> tabla_comisionfrac_model -> getComisionesfrac(true);
			break;
			
			case '8':
        		$data['comisiones'] = $this -> tabla_comisionfrac_model -> getComisionesfrac(true);
			break;
		endswitch;
		
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
        $data['module'] = 'admin/comisionesfrac_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('numcasas','N&uacute;mero de casas','trim|required|xss_clean');
        $this->form_validation->set_rules('comision','Comision','trim|required|xss_clean');
        		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$num_casas = $this->input->post('numcasas');
            $comision = $this->input->post('comision');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusComision  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'num_casas' => $num_casas,
                'comision' => $comision,
                'fechaRegistro' => $fechaRegistro,
                'statusComision' => $statusComision,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusComision' => '1',
                'idFraccionamiento' => $idFraccionamiento,
                'num_casas' => $num_casas
            );
			
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'fraccionamientocomision', true,$arrDataWhere,NULL);
            if($lastID> 0){
            	$nombreFrac = '';
            	$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFraccionamiento);
				
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                	<td>' . $nombreFrac->nombreFrac . '</td>
                    <td>' . $num_casas . '</td>
                    <td>' . $comision . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane' . $lastID . '">
                            <a id="editRow' . $lastID . '" href="' . base_url() . 'admin/comisionesfrac/editar/' . $lastID . '">
                                <img src="' . base_url() . '/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow' . $lastID . '" href="' . $lastID . '" class="deleteRow">                            
                                <img src="' . base_url() . '/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
				$data['response'] = 'true';                                                                
            }
			else {
				$data['response'] = 'false';
			}
            echo json_encode($data);            
        }       
    }

	function editar($idFraccionamientoComision = null){
        if($idFraccionamientoComision == null){
            redirect('admin/comisionesfrac');
        }

        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Comisiones';
        //$data['pestana'] = 1;
        $data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('fraccionamientocomision', 'idFraccionamientoComision', $idFraccionamientoComision, null);
        
        if($data['info'] == null){            
            redirect('admin/comisionesfrac');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
        
        $data['module'] = 'admin/editar_comisionesfrac_view';
        
        /*DYNAMIC css*/
        $data['css'] = array(); 
        $data['css'][] = "dhtmlgoodies_calendar";
            
        $data['js'] = array(); 
        $data['js'][] = "dhtmlgoodies_calendar";
                
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
		$this->form_validation->set_rules('idFraccionamientoComision','ID Fraccionamiento','trim|required|xss_clean');
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('numcasas','N&uacute;mero de casas','trim|required|xss_clean');
        $this->form_validation->set_rules('comision','Comision','trim|required|xss_clean');
        		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/comisionesfrac');            
            
        } else{
            //Arreglo para parsear por json
            $data = array();            
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamientoComision = $this->input->post('idFraccionamientoComision');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$num_casas = $this->input->post('numcasas');
            $comision = $this->input->post('comision');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusComision  = 1;
		            
            $arrData = array(
                'idFraccionamiento' => $idFraccionamiento,
                'num_casas' => $num_casas,
                'comision' => $comision,
                'fechaRegistro' => $fechaRegistro,
                'statusComision' => $statusComision,
                'idAnterior' => $idFraccionamientoComision,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idFraccionamientoComision' => $idFraccionamientoComision
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusComision' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'fraccionamientocomision', $arrDataWhere, $idFraccionamientoComision, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/comisionesfrac');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/comisionesfrac');            
			}
			            
        }       
    }

	function eliminar() {
		//Arreglo para parsear por json
        $data = array();            
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idFraccionamientoComision = $this->input->post('idFraccionamientoComision');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusComision' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idFraccionamientoComision' => $idFraccionamientoComision
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'fraccionamientocomision', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}