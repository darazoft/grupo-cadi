<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");
	
class Galeria extends CI_Controller{
		
	function __construct(){
		parent::__construct();
		if (is_login_with_session($this) || is_login_with_cookies($this)) {
            $tipoUsuario = $this->session->userdata('tipoUsuario');
            if (!($tipoUsuario == '0' || $tipoUsuario == '1')) {
                $this->session->set_flashdata('error', 'error_1');
                redirect('');
            }
        } else {
            redirect('sesion/logout/admin/');
        }
		$this->load->model('galeria_model');//el modelo que administra a BD
		$this->load->helper(array('form', 'url'));
        $this->load->library('upload');
		$this->load->library('image_lib');
        $this->load->library('pagination');
	}
	
	function index() {
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        $data['pestana'] = 2;
		/*PAGINACION*/
		$config['base_url'] = base_url() . 'admin/galeria/index';
        $config['total_rows'] = $this->galeria_model->totalrows();
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['num_links'] = 5; //Numero de links mostrados en la paginación
        $config['first_link'] = 'Inicio';
        $config['last_link'] = 'Fin';
		$this->pagination->initialize($config);
		/*TERMINA PAGINACION*/
		
		
		//$data es la variable que le envia todo la info a la vista
		$data['galerias'] = $this->galeria_model->getAllImages(null, $config['per_page'],$this->uri->segment(4));
		$data['module'] = 'admin/galeria_view';		
        $this->load->view('admin/main_view', $data);
    }
	
	function do_upload($input, $carpeta, $width, $height) {
        if (!empty($_FILES[$input]['name'])) {
            $config['upload_path'] = $carpeta;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['max_width'] = $width;
            $config['max_height'] = $height;
            $config['file_name'] = substr(md5(uniqid(rand())), 0, 10);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input)) {
                return null;
            } else {
                $data = $this->upload->data();				
                return  $data['file_name'];
            }
        }
    }
	
	function addPhoto() {
		$this->form_validation->set_rules('categoria', 'Categoría', 'trim|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		
		if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            redirect('admin/galeria');
            return false;
        } else {
            $photo = $this->do_upload('userfile', 'docs/imgGalerias', 2048, 2048);
			if ($photo != null) {
                $arrInsert = array(
                    'imagen' => $photo,
                    'categoria' => $this->input->post('categoria')
                );
                $this->galeria_model->addImage($arrInsert);
                $this->session->set_flashdata('error', 'error_6');
                redirect('admin/galeria');
            } else {
                $this->session->set_flashdata('error', 'error_7');
                redirect('admin/galeria');
            }
        }
	}

	function deletePhoto($idGaleria) {
		$this->galeria_model->deleteImage($idGaleria);
        $this->session->set_flashdata('error', 'error_10');		
		
        redirect('admin/galeria');        
	}		
}
?>