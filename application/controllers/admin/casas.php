<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casas extends CI_Controller {
	
	var $idRol = '';
	var $idUsuario = '';
	
	public function __construct(){
		parent::__construct();
		//Checo si el usuario esta loggeado o no
		if(is_login_with_session($this) || is_login_with_cookies($this)){
		    if(!tengo_permiso($this -> session -> userdata('idRol'), 5)){
		        //idRol, idPermiso
		        $this->session->set_flashdata('error', 'error_1');
                $this->session->set_flashdata('notyType', 'warning');                
                redirect('admin/dashboard');            
		    }
			$this -> idRol = $this -> session ->userdata('idRol');
			$this -> idUsuario = $this -> session ->userdata('idUsuario');			
            $this -> load -> model('menu_model');
			$this -> load -> model ('tabla_prototipo_model');
			$this -> load -> model ('tabla_fraccionamiento_model');
			$this -> load -> model ('tablacasas_model');
            $this -> load -> model('defaultdata_model');         
		}
		else{
			//No esta loggeado, que ejecuto?
			//index() tons no hago nada
			redirect('sesion/logout/admin/');
		}
    }
    
    function index(){   	
		
        $data = array();        
        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | DashBoard';
        //$data['pestana'] = 1;
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this -> idRol);
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
		
		switch ($this -> idRol):
			case '1':
				$data['casas'] = $this -> tablacasas_model -> getCasas(true);
			break;
			
			case '8':
				$data['casas'] = $this -> tablacasas_model -> getCasas(true);
			break;
			
		endswitch;
		
        $data['module'] = 'admin/casas_view';
        
        /*DYNAMIC css*/
        // $data['css'] = array(); 
        // $data['css'][] = "admin/layout.css";
        
        $this->load->view('admin/main_view', $data);        
    }
    
    function nuevo_do(){
    	
        $this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idPrototipo','Usuario','trim|xss_clean');
		$this->form_validation->set_rules('metros','Metros','trim|required|xss_clean');
		$this->form_validation->set_rules('metrosConstruccion','Metros Construccion','trim|required|xss_clean');
		$this->form_validation->set_rules('numCasa','N. Casa','trim|required|xss_clean');
		$this->form_validation->set_rules('acabados','Acabados','trim|xss_clean');
		$this->form_validation->set_rules('orientacion','Orientacion','trim|xss_clean');
		$this->form_validation->set_rules('precioLista','Precio de Lista','trim|xss_clean');
		$this->form_validation->set_rules('statusVenta','Estatus','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
    
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
			$data['response'] = 'error_val';
            echo json_encode($data);
        } else{
            //Arreglo para parsear por json
            $data['response'] = 'true';
            $data['html']     = '';
            
            //Preparamos arreglo para ir a insertar
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idPrototipo = $this->input->post('idPrototipo');
			$metros = $this->input->post('metros');
			$metrosConstruccion = $this->input->post('metrosConstruccion');
			$numCasa = $this->input->post('numCasa');
			$interior = $this->input->post('interior');
			$acabados = $this->input->post('acabados');
			$orientacion = $this->input->post('orientacion');
			$precioLista = $this->input->post('precioLista');
			$statusVenta = $this->input->post('statusVenta');
			
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusCasa  = 1;
		            
            $arrData = array(
                'idPrototipo' => $idPrototipo,
                'idFraccionamiento' => $idFraccionamiento,
                'metros' => $metros,
                'metrosConstruccion' => $metrosConstruccion,
                'numCasa' => $numCasa,
                'interior' => $interior,
                'acabados' => $acabados,
                'orientacion' => $orientacion,
                'precioLista' => $precioLista,
                'fechaRegistro' => $fechaRegistro,
                'statusVenta' => $statusVenta,
                'statusCasa' => $statusCasa,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones para validar si existe
			 * algún registro similar
			 */
			$arrDataWhere = array(
                'statusCasa' => '1',
                'idFraccionamiento' => $idFraccionamiento,
                'numCasa' => $numCasa,
                'interior' => $interior
            );
			
			
			$lastID = $this -> defaultdata_model -> addInfo($arrData, 'casa', true, $arrDataWhere, NULL);
			
            if($lastID > 0) {	
            	$nombreFrac = '';
				$nombrePrototipo = '';
            	$nombreFrac = $this -> defaultdata_model -> getNameRow('nombreFrac','fraccionamiento','idFraccionamiento',$idFraccionamiento);
				$nombrePrototipo = $this -> defaultdata_model -> getNameRow('nombrePrototipo','prototipo','idPrototipo',$idPrototipo);
				
				switch ($statusVenta):
					case '1':
						$statusVenta = 'Disponible';		
					break;
					
					case '2':
						$statusVenta = 'Apartada';		
					break;
					
					case '3':
						$statusVenta = 'Con contrato';		
					break;
					
					case '4':
						$statusVenta = 'Escriturada';		
					break;
					
					case '5':
						$statusVenta = 'Entregada';		
					break;
				endswitch;
				
               	$data['response'] = 'true';
                $data['html'] = '<tr id="' . $lastID  . '" class="light optsPane">
                	<td>' . $nombreFrac->nombreFrac . '</td>
                	<td>' . $numCasa.' '.$interior . '</td>
                    <td>' . $nombrePrototipo->nombrePrototipo . '</td>
                    <td>' . $metros . '</td>
                    <td>$' . $precioLista . '</td>
                    <td>' . $fechaRegistro . '</td>
                    <td>' . $statusVenta . '</td>
                    <td class="optionsPane" ontouchstart="touchStart(event,'.$lastID.');">
                        <span>
                            Activo
                        </span>
                        
                        <div id="optsPane'.$lastID.'">
                            <a id="editRow'.$lastID.'" href="'.base_url().'admin/casas/editar/'.$lastID.'">
                                <img src="'.base_url().'/img/edit_row.png" />
                            </a>
                            
                            <a id="deleteRow'.$lastID.'" href="'.$lastID.'" class="deleteRow">                            
                                <img src="'.base_url().'/img/delete_row.png" />
                            </a>
                        </div>                     
                    </td>                 
                </tr>';
				
				$data['response'] = 'true';                                                                
            }
			else {
				$data['response'] = 'false';	
			}
            echo json_encode($data);            
        }       
    }

	function editar($idCasa = null){
        if($idCasa == null){
            redirect('admin/casas');
        }

        $data['SYS_metaTitle'] = 'CADI';
        $data['SYS_metaDescription'] = 'Administracion | Editar Casas';
        //$data['pestana'] = 1;
        
        		
		// $arrDataJoin = array(
			// array('fraccionamiento as f','f.idFraccionamiento = c.idFraccionamiento','left'),
			// array('prototipo as p','p.idPrototipo = c.idPrototipo','left')
		// );
		
        /*USER INFO*/        
        $data['info'] = $this -> defaultdata_model -> getInfo('casa as c', 'idCasa', $idCasa);
        
        if($data['info'] == null){            
            redirect('admin/casas');                
        }
        
        /*********  DYNAMIC DATA  *********/                        
        $data['secciones'] = getMyMenu($this->idRol);
		$data['fraccionamientos'] = $this -> tabla_fraccionamiento_model -> getFraccionamientos(true);
		$data['prototipos'] = $this -> tabla_prototipo_model -> getPrototipos(true);
        
        $data['module'] = 'admin/editar_casa_view';
                        
        $this->load->view('admin/main_view', $data);
    }

	function editar_do(){
         
		$this->form_validation->set_rules('idFraccionamiento','Fraccionamiento','trim|required|xss_clean');
		$this->form_validation->set_rules('idPrototipo','Usuario','trim|required|xss_clean');
		$this->form_validation->set_rules('metros','Metros','trim|required|xss_clean');
		$this->form_validation->set_rules('metrosConstruccion','Metros Construccion','trim|required|xss_clean');
		$this->form_validation->set_rules('numCasa','N. Casa','trim|required|xss_clean');
		$this->form_validation->set_rules('acabados','Acabados','trim|xss_clean');
		$this->form_validation->set_rules('orientacion','Orientacion','trim|xss_clean');
		$this->form_validation->set_rules('precioLista','Precio de Lista','trim|xss_clean');
		$this->form_validation->set_rules('statusVenta','Estatus','trim|required|xss_clean');
                		       
        $this->form_validation->set_message('required','El campo "%s" es requerido');
        $this->form_validation->set_message('xss_clean','El campo "%s" contiene un posible ataque XSS');
        $this->form_validation->set_error_delimiters('<span class="error">','</span>');
		           
        // Ejecuto la validacion de campos de lado del servidor
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', 'error_7');
            $this->session->set_flashdata('notyType', 'warning');                
            redirect('admin/casas');            
            
        } else{
                        
            //Preparamos arreglo para ir a insertar
            $idCasa = $this->input->post('idCasa');
            $idFraccionamiento = $this->input->post('idFraccionamiento');
			$idPrototipo = $this->input->post('idPrototipo');
			$metros = $this->input->post('metros');
			$metrosConstruccion = $this->input->post('metrosConstruccion');
			$numCasa = $this->input->post('numCasa');
			$interior = $this->input->post('interior');
			$acabados = $this->input->post('acabados');
			$orientacion = $this->input->post('orientacion');
			$precioLista = $this->input->post('precioLista');
			$statusVenta = $this->input->post('statusVenta');
            $fechaRegistro  = date('Y-m-d H:i:s');
			$statusCasa  = 1;
		            
            $arrData = array(
                'idPrototipo' => $idPrototipo,
                'idFraccionamiento' => $idFraccionamiento,
                'metros' => $metros,
                'metrosConstruccion' => $metrosConstruccion,
                'numCasa' => $numCasa,
                'interior' => $interior,
                'acabados' => $acabados,
                'orientacion' => $orientacion,
                'precioLista' => $precioLista,
                'fechaRegistro' => $fechaRegistro,
                'statusVenta' => $statusVenta,
                'statusCasa' => $statusCasa,
                'idAnterior' => $idCasa,
                'idUser' => $this -> idUsuario
            );
			
			/***
			 * Array de condiciones con id anterior
			 */
			$arrDataWhere = array(
                'idCasa' => $idCasa
            );
			
			/***
			 * Array de campos a alterar id anterior
			 */
			$arrDataUpdate = array(
                'statusCasa' => 2,
                'statusTipo' => 'updated',
                'idUser' => $this -> idUsuario
            );
			                        
            if($this -> defaultdata_model -> updateInfo($arrData, 'casa', $arrDataWhere, $idCasa, $arrDataUpdate) ){
            	
				$this->session->set_flashdata('error', 'error_5');
                $this->session->set_flashdata('notyType', 'success');                
                redirect('admin/casas');            
            }
			else {
				$this->session->set_flashdata('error', 'error_7');
                $this->session->set_flashdata('notyType', 'error');                
                redirect('admin/casas');            
			}
                        
       	}       
    }
	
	function eliminar() {
		//Arreglo para parsear por json
        $data['response'] = 'true';
        $data['html']     = '';
        
        //Preparamos arreglo para ir a eliminar
        $idUser = $this -> idUsuario;
        $tipo = $this->input->post('tipo'); 
        $idCasa = $this->input->post('idCasa');
		
		$arrData = array(
			'statusTipo' => $tipo,
			'statusCasa' => '2',
			'idUser' => $idUser
		);
		
		$arrDataWhere = array(
			'idCasa' => $idCasa
		);
			
		if($this -> defaultdata_model -> deleteInfo($arrData, 'casa', $arrDataWhere) > 0){
            $data['response'] = 'true';                                                                
        }
		else {
			$data['response'] = 'false';
		}
		
        echo json_encode($data);
		
	}
	
}