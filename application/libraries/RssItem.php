<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RssItem {
	var $title, $url, $description, $fecha;
	function RssItem ($xml){
		$this->populate ($xml);
	}
	function populate ($xml){
		preg_match ("/<title> (.*) <\/title>/xsmUi", $xml, $matches);
		$this->title = $matches[1];
		preg_match ("/<link> (.*) <\/link>/xsmUi", $xml, $matches);
		$this->url = $matches[1];
		preg_match ("/<description> (.*) <\/description>/xsmUi", $xml, $matches);
		$this->description = $matches[1];
		preg_match ("/<pubDate> (.*) <\/pubDate>/xsmUi", $xml, $matches);
		$this->fecha = $matches[1];
	}
	function obtener_titulo (){
		return utf8_decode($this->title);
	}
	function obtener_url (){
		return utf8_decode($this->url);
	}
	function obtener_fecha (){
		return utf8_decode($this->fecha);
	}
	function obtener_descripcion (){
		$posicion = strrpos( utf8_decode($this->description), "<![CDATA[");
		if ($posicion === false) {
			return utf8_decode($this->description);
		}else{
			return substr(utf8_decode($this->description), $posicion+9, -8);
		}
	}
}

/* End of file RssItem.php */