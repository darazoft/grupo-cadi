<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prospecto_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getProspectos($idUsuario,$idRol,$where, $count) {
    	
		$idFraccionamiento = '';
    	if($idUsuario != null && $idRol != null) {
    		
    		$query = $this->db->select('idFraccionamiento');
			$query = $this->db->where(array('idUsuario' => $idUsuario, 'statusUsuario' => '1'));
			$query = $this->db->get('fraccionamientousuario');
			
			if($query->num_rows() != 0) {
				
				$wherein = array();
				foreach ($query -> result() as $key) {
					$wherein[] = $key->idFraccionamiento;
				}
				
	            $idFraccionamiento = $key->idFraccionamiento;
				
				if(!$count)
					$querytwo = $this->db->select('f.nombreFrac, idHojaVida,nombre,CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) cliente, email,ladaFijo,telFijo,ladaCelular,telCelular,hv.fechaRegistro,statusHoja,ec.clasificacion',false);
				else
					$querytwo = $this->db->select('count(*) as total',false);
					  
				$querytwo = $this->db->from('fraccionamiento as f');
				$querytwo = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
				$querytwo = $this->db->join('usuario as us','us.idUsuario = hv.idUsuario');
				$querytwo = $this->db->join('estatuscliente as ec','ec.idEstatusCliente = hv.idEstatusCliente');
				$querytwo = $this->db->where(array('statusHoja'=>'1'));
				
				if($where)
					$query = $this->db->where($where);
				
				$querytwo = $this->db->where_in('hv.idFraccionamiento',$wherein);
				
				if(!$count) {
					$querytwo = $this->db->order_by('f.nombreFrac','asc');
					$querytwo = $this->db->order_by('nombre','asc');
					$querytwo = $this->db->order_by('cliente','asc');
				}
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows() > 0) {
					return $querytwo->result();
				}
			}

			return null;
		}
		else if($idUsuario != null) {
			
				if(!$count) 
					$querytwo = $this->db->select('f.nombreFrac, idHojaVida,nombre,CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) cliente,email,ladaFijo,telFijo,ladaCelular,telCelular,hv.fechaRegistro,statusHoja, ec.clasificacion',false);
				else 
					$querytwo = $this->db->select('count(*) as total',false);
				 
				$querytwo = $this->db->from('fraccionamiento as f');
				$querytwo = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
				$querytwo = $this->db->join('usuario as us','us.idUsuario = hv.idUsuario');
				$querytwo = $this->db->join('estatuscliente as ec','ec.idEstatusCliente = hv.idEstatusCliente');
				$querytwo = $this->db->where(array('hv.idUsuario' => $idUsuario, 'statusHoja' => '1'));
				
				if($where)
					$query = $this->db->where($where);
				
				if(!$count) {
					$querytwo = $this->db->order_by('f.nombreFrac','asc');
					$querytwo = $this->db->order_by('nombre','asc');
					$querytwo = $this->db->order_by('cliente','asc');
				}	
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows()>0)
					return $querytwo->result();
				
			return null;
		}
		else if($idRol != null) {
			
			if(!$count)
				$query = $this->db->select('f.nombreFrac, idHojaVida,nombre,CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) cliente,email,ladaFijo,telFijo,ladaCelular,telCelular,hv.fechaRegistro,statusHoja, ec.clasificacion',false);
			else 
				$query = $this->db->select('count(*) as total',false);
			 
			$query = $this->db->from('fraccionamiento as f');
			$query = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
			$query = $this->db->join('usuario as us','us.idUsuario = hv.idUsuario');
			$query = $this->db->join('estatuscliente as ec','ec.idEstatusCliente = hv.idEstatusCliente');
			$query = $this->db->where(array('statusHoja'=>'1'));
			
			if($where)
				$query = $this->db->where($where);
			
			if(!$count) {
				$query = $this->db->order_by('f.nombreFrac','asc');
				$query = $this->db->order_by('nombre','asc');
				$query = $this->db->order_by('cliente','asc');
			}
			$query = $this->db->get();
			
			if($query->num_rows() != 0)
            	return $query -> result();
        	return null;
		}

        return null;
    }

	// function getActividades($idHojaVida, $where) {
// 			
        // $query = $this -> db -> get_where('hojaactividad as ha', $where);
        // if($query -> num_rows() > 0) {
//         	
			// $arrDataCom = array();
			// foreach ($query -> result() as $key): 
// 				
				// $querytwo = $this -> db -> select('IF((ADDDATE(fechaInicio, INTERVAL 1 DAY) > NOW() ), \'yellow\', \'red\' ) as estado, ha.*, us.nombre, co.idComentario, co.idUsuario, co.comentario, co.fechaRegistro',FALSE);
				// $querytwo = $this -> db -> from ('hojaactividad ha');
				// $querytwo = $this -> db -> join('comentario co', 'ha.idHojaActividad = co.idHojaActividad','left');
				// $querytwo = $this -> db -> join('usuario us', 'us.idusuario = co.idUsuario','left');
				// $querytwo = $this -> db -> where($where);
        		// $querytwo = $this -> db -> where(array('ha.idHojaActividad'=>$key->idHojaActividad) );
				// $querytwo = $this -> db-> get();
				// if($querytwo -> num_rows() != 0) {
// 					
					// $arrDataCom[] = $querytwo->result();
				// }	
// 				
			// endforeach;
// 		
			// return $arrDataCom;	
       	// }
// 		
        // return null;
    // }
	
	function getActividades($idHojaVida, $where) {
		
		$query = $this -> db -> select('IF((ADDDATE(fechaInicio, INTERVAL 1 DAY) > NOW() ), \'yellow\', \'red\' ) as estado, ha.*',FALSE);	
        $query = $this -> db -> get_where('hojaactividad as ha', $where);
        if($query -> num_rows() > 0)
            return $query -> result();
        return null;
    }
	
	function validaMiId($idRol, $idUsuario, $idHojaVida) {
			
		$query = $this->db->select('idFraccionamiento');
		$query = $this->db->where(array('idUsuario' => $idUsuario, 'statusUsuario' => '1'));
		$query = $this->db->get('fraccionamientousuario');
		
		if($query->num_rows() != 0) {
			
			$registros=0;
			foreach( $query -> result() as $key ) {
	            $idFraccionamiento = $key->idFraccionamiento;
										
				$querytwo = $this->db->select('idHojaVida'); 
				$querytwo = $this->db->from('hojavida');
				$querytwo = $this->db->where(array('idFraccionamiento'=>$idFraccionamiento,'idHojaVida'=>$idHojaVida,'statusHoja'=>'1'));
								
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows() > 0) {
					$registros++;
				}
			}
			
			if($registros > 0)
				return 1;
			return 0;
		}
		
		return 0;
	}
	
	// function validaActividad($idRol, $tabla, $campo, $where) {	
		// /***
		 // * Funciona para validar que la actividad solo 
		 // * se realice sobre porspectos que le pertenecen
		 // * al usuario logueado
		 // * Aplica para wm, admin, coordinador, asesor
		 // */
		 // $query = $this->db->select($campo);
		 // $query = $this->db->from($tabla);
		 // $query = $this->db->where($where);
		 // $query = $this->db->get();
// 		 
		 // if($query->num_rows() !=0)
		 	// return 1;
// 		 
		 // return 0;
	// }
	
	function getEstatusHoja($idHojaVida) {
			
		$query = $this->db->select('clasificacion');
		$query = $this->db->from('hojavida hv');
		$query = $this->db->join('estatuscliente ec','hv.idEstatusCliente = ec.idEstatusCliente');
		$query = $this->db->where(array('statusHoja' => 1, 'idHojaVida' => $idHojaVida));
		$query = $this->db->get();
		 
		if($query->num_rows() !=0)
			return $query->row();
		 
		return 0;
	}
	
	function getComentariosActividad($idHojaActividad) {
		
		$query = $this -> db -> select('co.*, us.nombre');
		$query = $this -> db -> from('comentario co');
		$query = $this -> db -> join('usuario us', 'us.idusuario = co.idUsuario','left');
        $query = $this -> db -> where(array('idHojaActividad' => $idHojaActividad));
		$query = $this -> db -> order_by('fechaRegistro','desc');
		$query = $this -> db -> get();
		
       	if($query -> num_rows() > 0)
            return $query -> result();
        return null;
    }
	
	function getComentariosCount($idHojaActividad) {
		
		$query = $this -> db -> select('COUNT(*) total');
		$query = $this -> db -> from('comentario co');
        $query = $this -> db -> where(array('idHojaActividad' => $idHojaActividad));
		
		$between = 'fechaRegistro between ADDDATE(fechaRegistro, INTERVAL -5 DAY) and NOW()';
		$query = $this -> db -> where($between);
		$query = $this -> db -> get();
		
       	if($query -> num_rows() > 0)
            return $query -> result();
        return null;
    }
	  
}