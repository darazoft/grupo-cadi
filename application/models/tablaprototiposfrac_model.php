<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablaprototiposfrac_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPrototiposfrac($activo) {
    	        
		$query = $this->db->select('nombreFrac, nombrePrototipo, fp.*'); 
		$query = $this->db->from('fraccionamiento as f');
		$query = $this->db->join('fraccionamientoprototipo as fp','f.idFraccionamiento = fp.idFraccionamiento');
		$query = $this->db->join('prototipo as p','´p.idPrototipo = fp.idPrototipo');
		
		if($activo) {
    		$this->db->where('statusFP', '1');
    	}
		else {  // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusFP !='updated' or statusFP is null)";
			$this->db->where($special);
		}
		
		$this->db->order_by('nombreFrac', 'asc');
		$this->db->order_by('nombrePrototipo', 'asc');
		
		$query = $this->db->get();

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
}