<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabla_usuario_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getUsuarios($activo,$notin,$myUser) {
    	
		// if($notin) {
			// /***
			 // * Busco id's usuario activo
			 // */
			// $queryUs = $this->db->select('idUsuario');
			// $queryUs = $this->db->select('idUsuario');
			// $queryUs = $this->db->get('usuario');
// 			
			// if($queryUs->num_rows() != 0)
				// return $queryUs -> result();
// 		
			// $this->db->where_not_in('idUsuario', $array);	
		// }
		
		if($activo) {
    		$this->db->where('estatus', '1');
    	}
		else { // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusTipo !='updated' or statusTipo is null)";
			$this->db->where($special);
		}
		
        $this->db->where('idRol !=', '8');
		$this->db->where('idUsuario !=', $myUser);
		$this->db->order_by('nombre', 'asc');
		$query = $this->db->get('usuario');

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    }
	
	function getUsuarioTipo($tipo) {
		
		$this->db->where('idRol', $tipo);
		$this->db->where('estatus', '1');
		$query = $this->db->get('usuario');

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
	}
	
	function getMisUsuarios($idUsuario) {
		
		$query = $this->db->select('idFraccionamiento');
		$query = $this->db->where(array('idUsuario' => $idUsuario, 'statusUsuario' => '1'));
		$query = $this->db->get('fraccionamientousuario');
		
		if($query->num_rows() != 0) {
			
			$wherein = array();
			foreach ($query -> result() as $key) {
				$wherein[] = $key->idFraccionamiento;
			}
			
			$querytwo = $this->db->select('DISTINCT(u.idUsuario) as idUsuario, u.nombre',false);
			$querytwo = $this->db->from('fraccionamientousuario as fu');
			$querytwo = $this->db->join('usuario as u','u.idUsuario = fu.idUsuario');
			$querytwo = $this->db->where(array('u.idRol' => 3, 'fu.statusUsuario' => '1'));
			$querytwo = $this->db->where_in('idFraccionamiento',$wherein);
			
			$querytwo = $this->db->get();
			if($querytwo->num_rows() != 0)
	            return $querytwo -> result();
		}
		
        return null;
    } 
	
}