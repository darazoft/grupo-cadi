<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablacasas_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getCasas($activo) {
    			        
		$query = $this->db->select('nombreFrac, nombrePrototipo, c.*'); 
		$query = $this->db->from('fraccionamiento as f');
		$query = $this->db->join('casa as c','f.idFraccionamiento = c.idFraccionamiento');
		$query = $this->db->join('prototipo as p','p.idPrototipo = c.idPrototipo');
		
		if($activo) {
    		$this->db->where('statusCasa', '1');
    	}
		else {  // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusCasa !='updated' or statusCasa is null)";
			$this->db->where($special);
		}
		
		$this->db->order_by('nombreFrac', 'asc');
		$this->db->order_by('numCasa', 'asc');
		$query = $this->db->get();

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
}