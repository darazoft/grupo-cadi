<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuario_model extends CI_Model {
    
    var $_tabla = 'usuario';

    function __construct() {
        parent::__construct();
    }

    /*
      Metodos para manejo de un solo usuario
     */

    function login_correct($emailUsuario, $contrasenaUsuario, $concookie) {
        $this->db->select('emailUsuario, contrasenaUsuario');
        $this->db->where('emailUsuario', $emailUsuario);
		$this->db->where('estatus', 1);
        if ($concookie == "true") {
            $this->db->where('hashUsuario', $contrasenaUsuario);
        } else {
            $this->db->where('contrasenaUsuario', $contrasenaUsuario);
        }
        $resultado = $this->db->get('usuario');
        return ($resultado->num_rows() > 0);
    }

    function login_user($emailUsuario, $recordarme, $encryptionKey) {
        // Destruyo Cookies existentes
        if ($this->input->cookie($encryptionKey . 'emailUsuario') != FALSE || $this->input->cookie($encryptionKey . 'contrasenaUsuario') != FALSE) {
            $cookieUser = array(
                'name' => 'emailUsuario',
                'value' => '',
                'expire' => '',
                'path' => '/',
                'prefix' => $encryptionKey,
                'secure' => FALSE
            );
            $cookiePass = array(
                'name' => 'contrasenaUsuario',
                'value' => '',
                'expire' => '',
                'path' => '/',
                'prefix' => $encryptionKey,
                'secure' => FALSE
            );
            $this->input->set_cookie($cookieUser);
            $this->input->set_cookie($cookiePass);
        }

        // Update en la DB con nueva cookie
        $arrUpdate = array();
        $arrUpdate['hashUsuario'] = substr(md5(uniqid(rand())), 0, 10);
        $this->db->where('emailUsuario', $emailUsuario);
		$this->db->where('estatus', 1);
        $this->db->update('usuario', $arrUpdate);

        // Obtengo info de la db
        $this->db->where('emailUsuario', $emailUsuario);
		$this->db->where('estatus', 1);
        $resultado = $this->db->get('usuario');
        $info = $resultado->row();
        $this->session->set_userdata(array('adentro' => true,
            'idUsuario' => $info->idUsuario,
            'idRol' => $info->idRol,
            'tipoUsuario' => $info->tipoUsuario,
            'estatus' => $info->estatus,
            'emailUsuario' => $info->emailUsuario,
			'nombreUsuario' => $info->nombre));

        //Creo la cookie con la que guardo los datos del usuario;  
        if ($recordarme == "true") {
            setcookie($encryptionKey . 'emailUsuario', $emailUsuario, time() + 60 * 60 * 24 * 30, "/");
            setcookie($encryptionKey . 'contrasenaUsuario', $arrUpdate['hashUsuario'], time() + 60 * 60 * 24 * 30, "/");
        }
        return true;
    }

    function cambiarContrasena($contrasenaActual, $contrasenaUsuario, $idUsuario) {
        $this->db->select('contrasenaUsuario');
        $this->db->where('idUsuario', $idUsuario);
        $this->db->where('contrasenaUsuario', $contrasenaActual);
        $resultado = $this->db->get($this->_tabla);
        if ($resultado->num_rows() >= 1) {
            $arrNewPass = array();
            $arrNewPass['contrasenaUsuario'] = $contrasenaUsuario;
            $this->db->where('idUsuario', $idUsuario);
            $this->db->update($this->_tabla, $arrNewPass);
            return true;
        } else {
            return false;
        }
    }

    function getUsers() {
        $this->db->select('idUsuario, emailUsuario, estatus');
        $this->db->where('tipoUsuario', '1');
        $query = $this->db->get($this->_tabla);
        return $query->result();
    }

    function agregarUsuario($dataArr) {
        $this->db->insert($this->_tabla, $dataArr);
        return true;
    }

    function eliminar($delArr) {
        $this->db->where('tipoUsuario', '1');
        $query = $this->db->get($this->_tabla);
        if ($query->num_rows() >= 2) {
            $this->db->delete($this->_tabla, $delArr);
            return true;
        } else {
            return false;
        }
    }

    function estatus($idUsuario, $updateArr) {
        $this->db->where('idUsuario', $idUsuario);
        $this->db->update($this->_tabla, $updateArr);
        return true;
    }

    /*
      Metodos para manejo de todos los Usuarios
     */
}