<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabla_fraccionamiento_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getFraccionamientos($activo) {
    	if($activo) {
    		$query = $this->db->where('statusFrac',1);
    	}
		$this->db->order_by('nombreFrac', 'asc');        
		$query = $this->db->get('fraccionamiento');

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    }
	
	function getFracCasa($activo) {
				
		/* 
		 * Query para obtener los fraccionamientos activos, relacionados
		 * con las casas, activas y en estatus disponible
		 */
		$query = $this -> db -> select('idFraccionamiento');
		$query = $this-> db ->group_by('idFraccionamiento');
		$query = $this -> db -> get_where('casa',array('statusCasa' => 1, 'statusVenta' => 1, 'promocion'=>2 ));

        if($query->num_rows() != 0) {
        		
        	$arrDataFrac = array();  //Creamos arreglo. 
        				
			foreach($query -> result() as $key):
		 
		 		/*
				 * Query que obtiene las casas relacionadas con el fraccionamiento
				 * correpondiente.
				 */
				$querytwo = $this->db->select('f.nombreFrac,p.nombrePrototipo,c.*');
				$querytwo = $this->db->from('fraccionamiento as f');
				$querytwo = $this->db->join('casa as c','f.idFraccionamiento = c.idFraccionamiento');
				$querytwo = $this->db->join('prototipo as p','p.idPrototipo = c.idPrototipo');
				$querytwo = $this->db->where(array('c.idFraccionamiento' => $key->idFraccionamiento, 'statusCasa' => 1, 'statusVenta' => 1, 'promocion' => 2) );
				$querytwo = $this->db->order_by('numCasa', 'asc');
				$querytwo = $this->db->order_by('interior', 'asc'); 
				       
				$querytwo = $this->db->get();
				
				if($querytwo -> num_rows() != 0) {
					
					$arrDataFrac[] = $querytwo->result();
				}	
		
			endforeach;
			
			return $arrDataFrac;
		}
        return null;
    }

	function getMisFraccionamientos($idUsuario) {
				
		$query = $this->db->select('f.idFraccionamiento, f.nombreFrac');
		$query = $this->db->from('fraccionamientousuario as fu');
		$query = $this->db->join('fraccionamiento as f','f.idFraccionamiento = fu.idFraccionamiento');
		$query = $this->db->where(array('fu.idUsuario' => $idUsuario, 'statusUsuario' => '1'));
		$query = $this->db->get();
		
		if($query->num_rows() != 0)
            return $query -> result();
        return null;
    }
	
	
	
}