<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabla_comisionfrac_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getComisionesfrac($activo) {
    	
		$query = $this->db->select('nombreFrac, fc.*'); 
		$query = $this->db->from('fraccionamiento as f');
		$query = $this->db->join('fraccionamientocomision as fc','f.idFraccionamiento = fc.idFraccionamiento');
				
		if($activo) {
    		$this->db->where('statusComision', '1');
    	}
		else {  // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusComision !='updated' or statusComision is null)";
			$this->db->where($special);
		}
		
		$this->db->order_by('nombreFrac', 'asc');
		$this->db->order_by('num_casas', 'asc');
		$query = $this->db->get();
		

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
}