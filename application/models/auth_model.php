<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {
    
    function __construct() {
        parent::__construct();        
    }
    
    function tengo_permiso($idRol, $idPermiso){
        $query = $this -> db -> get_where('roltienepermiso', array('idPermiso' => $idPermiso, 'idRol' => $idRol));
        
        if($query -> num_rows() != 0)
            return true;
        return false;
    }
       
}