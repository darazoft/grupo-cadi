<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablapromociones_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPromociones($activo) {
    	        
		$query = $this->db->select('f.nombreFrac, c.numCasa, c.interior, fp.*'); 
		$query = $this->db->from('fraccionamiento as f');
		$query = $this->db->join('fraccionamientopromocion as fp','f.idFraccionamiento = fp.idFraccionamiento');
		$query = $this->db->join('casa as c','c.idCasa = fp.idCasa');
		
		if($activo) {
			$query = $this-> db -> where('statusPromo',1);
		}
		$this->db->order_by('nombreFrac', 'asc');
		$this->db->order_by('numCasa', 'asc');
		$this->db->order_by('interior', 'asc');
		$query = $this->db->get();

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
	
	function getCaducidad($id){
		
		$query = $this -> db -> select('IF(ADDDATE(fechaFin, INTERVAL 1 DAY) > NOW(), 1, 0 ) as res',FALSE);
		$query = $this -> db -> get_where('fraccionamientopromocion',array('statusPromo'=>1, 'idFraccionamientoPromocion'=>$id));
		
		if($query->num_rows() != 0) {
            	
            $value = $query -> row() -> res;
			if($value == 0) {
				
				$data = array(
					'statusPromo' => 3
				);
				
				$this -> db -> where('idFraccionamientoPromocion',$id);        
		        $this -> db -> update('fraccionamientopromocion', $data);
		        	
			}	
		}
	}
	
	function getPromocion($casa) {
		
		$query = $this->db->select('titulo, promocion, fechaInicio, fechaFin');
		$query = $this->db->get_where('fraccionamientopromocion',array('idCasa'=>$casa,'statusPromo'=>1));

        if($query->num_rows() != 0)
            return $query -> row();
        return null;
	}
}