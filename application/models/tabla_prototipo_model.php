<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabla_prototipo_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPrototipos($activo) {
    	if($activo) {
    		$query = $this->db->where('statusPrototipo',1);
    	}        
		$this->db->order_by('nombrePrototipo', 'asc');
		$query = $this->db->get('prototipo');

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
	
}