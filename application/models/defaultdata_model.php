<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Defaultdata_model extends CI_Model {
    
    function __construct() {
        parent::__construct();        
    }
    
    function addInfo($data, $table, $return,$where,$like) {
    	
		/*** 
		 * Verificamos que no exista un registro similar, 
		 * para evitar duplicidad
		 * **/
		if($like != null) {
			$query = $this -> db -> like($like);
		}
		
		if($where != null) {
			$query = $this -> db -> where($where);
			$query = $this -> db ->get($table);
			if($query->num_rows()!=0)
				return 0;
		}	
		/******** Termina la validación ********/
		
       	$this -> db -> insert($table, $data);
        if($return)
            return $this->db->insert_id();
        return true;
    }
    
    function updateInfo($data, $tabla, $where,$id, $dataUpdate){
        
		$DB_inicial = '';
		
		/* 1.- Consulto idInicial del 
		 * registro que actualizaremos 
		 */
		$query = $this -> db -> select('idInicial');
		$query = $this -> db -> get_where($tabla,$where);
		
		if($query -> num_rows() > 0) {
			
			foreach ($query->result() as $key):
						
				if($key->idInicial != ''):
					$DB_inicial = $key->idInicial; 
				else: 
					$DB_inicial = $id;
				endif;
			
			endforeach;
			
			/* 2.- Agregamos idInicial en el arreglo que 
			 *  traemos como parametro. $data
			 * para complementar el insert
			 */
			 $complement = array('idInicial' => $DB_inicial);
			 $data = array_merge($data,$complement);
			 
			 /* 3.- Insertamos nuevo registro
			  **/
			if( $this -> db -> insert($tabla, $data) ) {
			
				/**
				 * 4.- Update al registro anterior para desactivarlo.
				 */
				$this -> db -> where($where);
        		if($this -> db -> update($tabla, $dataUpdate) )
        			return true;
				else
					return false;		
			}
			else {
				return false;
			}
        	
		}
		else {
        	return false;
		}
    }
    
    function deleteInfo($data, $tabla, $where) {    	
		$val = 0;
       	$this -> db -> where($where);        
        if($this -> db -> update($tabla,$data)) {
        	$val = $this->db->affected_rows();
			return $val;
        }
		
		return 0;
  	}
	
	/*********************/
	/* Obtiene el nombre de 
	 * la coleccion correpondiente */
	function getNameRow($campo,$tabla,$where,$id) {
		$query = $this->db->select($campo,false);
		$query = $this->db->from($tabla);
		$query = $this->db->where(array($where => $id));
		$query = $this->db->get();
		if($query->num_rows() != 0)
            return $query -> row();
        return null;
	}
    
    function getInfo($table, $conditionField, $conditionValue) {
        $query = $this -> db -> get_where($table, array($conditionField => $conditionValue));
        if($query -> num_rows() > 0)
            return $query -> row();
        return null;
    }
	
	function getInfoJoin($table, $campos, $where ,$joins, $flag) {
		
		for($i=0; $i<count($joins); $i++) {
			
			if($flag)  //Verificar funcionalidad, con active record, no funciona ahora
				$query = $this -> db -> join( $joins[$i][0], $joins[$i][1], $joins[$i][1], $joins[$i][2]);
			else 
				$query = $this -> db -> join( $joins[$i][0], $joins[$i][1]);
		}
		
		$query = $this-> db -> select($campos);
		$query = $this-> db -> where($where);
		$query = $this -> db -> get($table);
		if($query -> num_rows() > 0)
            return $query -> row();
        return 0;
	}
	
	/*********************/
	/* Obtiene resultados de un  
	 * criterio de búsqueda */
	function consultaInfo($tabla,$where,$orderby,$count,$sum) {
		
		if($count)
			$query = $this->db->select('count(*) as totalCount', false);
		
		if($sum != null)
			$query = $this->db->select_sum($sum,'totalSum');
		
		$query = $this->db->where($where,false);
		
		if(!$count && $sum == null && $orderby != null) 
			$query = $this->db->order_by($orderby);
		
		$query = $this->db->get($tabla);
		if($query->num_rows() != 0)
            return $query -> result();
        return null;
	} 
	
	       
}