<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agenda_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPendientes($idUsuario,$idRol,$statusActividad,$valueAct,$tipoActividad,$value,$between,$pendientes) {
    	
		$idFraccionamiento = '';			
    	if($idUsuario != null && $idRol != null) {
    		
    		$query = $this->db->select('idFraccionamiento');
			$query = $this->db->where(array('idUsuario' => $idUsuario, 'statusUsuario' => '1'));
			$query = $this->db->get('fraccionamientousuario');
			
			if($query->num_rows() != 0) {
			
				$wherein = array();
				foreach ($query -> result() as $key) {
					$wherein[] = $key->idFraccionamiento;
				}
			
				$querytwo = $this->db->select('IF((ADDDATE(fechaInicio, INTERVAL 1 DAY) > NOW() ), \'yellow\', \'red\' ) as estado, 
				f.nombreFrac, hv.idHojaVida,nombre, CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) cliente ,email,ladaFijo,telFijo,ladaCelular,telCelular,hv.fechaRegistro,statusHoja, 
				ha.idHojaActividad,	ha.nombreActividad, ha.fechaInicio, ha.statusActividad, DATEDIFF(NOW(), hv.fechaRegistro) dias', false);
				 
				$querytwo = $this->db->from('fraccionamiento as f');
				$querytwo = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
				$querytwo = $this->db->join('hojaactividad ha','ha.idHojaVida = hv.idHojaVida');
				$querytwo = $this->db->join('usuario us','us.idUsuario = hv.idUsuario ');
				$querytwo = $this->db->where(array('statusHoja' => '1', $statusActividad => $valueAct));
				
				if($tipoActividad != null) :
					$querytwo = $this->db->where(array($tipoActividad => $value));
				endif;
				if($between != null) :
					$querytwo = $this->db->where($between);
				endif;
				if($pendientes != null) :
					$querytwo = $this->db->where($pendientes);
				endif;
				
				$querytwo = $this->db->where_in('hv.idFraccionamiento',$wherein);
				
				$querytwo = $this->db->order_by('ha.fechaInicio','asc');
				$querytwo = $this->db->order_by('nombre','asc');
				$querytwo = $this->db->order_by('cliente','asc');
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows()>0)
					return $querytwo->result();
			}
				
			return null;
				
		}
		else if($idUsuario != null) {
			
				$querytwo = $this->db->select('IF((ADDDATE(fechaInicio, INTERVAL 1 DAY) > NOW() ), \'yellow\', \'red\' ) as estado, 
				f.nombreFrac, hv.idHojaVida,nombre, CONCAT(apaterno,\' \', amaterno, \' \', nombreCliente) cliente ,email,ladaFijo,telFijo,ladaCelular,telCelular,hv.fechaRegistro,statusHoja, 
				ha.idHojaActividad,	ha.nombreActividad, ha.fechaInicio, ha.statusActividad, DATEDIFF(NOW(), hv.fechaRegistro) dias', false);
				 
				$querytwo = $this->db->from('fraccionamiento as f');
				$querytwo = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
				$querytwo = $this->db->join('hojaactividad ha','ha.idHojaVida = hv.idHojaVida');
				$querytwo = $this->db->join('usuario us','us.idUsuario = hv.idUsuario ');
				$querytwo = $this->db->where(array('hv.idUsuario' => $idUsuario, 'statusHoja' => '1', $statusActividad => $valueAct));
				
				if($tipoActividad != null) :
					$querytwo = $this->db->where(array($tipoActividad => $value));
				endif;
				if($between != null) :
					$querytwo = $this->db->where($between);
				endif;
				if($pendientes != null) :
					$querytwo = $this->db->where($pendientes);
				endif;
				
				$querytwo = $this->db->order_by('ha.fechaInicio','asc');
				$querytwo = $this->db->order_by('nombre','asc');
				$querytwo = $this->db->order_by('cliente','asc');
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows()>0)
					return $querytwo->result();
				
			return null;
		}
		else if($idRol != null) {
			$query = $this->db->select('f.nombreFrac, idHojaVida,nombre,cliente,email,telFijo,telCelular,hv.fechaRegistro,statusHoja'); 
			$query = $this->db->from('fraccionamiento as f');
			$query = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
			$query = $this->db->join('usuario as us','us.idUsuario = hv.idUsuario');
			$query = $this->db->where(array('statusHoja'=>'1'));
			$query = $this->db->order_by('f.nombreFrac','asc');
			$query = $this->db->order_by('nombre','asc');
			$query = $this->db->order_by('cliente','asc');
			$query = $this->db->get();
			
			if($query->num_rows() != 0)
            	return $query -> result();
        	return null;
		}

        return null;
    }

	function getActividades($idHojaVida, $where) {
		
		$query = $this -> db -> select('IF((ADDDATE(fechaInicio, INTERVAL 1 DAY) > NOW() ), \'yellow\', \'red\' ) as estado, ha.*',FALSE);	
        $query = $this -> db -> get_where('hojaactividad as ha', $where);
        if($query -> num_rows() > 0)
            return $query -> result();
        return null;
    }
	
	function validaMiId($idUsuario, $idHojaVida) {
			
		$query = $this->db->select('idFraccionamiento');
		$query = $this->db->where(array('idUsuario' => $idUsuario, 'statusUsuario' => '1'));
		$query = $this->db->get('fraccionamientousuario');
		
		if($query->num_rows() != 0) {
			
			$registros=0;
			foreach( $query -> result() as $key ) {
	            $idFraccionamiento = $key->idFraccionamiento;
				
				$querytwo = $this->db->select('idHojaVida'); 
				$querytwo = $this->db->from('hojavida');
				$querytwo = $this->db->where(array('idFraccionamiento'=>$idFraccionamiento,'idHojaVida'=>$idHojaVida,'statusHoja'=>'1'));
				$querytwo = $this->db->get();
				
				if($querytwo->num_rows() > 0) {
					$registros++;
				}
			}
			
			if($registros > 0)
				return 1;
			return 0;
		}
		return 0;
	}
	
	function getTotalPendiente($idUsuario,$idRol,$statusActividad,$valueAct,$vencidas,$between) {
		
		$querytwo = $this->db->select('count(*) as total', false);
		 
		$querytwo = $this->db->from('fraccionamiento as f');
		$querytwo = $this->db->join('hojavida as hv','f.idFraccionamiento = hv.idFraccionamiento');
		$querytwo = $this->db->join('hojaactividad ha','ha.idHojaVida = hv.idHojaVida');
		$querytwo = $this->db->join('usuario us','us.idUsuario = hv.idUsuario ');
		$querytwo = $this->db->where(array('hv.idUsuario' => $idUsuario, 'statusHoja' => '1', $statusActividad => $valueAct),false);
		
		if($vencidas != null) :
			$querytwo = $this->db->where($vencidas);
		endif;
		
		if($between != null) :
			$querytwo = $this->db->where($between);
		endif;
		
			$querytwo = $this->db->get();
			
			if($querytwo->num_rows()>0)
				return $querytwo->row();
			
		return null;
	}
		  
}