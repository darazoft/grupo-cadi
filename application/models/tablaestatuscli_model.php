<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablaestatuscli_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getEstatus($activo) {
    	
		if($activo) {
    		$this->db->where('statusCliente', '1');
    	}
		else {  // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusCliente !='updated' or statusCliente is null)";
			$this->db->where($special);
		}
		
		$this->db->order_by('clasificacion', 'asc');
		$query = $this->db->get('estatuscliente');

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
}