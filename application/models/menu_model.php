<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {
	
	function __construct() {
		parent::__construct();        
	}
    
    function getSecciones($idUsuario){
        $query = $this -> db -> query("select idSeccion, nombreSeccion, URL from seccion where idSeccion in (select idSeccion from seccionpermiso where idUsuario = '".$idUsuario."')");
        if($query->num_rows() != 0)
            return $query -> result();
        return null;                
    }
    
    function getMisHijos($idUsuario, $idSeccion){
        $query = $this->db->query("select idSubseccion, idSeccion, nombreSubseccion, subseccion.URL as URL_sub from subseccion where idSeccion='" . $idSeccion . "' and idSubseccion in(select idSubseccion from subseccionpermiso where idUsuario = '".$idUsuario."')");
        if($query -> num_rows() != 0)
            return $query -> result();
        return null;                
    }
    
		
}
