<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablapersonalfrac_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPersonalFrac($activo) {
    	        
		$query = $this->db->select('nombreFrac, nombre,fu.*'); 
		$query = $this->db->from('fraccionamiento as f');
		$query = $this->db->join('fraccionamientousuario as fu','f.idFraccionamiento = fu.idFraccionamiento');
		$query = $this->db->join('usuario as u','´u.idUsuario = fu.idUsuario');
		
		if($activo) {
    		$this->db->where('statusUsuario', '1');
    	}
		else {  // En esa caso mostrará los registros inactivos que fueron eliminados, evitando los updated
			$special = "(statusUsuario !='updated' or statusUsuario is null)";
			$this->db->where($special);
		}
		
		$this->db->order_by('nombreFrac', 'asc');
		$this->db->order_by('nombre', 'asc');
		
		$query = $this->db->get();

        if($query->num_rows() != 0)
            return $query -> result();
        return null;
    } 
}